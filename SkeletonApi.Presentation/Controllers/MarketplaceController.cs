﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Marketplace;
using SkeletonApi.Application.Features.MasterData.Marketplace.Commands.CreateMarketplace;
using SkeletonApi.Application.Features.MasterData.Marketplace.Commands.DeleteMarketplace;
using SkeletonApi.Application.Features.MasterData.Marketplace.Commands.UpdateMarketplace;
using SkeletonApi.Application.Features.MasterData.Marketplace.Queries.DropdownMarketplace;
using SkeletonApi.Application.Features.MasterData.Marketplace.Queries.GetMarketplaceWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/marketplace")]
    public class MarketplaceController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public MarketplaceController(IMediator mediator, ILogger<MarketplaceController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateMarketplaceResponseDto>>> CreateMarketplace(CreateMarketplaceRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteMarketPlace(Guid id)
        {
            return await _mediator.Send(new DeleteMarketplaceRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterMarketplace>>> UpdateMarketplace(Guid id, UpdateMarketplaceRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetMarketplaceWithPaginationDto>>> GetMarketplaceWithPagination([FromQuery] GetMarketplaceWithPaginationQuery query)
        {
            var validator = new GetMarketplaceWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-marketplace")]
        public async Task<ActionResult<Result<List<GetMarketplaceAllDto>>>> GetAllMarketplace()
        {
            return await _mediator.Send(new GetMarketplaceAllQuery());
        }
    }
}