﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Barang;
using SkeletonApi.Application.Features.MasterData.Barang.Commands.CreateBarang;
using SkeletonApi.Application.Features.MasterData.Barang.Commands.DeleteBarang;
using SkeletonApi.Application.Features.MasterData.Barang.Commands.UpdateBarang;
using SkeletonApi.Application.Features.MasterData.Barang.Queries.DropdownBarang;
using SkeletonApi.Application.Features.MasterData.Barang.Queries.GetBarangWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/barang")]
    public class BarangController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public BarangController(IMediator mediator, ILogger<BarangController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateBarangResponseDto>>> CreateBarang(CreateBarangRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteBarang(Guid id)
        {
            return await _mediator.Send(new DeleteBarangRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterBarang>>> UpdateBarang(Guid id, UpdateBarangRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetBarangWithPaginationDto>>> GetBarangWithPagination([FromQuery] GetBarangWithPaginationQuery query)
        {
            var validator = new GetBarangWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-barang")]
        public async Task<ActionResult<Result<List<GetBarangAllDto>>>> GetAllBarang()
        {
            return await _mediator.Send(new GetBarangAllQuery());
        }
    }
}