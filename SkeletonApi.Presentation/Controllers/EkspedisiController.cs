﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Ekspedisi;
using SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.CreateEkspedisi;
using SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.DeleteEkspedisi;
using SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.UpdateEkspedisi;
using SkeletonApi.Application.Features.MasterData.Ekspedisi.Queries.DropdownEkspedisi;
using SkeletonApi.Application.Features.MasterData.Ekspedisi.Queries.GetEkspedisiWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/ekspedisi")]
    public class EkspedisiController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public EkspedisiController(IMediator mediator, ILogger<BarangController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateResponseEkspedisiDto>>> CreateEkspedisi(CreateEkspedisiRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteEkspedisi(Guid id)
        {
            return await _mediator.Send(new DeleteEkspedisiRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterEkspedisi>>> UpdateEkspedisi(Guid id, UpdateEkspedisiRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetEkspedisiWithPaginationDto>>> GetEkspedisiWithPagination([FromQuery] GetEkspedisiWithPaginationQuery query)
        {
            var validator = new GetEkspedisiWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-ekspedisi")]
        public async Task<ActionResult<Result<List<GetEkspedisiAllDto>>>> GetAllEkspedisi()
        {
            return await _mediator.Send(new GetEkspedisiAllQuery());
        }
    }
}