﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisJumlahStok;
using SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisTanggaldanRak;
using SkeletonApi.Shared;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/isi-otomatis")]
    public class IsiOtomatisController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public IsiOtomatisController(IMediator mediator, ILogger<IsiOtomatisController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("list-otomatis-tanggal-dan-rak")]
        public async Task<ActionResult<Result<List<OtomatisTanggaldanRakDto>>>> GetListTanggalDanRak(Guid IdBarang, Guid IdLokasi)
        {
            return await _mediator.Send(new OtomatisTanggalDanRakQuery(IdBarang, IdLokasi));
        }

        [HttpGet("data-otomatis-jumlah-stok")]
        public async Task<ActionResult<Result<OtomatisJumlahStokDto>>> GetListJumlahStok(Guid IdRak, Guid IdLokasi)
        {
            return await _mediator.Send(new OtomatisJumlahStokQuery(IdRak, IdLokasi));
        }
    }
}