﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.DataPeringatanStok;
using SkeletonApi.Application.Features.DataPeringatanStok.Commands.CreatePeringatanStok;
using SkeletonApi.Application.Features.DataPeringatanStok.Commands.DeletePeringatanStok;
using SkeletonApi.Application.Features.DataPeringatanStok.Commands.UpdatePeringatanStok;
using SkeletonApi.Application.Features.DataPeringatanStok.Queries.GetPeringatanStokWithPagination;
using SkeletonApi.Application.Features.DataPeringatanStok.Queries.Notification;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/peringatan-stok")]
    public class PeringatanStokController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public PeringatanStokController(IMediator mediator, ILogger<PeringatanStokController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<List<CreatePeringatanStokResponseDto>>>> CreatePeringatanStok(CreatePeringatanStokRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeletePeringatanStok(Guid id)
        {
            return await _mediator.Send(new DeletePeringatanStokRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<PeringatanStok>>> UpdateBarangMasuk(Guid id, UpdatePeringatanStokRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetPeringatanStokWithPaginationDto>>> GetBarangMasukWithPagination([FromQuery] GetPeringatanStokWithPaginationQuery query)
        {
            var validator = new GetPeringatanStokWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("download")]
        public async Task<ActionResult<PaginatedResult<GetPeringatanStokWithPaginationDto>>> DownloadRepairToExcel([FromQuery] GetPeringatanStokWithPaginationQuery query)
        {
            var validator = new GetPeringatanStokWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                byte[]? bytes = null;
                string filename = string.Empty;
                try
                {
                    if (pg != null)
                    {
                        var Export = new DownloadPeringatanStokToExcell(pg);
                        Export.GetListExcel(ref bytes, ref filename);
                    }
                    Response.Headers.Add("x-download", filename);
                    return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null) { return Problem(ex.Message); }
                    return Problem(ex.InnerException.Message);
                }
            }
            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("notification-stok")]
        public async Task<ActionResult<Result<List<GetNotificationDto>>>> GetNotification()
        {
            return await _mediator.Send(new GetNotificationQuery());
        }
    }
}