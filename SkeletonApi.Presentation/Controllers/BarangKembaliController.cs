﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.Accounts.Profiles.Queries.GetAllAccountsByUsername;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.KonfirmasiBarangKeluar;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.DownloadBarangKeluarToExcell;
using SkeletonApi.Application.Features.DaftarBarangKembali;
using SkeletonApi.Application.Features.DaftarBarangKembali.Commands.CreateBarangKembali;
using SkeletonApi.Application.Features.DaftarBarangKembali.Commands.DeleteBarangKembali;
using SkeletonApi.Application.Features.DaftarBarangKembali.Commands.KonfirmasiBarangKembali;
using SkeletonApi.Application.Features.DaftarBarangKembali.Commands.UpdateBarangKembali;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.Download;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetBarangKembaliWithPagination;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetByIdTransaksi;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("barang-kembali")]
    public class BarangKembaliController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public BarangKembaliController(IMediator mediator, ILogger<BarangKembaliController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<List<CreateResponseBarangKembaliDto>>>> CreateBarangMasuk(CreateBarangKembaliRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Result<string>>> DeleteBarangMasuk(string id)
        {
            return await _mediator.Send(new DeleteBarangKembaliRequest(id));
        }

        [HttpPut("konfirmasi/{id}")]
        public async Task<ActionResult<Result<UpdateStatusDto>>> UpdateBarangKembali(string id, KonfirmasiBarangKembaliRequest command)
        {
            if (id != command.IdTransaksi)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpPut()]
        public async Task<ActionResult<Result<BarangKembali>>> Update(UpdateBarangKembaliRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetBarangKembaliWithPaginationDto>>> GetBarangKeluarWithPagination([FromQuery] GetBarangKembaliWithPaginationQuery query)
        {
            var validator = new GetBarangKembaliWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }
        [HttpGet("{idTransaksi}")]
        public async Task<ActionResult<Result<List<GetByIdTransaksiDto>>>> GetByIdTransaksi(string idTransaksi)
        {
            return await _mediator.Send(new GetByIdTransaksiQuery(idTransaksi));
        }
        [HttpGet("download-pdf")]
        public async Task<ActionResult<DownloadBarangKembaliToExcelAndPdfDto>> GetPdf(string idTransaksi)
        {
            return await _mediator.Send(new DownloadBarangKembaliToExcelAndPdfQuery(idTransaksi));
        }

        [HttpGet("download-excel")]
        public async Task<ActionResult<DownloadBarangKembaliToExcelAndPdfDto>> DownloadBarangKembaliToExcel(string idTransaksi)
        {
            var pg = _mediator.Send(new DownloadBarangKembaliToExcelAndPdfQuery(idTransaksi));
            byte[]? bytes = null;
            string filename = string.Empty;
            try
            {
                if (pg != null)
                {
                    var Export = new DownloadBarangKembaliToExcel(pg);
                    Export.GetListExcel(ref bytes, ref filename);
                }
                Response.Headers.Add("x-download", filename);
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null) { return Problem(ex.Message); }
                return Problem(ex.InnerException.Message);
            }
        }
    }
}