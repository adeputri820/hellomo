﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.Dropdown.Queries.DropdownLocation;
using SkeletonApi.Application.Features.Dropdown.Queries.DropdownRak;
using SkeletonApi.Application.Features.Dropdown.Queries.DropdownSKUdanBarang;
using SkeletonApi.Shared;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/dropdown")]
    public class DropdownController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public DropdownController(IMediator mediator, ILogger<DropdownController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("sku-dan-barang")]
        public async Task<ActionResult<Result<List<DropdownSKUdanBarangDto>>>> GetListSKUDanBarang()
        {
            return await _mediator.Send(new DropdownSKUdanBarangQuery());
        }

        [HttpGet("location")]
        public async Task<ActionResult<Result<List<DropdownLocationDto>>>> GetListLocation()
        {
            return await _mediator.Send(new DropdownLocationQuery());
        }

        [HttpGet("rak")]
        public async Task<ActionResult<Result<List<DropdownRakDto>>>> GetListRak()
        {
            return await _mediator.Send(new DropdownRakQuery());
        }
    }
}