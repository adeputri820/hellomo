﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.DaftarKomplain;
using SkeletonApi.Application.Features.DaftarKomplain.Commands.CreateKomplain;
using SkeletonApi.Application.Features.DaftarKomplain.Commands.DeleteKomplain;
using SkeletonApi.Application.Features.DaftarKomplain.Queries.GetKomplainWithPagination;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/komplain")]
    public class KomplainController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public KomplainController(IMediator mediator, ILogger<KomplainController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateKomplainResponseDto>>> Create(CreateKomplainRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteLokasi(Guid id)
        {
            return await _mediator.Send(new DeleteKomplainRequest(id));
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetKomplainWithPaginationDto>>> GetEkspedisiWithPagination([FromQuery] GetKomplainWithPaginationQuery query)
        {
            var validator = new GetKomplainWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }
    }
}