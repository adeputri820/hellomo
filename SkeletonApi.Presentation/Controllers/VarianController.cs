﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Varian;
using SkeletonApi.Application.Features.MasterData.Varian.Commands.CreateVarian;
using SkeletonApi.Application.Features.MasterData.Varian.Commands.DeleteVarian;
using SkeletonApi.Application.Features.MasterData.Varian.Commands.UpdateVarian;
using SkeletonApi.Application.Features.MasterData.Varian.Queries.DropdownVarian;
using SkeletonApi.Application.Features.MasterData.Varian.Queries.GetVarianWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/varian")]
    public class VarianController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public VarianController(IMediator mediator, ILogger<VarianController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateVarianResponseDto>>> CreateVarian(CreateVarianRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteVarian(Guid id)
        {
            return await _mediator.Send(new DeleteVarianRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterVarian>>> UpdateVarian(Guid id, UpdateVarianRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetVarianWithPaginationDto>>> GetVarianWithPagination([FromQuery] GetVarianWithPaginationQuery query)
        {
            var validator = new GetVarianWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-varian")]
        public async Task<ActionResult<Result<List<GetVarianAllDto>>>> GetAllVarian()
        {
            return await _mediator.Send(new GetVarianAllQuery());
        }
    }
}