﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Commands;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.CreateBarangKeluar;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.KonfirmasiBarangKeluar;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.DownloadBarangKeluarToExcell;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.GetBarangKeluarWithPagination;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.GetKonfirmasiBarangKeluarWithPagination;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/barang-keluar")]
    public class BarangKeluarController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public BarangKeluarController(IMediator mediator, ILogger<BarangKeluarController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<List<CreateResponseBarangKeluarDto>>>> CreateBarangMasuk(CreateBarangKeluarRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("update-konfirmasi-finish")]
        public async Task<ActionResult<Result<UpdateStatusDto>>> UpdateBarangMasuk(UpdateKonfirmasiBarangKeluarRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetBarangKeluarWithPaginationDto>>> GetBarangKeluarWithPagination([FromQuery] GetBarangKeluarWithPaginationQuery query)
        {
            var validator = new GetBarangKeluarWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("paged-konfirmasi-barang")]
        public async Task<ActionResult<PaginatedResult<GetKonfirmasiBarangKeluarWithPaginationDto>>> GetKonfirmasiBarangWithPagination([FromQuery] GetKonfirmasiBarangKeluarWithPaginationQuery query)
        {
            var validator = new GetKonfirmasiBarangKeluarWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("download-excell   ")]
        public async Task<ActionResult<DownloadBarangToExcellAndPdfDto>> DownloadBarangKeluarToExcel(string idTransaksi)
        {
            var pg = _mediator.Send(new DownloadBarangToExcellAndPdfQuery(idTransaksi));
            byte[]? bytes = null;
            string filename = string.Empty;
            try
            {
                if (pg != null)
                {
                    var Export = new DownloadBarangToExcell(pg);
                    Export.GetListExcel(ref bytes, ref filename);
                }
                Response.Headers.Add("x-download", filename);
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null) { return Problem(ex.Message); }
                return Problem(ex.InnerException.Message);
            }
        }

        [HttpGet("download-pdf")]
        public async Task<ActionResult<DownloadBarangToExcellAndPdfDto>> GetListTanggalDanRak(string idTransaksi)
        {
            return await _mediator.Send(new DownloadBarangToExcellAndPdfQuery(idTransaksi));
        }
    }
}