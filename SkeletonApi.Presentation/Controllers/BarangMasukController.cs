﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Commands;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.CreateBarangMasuk;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.DeleteBarangMasuk;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.UpdateBarangMasuk;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Queries.GetBarangMasukWithPagination;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Queries.GetSkuBarangMasuk;
using SkeletonApi.Application.Features.MasterData.Barang.Queries.DropdownBarang;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/barang-masuk")]
    public class BarangMasukController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public BarangMasukController(IMediator mediator, ILogger<BarangMasukController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<List<CreateResponseBarangMasukDto>>>> CreateBarangMasuk(CreateBarangMasukReques command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<BarangMasuk>>> UpdateBarangMasuk(Guid id, UpdateBarangMasukRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteBarangMasuk(Guid id)
        {
            return await _mediator.Send(new DeleteBarangMasukRequest(id));
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetBarangMasukWithPaginationDto>>> GetBarangMasukWithPagination([FromQuery] GetBarangMasukWithPaginationQuery query)
        {
            var validator = new GetBarangMasukWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("download")]
        public async Task<ActionResult<PaginatedResult<GetBarangMasukWithPaginationDto>>> DownloadRepairToExcel([FromQuery] GetBarangMasukWithPaginationQuery query)
        {
            var validator = new GetBarangMasukWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                byte[]? bytes = null;
                string filename = string.Empty;
                try
                {
                    if (pg != null)
                    {
                        var Export = new DownloadBarangMasukToExcell(pg);
                        Export.GetListExcel(ref bytes, ref filename);
                    }
                    Response.Headers.Add("x-download", filename);
                    return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null) { return Problem(ex.Message); }
                    return Problem(ex.InnerException.Message);
                }
            }
            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }
        [HttpGet]
        public async Task<ActionResult<Result<List<GetSkuBarangMasukDto>>>> GetAllSkuBarang()
        {
            return await _mediator.Send(new GetSkuBarangMasukQuery());
        }
    }
}