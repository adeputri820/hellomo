﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetByIdTransaksi;
using SkeletonApi.Application.Features.DaftarPembelian;
using SkeletonApi.Application.Features.DaftarPembelian.Commands.CreatePembelian;
using SkeletonApi.Application.Features.DaftarPembelian.Queries.GetPembelianByNoPesanan;
using SkeletonApi.Application.Features.DaftarPembelian.Queries.GetPembelianWithPagination;
using SkeletonApi.Application.Features.DataPeringatanStok.Queries.GetPeringatanStokWithPagination;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/pembelian")]
    public class PembelianController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public PembelianController(IMediator mediator, ILogger<PembelianController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<List<CreatePembelianResponseDto>>>> Create(CreatePembelianRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetPembelianWithPaginationDto>>> GetWithPagination([FromQuery] GetPembelianWithPaginationQuery query)
        {
            var validator = new GetPembelianWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }
        [HttpGet("{noPesanan}")]
        public async Task<ActionResult<Result<GetPembelianByNoPesananDto>>> GetByNoPesanan(string noPesanan)
        {
            return await _mediator.Send(new GetPembelianByNoPesananQuery(noPesanan));
        }
        [HttpGet("download")]
        public async Task<ActionResult<PaginatedResult<GetPembelianWithPaginationDto>>> DownloadRepairToExcel([FromQuery] GetPembelianWithPaginationQuery query)
        {
            var validator = new GetPembelianWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                byte[]? bytes = null;
                string filename = string.Empty;
                try
                {
                    if (pg != null)
                    {
                        var Export = new DownloadPembelianToExcel(pg);
                        Export.GetListExcel(ref bytes, ref filename);
                    }
                    Response.Headers.Add("x-download", filename);
                    return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null) { return Problem(ex.Message); }
                    return Problem(ex.InnerException.Message);
                }
            }
            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }
    }
}