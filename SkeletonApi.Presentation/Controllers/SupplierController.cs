﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Supplier;
using SkeletonApi.Application.Features.MasterData.Supplier.Commands.CreateSupplier;
using SkeletonApi.Application.Features.MasterData.Supplier.Commands.DeleteSupplier;
using SkeletonApi.Application.Features.MasterData.Supplier.Commands.UpdateSupplier;
using SkeletonApi.Application.Features.MasterData.Supplier.Queries.DropdownSupplier;
using SkeletonApi.Application.Features.MasterData.Supplier.Queries.GetSupplierWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/supplier")]
    public class SupplierController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public SupplierController(IMediator mediator, ILogger<SupplierController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateSupplierResponseDto>>> CreateSupplier(CreateSupplierRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteSupplier(Guid id)
        {
            return await _mediator.Send(new DeleteSupplierRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterSupplier>>> UpdateSupplier(Guid id, UpdateSupplierRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetSupplierWithPaginationDto>>> GetSupplierWithPagination([FromQuery] GetSupplierWithPaginationQuery query)
        {
            var validator = new GetSupplierWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-supplier")]
        public async Task<ActionResult<Result<List<GetSupplierAllDto>>>> GetAllSupplier()
        {
            return await _mediator.Send(new GetSupplierAllQuery());
        }
    }
}