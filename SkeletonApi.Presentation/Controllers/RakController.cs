﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Rak;
using SkeletonApi.Application.Features.MasterData.Rak.Commands.CreateRak;
using SkeletonApi.Application.Features.MasterData.Rak.Commands.DeleteRak;
using SkeletonApi.Application.Features.MasterData.Rak.Commands.UpdateRak;
using SkeletonApi.Application.Features.MasterData.Rak.Queries.DropdownRak;
using SkeletonApi.Application.Features.MasterData.Rak.Queries.GetRakWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/rak")]
    public class RakController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public RakController(IMediator mediator, ILogger<RakController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateRakResponseDto>>> CreateRak(CreateRakRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteRak(Guid id)
        {
            return await _mediator.Send(new DeleteRakRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterRak>>> UpdateRak(Guid id, UpdateRakRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetRakWithPaginationDto>>> GetRakWithPagination([FromQuery] GetRakWithPaginationQuery query)
        {
            var validator = new GetRakWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-rak")]
        public async Task<ActionResult<Result<List<GetRakAllDto>>>> GetAllRak()
        {
            return await _mediator.Send(new GetRakAllQuery());
        }
    }
}