﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Lokasi;
using SkeletonApi.Application.Features.MasterData.Lokasi.Commands.CreateLokasi;
using SkeletonApi.Application.Features.MasterData.Lokasi.Commands.DeleteLokasi;
using SkeletonApi.Application.Features.MasterData.Lokasi.Commands.UpdateLokasi;
using SkeletonApi.Application.Features.MasterData.Lokasi.Queries.DropdownLokasi;
using SkeletonApi.Application.Features.MasterData.Lokasi.Queries.GetLokasiWIthPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/lokasi")]
    public class LokasiController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public LokasiController(IMediator mediator, ILogger<LokasiController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateLokasiResponseDto>>> CreateLokasi(CreateLokasiRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> DeleteLokasi(Guid id)
        {
            return await _mediator.Send(new DeleteLokasiRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterLokasi>>> UpdateLokasi(Guid id, UpdateLokasiRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetLokasiWithPaginationDto>>> GetLokasiWithPagination([FromQuery] GetLokasiWithPaginationQuery query)
        {
            var validator = new GetLokasiWithPaginationValidator();
            // Call Validate or ValidateAsync and pass the object which needs to be validated

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.PageNumber,
                    pg.TotalPages,
                    pg.PageSize,
                    pg.TotalCount,
                    pg.Has_Previous,
                    pg.Has_Next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-lokasi")]
        public async Task<ActionResult<Result<List<GetLokasiAllDto>>>> GetAllLokasi()
        {
            return await _mediator.Send(new GetLokasiAllQuery());
        }
    }
}