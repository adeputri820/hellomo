﻿namespace SkeletonApi.Infrastructure.DTOs
{
    public class GetDataDto
    {
        public string name { get; set; }
        public string category { get; set; }
    }
}