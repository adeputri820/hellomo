﻿namespace SkeletonApi.Infrastructure.Common
{
    public class MqttBrokerSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}