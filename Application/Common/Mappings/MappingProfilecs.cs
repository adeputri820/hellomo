﻿using AutoMapper;
using SkeletonApi.Application.Features.Accounts;
using SkeletonApi.Application.Features.Accounts.Profiles.Commands.CreateAccount;
using SkeletonApi.Application.Features.DaftarBarangKembali;
using SkeletonApi.Application.Features.DaftarBarangKembali.Commands.CreateBarangKembali;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Commands;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.CreateBarangMasuk;
using SkeletonApi.Application.Features.DaftarKomplain;
using SkeletonApi.Application.Features.DaftarKomplain.Commands.CreateKomplain;
using SkeletonApi.Application.Features.DaftarPembelian;
using SkeletonApi.Application.Features.DaftarPembelian.Commands.CreatePembelian;
using SkeletonApi.Application.Features.DataPeringatanStok;
using SkeletonApi.Application.Features.DataPeringatanStok.Commands.CreatePeringatanStok;
using SkeletonApi.Application.Features.ManagementUser.Permissions;
using SkeletonApi.Application.Features.ManagementUser.Permissions.Commands.CreatePermissions;
using SkeletonApi.Application.Features.ManagementUser.Permissions.Commands.UpdatePermissions;
using SkeletonApi.Application.Features.ManagementUser.Roles;
using SkeletonApi.Application.Features.ManagementUser.Roles.Commands.CreateRoles;
using SkeletonApi.Application.Features.ManagementUser.Users.Commands.CreateUser;
using SkeletonApi.Application.Features.MasterData.Barang;
using SkeletonApi.Application.Features.MasterData.Barang.Commands.CreateBarang;
using SkeletonApi.Application.Features.MasterData.Ekspedisi;
using SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.CreateEkspedisi;
using SkeletonApi.Application.Features.MasterData.Lokasi;
using SkeletonApi.Application.Features.MasterData.Lokasi.Commands.CreateLokasi;
using SkeletonApi.Application.Features.MasterData.Marketplace;
using SkeletonApi.Application.Features.MasterData.Marketplace.Commands.CreateMarketplace;
using SkeletonApi.Application.Features.MasterData.Rak;
using SkeletonApi.Application.Features.MasterData.Rak.Commands.CreateRak;
using SkeletonApi.Application.Features.MasterData.Supplier;
using SkeletonApi.Application.Features.MasterData.Supplier.Commands.CreateSupplier;
using SkeletonApi.Application.Features.MasterData.Varian;
using SkeletonApi.Application.Features.MasterData.Varian.Commands.CreateVarian;
using SkeletonApi.Application.Features.Users;
using SkeletonApi.Domain.Entities;
using System.Reflection;

namespace SkeletonApi.Application.Common.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());

            CreateMap<CreateUserRequest, User>();
            CreateMap<User, CreateUserResponseDto>();

            CreateMap<CreateRolesRequest, Role>();
            CreateMap<Role, CreateRolesResponseDto>();

            CreateMap<CreatePermissionsRequest, Permission>();
            CreateMap<Permission, CreatePermissionsResponseDto>();

            CreateMap<UpdatePermissionsRequest, Permission>();

            CreateMap<CreateAccountRequest, Account>();
            CreateMap<Account, CreateAccountResponseDto>();

            CreateMap<CreateBarangRequest, MasterBarang>();
            CreateMap<MasterBarang, CreateBarangResponseDto>();

            CreateMap<CreateBarangMasukReques, BarangMasuk>();
            CreateMap<BarangMasuk, CreateResponseBarangMasukDto>();

            CreateMap<CreateBarangKembaliRequest, BarangKembali>();
            CreateMap<BarangKembali, CreateResponseBarangKembaliDto>();

            CreateMap<CreatePeringatanStokRequest, PeringatanStok>();
            CreateMap<PeringatanStok, CreatePeringatanStokResponseDto>();

            CreateMap<CreateEkspedisiRequest, MasterEkspedisi>();
            CreateMap<MasterEkspedisi, CreateResponseEkspedisiDto>();

            CreateMap<CreateLokasiRequest, MasterLokasi>();
            CreateMap<MasterLokasi, CreateLokasiResponseDto>();

            CreateMap<CreateMarketplaceRequest, MasterMarketplace>();
            CreateMap<MasterMarketplace, CreateMarketplaceResponseDto>();

            CreateMap<CreateRakRequest, MasterRak>();
            CreateMap<MasterRak, CreateRakResponseDto>();

            CreateMap<CreateSupplierRequest, MasterSupplier>();
            CreateMap<MasterSupplier, CreateSupplierResponseDto>();

            CreateMap<CreateVarianRequest, MasterVarian>();
            CreateMap<MasterVarian, CreateVarianResponseDto>();

            CreateMap<CreatePembelianRequest, Pembelian>();
            CreateMap<Pembelian, CreatePembelianResponseDto>();

            CreateMap<CreateKomplainRequest, Komplain>();
            CreateMap<Komplain, CreateKomplainResponseDto>();
        }

        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var mapFromType = typeof(IMapFrom<>);

            var mappingMethodName = nameof(IMapFrom<object>.Mapping);

            bool HasInterface(Type t) => t.IsGenericType && t.GetGenericTypeDefinition() == mapFromType;

            var types = assembly.GetExportedTypes().Where(t => t.GetInterfaces().Any(HasInterface)).ToList();

            var argumentTypes = new Type[] { typeof(Profile) };

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type);

                var methodInfo = type.GetMethod(mappingMethodName);

                if (methodInfo != null)
                {
                    methodInfo.Invoke(instance, new object[] { this });
                }
                else
                {
                    var interfaces = type.GetInterfaces().Where(HasInterface).ToList();

                    if (interfaces.Count > 0)
                    {
                        foreach (var @interface in interfaces)
                        {
                            var interfaceMethodInfo = @interface.GetMethod(mappingMethodName, argumentTypes);

                            interfaceMethodInfo.Invoke(instance, new object[] { this });
                        }
                    }
                }
            }
        }
    }
}