﻿using SkeletonApi.Application.Features.DashboardInventory.Queries.GetDashboard;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IDashboardRepository
    {
        Task<GetDashboardDto> GetDashboardAsync(DateTime start, DateTime end, string type);
    }
}