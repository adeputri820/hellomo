﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IMarketplaceRepository
    {
        Task<bool> ValidateData(MasterMarketplace masterMarketplace);
    }
}