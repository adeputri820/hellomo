﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface ILokasiRepository
    {
        Task<bool> ValidateData(MasterLokasi lokasi);
    }
}