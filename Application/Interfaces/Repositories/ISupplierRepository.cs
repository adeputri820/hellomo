﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface ISupplierRepository
    {
        Task<bool> ValidateData(MasterSupplier supplier);
    }
}