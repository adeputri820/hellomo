﻿using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.Download;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetBarangKembaliWithPagination;
using SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisJumlahStok;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IJumlahStokRepository
    {
        Task<OtomatisJumlahStokDto> JumlahStok(Guid idRak, Guid idLokasi);
        Task<List<GetBarangKembaliWithPaginationDto>> GetBarangKembaliWithPagination();
        Task<DownloadBarangKembaliToExcelAndPdfDto> DownloadBarangKembaliToExcelAndPdfDto(string idTransaksi);
    }
}