﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IBarangRepository
    {
        Task<bool> ValidateData(MasterBarang masterBarang);
    }
}