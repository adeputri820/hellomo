﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IRakRepository
    {
        Task<bool> ValidateData(MasterRak rak);
    }
}