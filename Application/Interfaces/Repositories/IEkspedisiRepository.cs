﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IEkspedisiRepository
    {
        Task<bool> ValidateData(MasterEkspedisi masterBarang);
    }
}