﻿using SkeletonApi.Application.Features.DataPeringatanStok.Queries.Notification;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface INotificationStokRepository
    {
        Task<List<GetNotificationDto>> NotificationStok();
    }
}