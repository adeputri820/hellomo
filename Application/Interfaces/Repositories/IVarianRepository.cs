﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IVarianRepository
    {
        Task<bool> ValidateData(MasterVarian masterVarian);
    }
}