﻿namespace SkeletonApi.Application.Interfaces
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
    }
}