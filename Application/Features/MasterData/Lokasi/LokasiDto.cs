﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Lokasi
{
    public record LokasiDto
    {
        [JsonPropertyName("nama_lokasi")]
        public string NamaLokasi { get; set; }
    }
    public sealed record CreateLokasiResponseDto : LokasiDto { }
}