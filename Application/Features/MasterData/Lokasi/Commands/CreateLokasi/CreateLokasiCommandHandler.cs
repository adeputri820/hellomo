﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.CreateLokasi
{
    internal class CreateLokasiCommandHandler : IRequestHandler<CreateLokasiRequest, Result<CreateLokasiResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILokasiRepository _lokasiRepository;

        public CreateLokasiCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ILokasiRepository lokasiRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _lokasiRepository = lokasiRepository;
        }

        public async Task<Result<CreateLokasiResponseDto>> Handle(CreateLokasiRequest request, CancellationToken cancellationToken)
        {
            var lokasi = _mapper.Map<MasterLokasi>(request);

            var validateLokasi = await _lokasiRepository.ValidateData(lokasi);
            if (validateLokasi == true)
            {
                return await Result<CreateLokasiResponseDto>.FailureAsync("Lokasi already exist.");
            }
            lokasi.NamaLokasi = request.NamaLokasi;
            lokasi.CreatedAt = DateTime.UtcNow;
            lokasi.UpdatedAt = DateTime.UtcNow;
            await _unitOfWork.Repository<MasterLokasi>().AddAsync(lokasi);
            lokasi.AddDomainEvent(new LokasiCreatedEvent(lokasi));
            await _unitOfWork.Save(cancellationToken);
            var lokasiResponse = _mapper.Map<CreateLokasiResponseDto>(lokasi);
            return await Result<CreateLokasiResponseDto>.SuccessAsync(lokasiResponse, "Lokasi created.");
        }
    }
}