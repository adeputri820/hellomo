﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.CreateLokasi
{
    public class LokasiCreatedEvent : BaseEvent
    {
        public MasterLokasi MasterLokasi { get; set; }

        public LokasiCreatedEvent(MasterLokasi masterLokasi)
        {
            MasterLokasi = masterLokasi;
        }
    }
}