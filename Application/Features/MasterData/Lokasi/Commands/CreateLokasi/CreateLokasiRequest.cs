﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.CreateLokasi
{
    public class CreateLokasiRequest : IRequest<Result<CreateLokasiResponseDto>>
    {
        [JsonPropertyName("nama_lokasi")]
        public string NamaLokasi { get; set; }
    }
}