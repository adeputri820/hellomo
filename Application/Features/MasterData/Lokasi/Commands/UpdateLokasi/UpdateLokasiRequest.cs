﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.UpdateLokasi
{
    public class UpdateLokasiRequest : IRequest<Result<MasterLokasi>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_lokasi")]
        public string NamaLokasi { get; set; }
    }
}