﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.UpdateLokasi
{
    public class LokasiUpdateEvent : BaseEvent
    {
        public MasterLokasi MasterLokasi { get; set; }

        public LokasiUpdateEvent(MasterLokasi masterLokasi)
        {
            MasterLokasi = masterLokasi;
        }
    }
}