﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.UpdateLokasi
{
    internal class UpdateLokasiCommandHandler : IRequestHandler<UpdateLokasiRequest, Result<MasterLokasi>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateLokasiCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<MasterLokasi>> Handle(UpdateLokasiRequest request, CancellationToken cancellationToken)
        {
            var lokasiUpdate = await _unitOfWork.Repository<MasterLokasi>().GetByIdAsync(request.Id);
            if (lokasiUpdate != null)
            {
                lokasiUpdate.NamaLokasi = request.NamaLokasi;
                lokasiUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterLokasi>().UpdateAsync(lokasiUpdate);
                lokasiUpdate.AddDomainEvent(new LokasiUpdateEvent(lokasiUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<MasterLokasi>.SuccessAsync(lokasiUpdate, "Lokasi Updated");
            }
            return await Result<MasterLokasi>.FailureAsync("Lokasi Not Found");
        }
    }
}