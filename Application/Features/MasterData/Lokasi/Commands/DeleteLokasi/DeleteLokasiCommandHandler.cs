﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.DeleteLokasi
{
    internal class DeleteLokasiCommandHandler : IRequestHandler<DeleteLokasiRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteLokasiCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteLokasiRequest request, CancellationToken cancellationToken)
        {
            var lokasiDelete = await _unitOfWork.Repository<MasterLokasi>().GetByIdAsync(request.Id);
            if (lokasiDelete != null)
            {
                lokasiDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterLokasi>().DeleteAsync(lokasiDelete);
                lokasiDelete.AddDomainEvent(new LokasiDeletedEvent(lokasiDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(lokasiDelete.Id, "Lokasi Deleted.");
            }
            return await Result<Guid>.FailureAsync("Lokasi Not Found");
        }
    }
}