﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.DeleteLokasi
{
    public class DeleteLokasiRequest : IRequest<Result<Guid>>, IMapFrom<MasterLokasi>
    {
        public Guid Id { get; set; }

        public DeleteLokasiRequest()
        { }

        public DeleteLokasiRequest(Guid id)
        {
            Id = id;
        }
    }
}