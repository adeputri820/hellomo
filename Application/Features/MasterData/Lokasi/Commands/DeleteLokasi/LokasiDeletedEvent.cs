﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Commands.DeleteLokasi
{
    public class LokasiDeletedEvent : BaseEvent
    {
        public MasterLokasi MasterLokasi { get; set; }

        public LokasiDeletedEvent(MasterLokasi masterLokasi)
        {
            MasterLokasi = masterLokasi;
        }
    }
}