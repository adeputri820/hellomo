﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Queries.DropdownLokasi
{
    public record GetLokasiAllQuery : IRequest<Result<List<GetLokasiAllDto>>>;

    internal class GetLokasiAllQueryHandler : IRequestHandler<GetLokasiAllQuery, Result<List<GetLokasiAllDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetLokasiAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetLokasiAllDto>>> Handle(GetLokasiAllQuery query, CancellationToken cancellationToken)
        {
            var lokasi = await _unitOfWork.Repository<MasterLokasi>().Entities
                            .ProjectTo<GetLokasiAllDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetLokasiAllDto>>.SuccessAsync(lokasi, "Successfully fetch data");
        }
    }
}