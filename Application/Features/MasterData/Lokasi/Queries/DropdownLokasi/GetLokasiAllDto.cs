﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Queries.DropdownLokasi
{
    public class GetLokasiAllDto : IMapFrom<MasterLokasi>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_lokasi")]
        public string NamaLokasi { get; set; }
    }
}