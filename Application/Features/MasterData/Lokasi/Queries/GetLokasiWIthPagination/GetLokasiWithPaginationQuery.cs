﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Queries.GetLokasiWIthPagination
{
    public record GetLokasiWithPaginationQuery : IRequest<PaginatedResult<GetLokasiWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetLokasiWithPaginationQuery()
        {
        }

        public GetLokasiWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetLokasiWithPaginationQueryHandler : IRequestHandler<GetLokasiWithPaginationQuery, PaginatedResult<GetLokasiWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetLokasiWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetLokasiWithPaginationDto>> Handle(GetLokasiWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterLokasi>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || query.SearchTerm.ToLower() == x.NamaLokasi.ToLower()))
             .Select(o => new GetLokasiWithPaginationDto
             {
                 Id = o.Id,
                 NamaLokasi = o.NamaLokasi,
                 UpdatedAt = o.UpdatedAt.Value.ToString("dd-MM-yyyy"),
                 Updated_At = o.UpdatedAt.Value
             })
            .OrderByDescending(x => x.Updated_At)
            .ProjectTo<GetLokasiWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}