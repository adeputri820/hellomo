﻿using FluentValidation;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Queries.GetLokasiWIthPagination
{
    public class GetLokasiWithPaginationValidator : AbstractValidator<GetLokasiWithPaginationQuery>
    {
        public GetLokasiWithPaginationValidator()
        {
            RuleFor(x => x.PageNumber)
                  .GreaterThanOrEqualTo(1)
                  .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}