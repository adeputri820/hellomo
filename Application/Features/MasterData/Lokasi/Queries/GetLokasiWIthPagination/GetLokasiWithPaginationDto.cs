﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Lokasi.Queries.GetLokasiWIthPagination
{
    public class GetLokasiWithPaginationDto : IMapFrom<GetLokasiWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_lokasi")]
        public string NamaLokasi { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdatedAt { get; set; }
        public DateTime Updated_At { get; set; }
    }
}