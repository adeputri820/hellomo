﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Queries.GetMarketplaceWithPagination
{
    public class GetMarketplaceWithPaginationDto : IMapFrom<GetMarketplaceWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_marketplace")]
        public string NamaMarketplace { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdateAt { get; set; }
        public DateTime Update_At { get; set; }
    }
}