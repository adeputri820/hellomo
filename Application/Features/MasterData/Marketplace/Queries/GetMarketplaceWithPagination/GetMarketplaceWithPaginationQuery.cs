﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Queries.GetMarketplaceWithPagination
{
    public record GetMarketplaceWithPaginationQuery : IRequest<PaginatedResult<GetMarketplaceWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetMarketplaceWithPaginationQuery()
        {
        }

        public GetMarketplaceWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetMarketplaceWithPaginationQueryHandler : IRequestHandler<GetMarketplaceWithPaginationQuery, PaginatedResult<GetMarketplaceWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMarketplaceWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetMarketplaceWithPaginationDto>> Handle(GetMarketplaceWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterMarketplace>().FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.NamaMarketplace.ToLower())
            .Select(o => new GetMarketplaceWithPaginationDto
            {
                Id = o.Id,
                NamaMarketplace = o.NamaMarketplace,
                UpdateAt =o.UpdatedAt.Value.ToString("dd-MM-yyyy")
            })
            .OrderByDescending(x => x.Update_At)
            .ProjectTo<GetMarketplaceWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}