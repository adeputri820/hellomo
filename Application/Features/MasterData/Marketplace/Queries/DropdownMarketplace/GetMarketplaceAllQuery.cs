﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Queries.DropdownMarketplace
{
    public record GetMarketplaceAllQuery : IRequest<Result<List<GetMarketplaceAllDto>>>;

    internal class GetMarketplaceAllQueryHandler : IRequestHandler<GetMarketplaceAllQuery, Result<List<GetMarketplaceAllDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMarketplaceAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetMarketplaceAllDto>>> Handle(GetMarketplaceAllQuery query, CancellationToken cancellationToken)
        {
            var ekspedisi = await _unitOfWork.Repository<MasterMarketplace>().Entities
                            .ProjectTo<GetMarketplaceAllDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetMarketplaceAllDto>>.SuccessAsync(ekspedisi, "Successfully fetch data");
        }
    }
}