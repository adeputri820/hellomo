﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Queries.DropdownMarketplace
{
    public class GetMarketplaceAllDto : IMapFrom<MasterMarketplace>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_marketplace")]
        public string NamaMarketplace { get; set; }
    }
}