﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Marketplace
{
    public record MarketplaceDto
    {
        [JsonPropertyName("nama_marketplace")]
        public string NamaMarketplace { get; set; }
    }
    public sealed record CreateMarketplaceResponseDto : MarketplaceDto { }
}