﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.UpdateMarketplace
{
    public class MarketplaceUpdateEvent : BaseEvent
    {
        public MasterMarketplace MasterMarketplace { get; set; }

        public MarketplaceUpdateEvent(MasterMarketplace masterMarketplace)
        {
            MasterMarketplace = masterMarketplace;
        }
    }
}