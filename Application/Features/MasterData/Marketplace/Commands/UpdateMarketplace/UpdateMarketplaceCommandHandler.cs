﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.UpdateMarketplace
{
    internal class UpdateMarketplaceCommandHandler : IRequestHandler<UpdateMarketplaceRequest, Result<MasterMarketplace>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateMarketplaceCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<MasterMarketplace>> Handle(UpdateMarketplaceRequest request, CancellationToken cancellationToken)
        {
            var marketplcaeUpdate = await _unitOfWork.Repository<MasterMarketplace>().GetByIdAsync(request.Id);

            if (marketplcaeUpdate != null)
            {
                marketplcaeUpdate.NamaMarketplace = request.NamaMarketplace;
                marketplcaeUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterMarketplace>().UpdateAsync(marketplcaeUpdate);
                marketplcaeUpdate.AddDomainEvent(new MarketplaceUpdateEvent(marketplcaeUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<MasterMarketplace>.SuccessAsync(marketplcaeUpdate, "Marketplace updated");
            }
            return await Result<MasterMarketplace>.FailureAsync("Marketplace not found");
        }
    }
}