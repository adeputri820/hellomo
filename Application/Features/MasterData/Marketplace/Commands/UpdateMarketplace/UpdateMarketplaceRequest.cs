﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.UpdateMarketplace
{
    public class UpdateMarketplaceRequest : IRequest<Result<MasterMarketplace>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_marketplace")]
        public string NamaMarketplace { get; set; }
    }
}