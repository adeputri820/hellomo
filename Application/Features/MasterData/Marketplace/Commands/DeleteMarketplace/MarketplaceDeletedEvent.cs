﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.DeleteMarketplace
{
    public class MarketplaceDeletedEvent : BaseEvent
    {
        public MasterMarketplace MasterMarketplace { get; set; }

        public MarketplaceDeletedEvent(MasterMarketplace masterMarketplace)
        {
            MasterMarketplace = masterMarketplace;
        }
    }
}