﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.DeleteMarketplace
{
    internal class DeleteMarketplaceCommandHandler : IRequestHandler<DeleteMarketplaceRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteMarketplaceCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteMarketplaceRequest request, CancellationToken cancellationToken)
        {
            var marketplaceDelete = await _unitOfWork.Repository<MasterMarketplace>().GetByIdAsync(request.Id);
            if (marketplaceDelete != null)
            {
                marketplaceDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterMarketplace>().DeleteAsync(marketplaceDelete);
                marketplaceDelete.AddDomainEvent(new MarketplaceDeletedEvent(marketplaceDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(marketplaceDelete.Id, "Marketplace deleted.");
            }
            return await Result<Guid>.FailureAsync("Marketplace not found");
        }
    }
}