﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.DeleteMarketplace
{
    public class DeleteMarketplaceRequest : IRequest<Result<Guid>>, IMapFrom<MasterMarketplace>
    {
        public Guid Id { get; set; }

        public DeleteMarketplaceRequest(Guid id)
        {
            Id = id;
        }

        public DeleteMarketplaceRequest()
        {
        }
    }
}