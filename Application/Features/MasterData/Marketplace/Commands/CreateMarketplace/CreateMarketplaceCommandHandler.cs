﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.CreateMarketplace
{
    internal class CreateMarketplaceCommandHandler : IRequestHandler<CreateMarketplaceRequest, Result<CreateMarketplaceResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IMarketplaceRepository _marketplaceRepository;

        public CreateMarketplaceCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IMarketplaceRepository marketplaceRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _marketplaceRepository = marketplaceRepository;
        }

        public async Task<Result<CreateMarketplaceResponseDto>> Handle(CreateMarketplaceRequest request, CancellationToken cancellationToken)
        {
            var marketplace = _mapper.Map<MasterMarketplace>(request);

            var validateLokasi = await _marketplaceRepository.ValidateData(marketplace);
            if (validateLokasi == true)
            {
                return await Result<CreateMarketplaceResponseDto>.FailureAsync("Marketplace already exist.");
            }
            marketplace.NamaMarketplace = request.NamaMarketplace;
            marketplace.CreatedAt = DateTime.UtcNow;
            marketplace.UpdatedAt = DateTime.UtcNow;
            await _unitOfWork.Repository<MasterMarketplace>().AddAsync(marketplace);
            marketplace.AddDomainEvent(new MarketplaceCreatedEvent(marketplace));
            await _unitOfWork.Save(cancellationToken);
            var marketplaceResponse = _mapper.Map<CreateMarketplaceResponseDto>(marketplace);
            return await Result<CreateMarketplaceResponseDto>.SuccessAsync(marketplaceResponse, "Marketplace created.");
        }
    }
}