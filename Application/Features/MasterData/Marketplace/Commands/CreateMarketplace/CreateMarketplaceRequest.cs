﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.CreateMarketplace
{
    public class CreateMarketplaceRequest : IRequest<Result<CreateMarketplaceResponseDto>>
    {
        [JsonPropertyName("nama_marketplace")]
        public string NamaMarketplace { get; set; }
    }
}