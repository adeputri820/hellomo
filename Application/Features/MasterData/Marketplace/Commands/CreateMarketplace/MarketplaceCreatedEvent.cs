﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Marketplace.Commands.CreateMarketplace
{
    public class MarketplaceCreatedEvent : BaseEvent
    {
        public MasterMarketplace Marketplace { get; set; }

        public MarketplaceCreatedEvent(MasterMarketplace marketplace)
        {
            Marketplace = marketplace;
        }
    }
}