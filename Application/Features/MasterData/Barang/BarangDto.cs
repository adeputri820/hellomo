﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Barang
{
    public record BarangDto
    {
        [JsonPropertyName("sku")]
        public string Sku { get; set; }
        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }
        [JsonPropertyName("harga_default")]
        public int HargaDefault { get; set; }
    }
    public sealed record CreateBarangResponseDto : BarangDto { }
}