﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Barang.Queries.DropdownBarang
{
    public class GetBarangAllDto : IMapFrom<GetBarangAllDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("varian")]
        public string Varian { get; set; }
    }
}