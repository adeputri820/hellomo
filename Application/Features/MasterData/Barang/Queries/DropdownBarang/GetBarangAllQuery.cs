﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Barang.Queries.DropdownBarang
{
    public record GetBarangAllQuery : IRequest<Result<List<GetBarangAllDto>>>;

    internal class GetBarangAllQueryHandler : IRequestHandler<GetBarangAllQuery, Result<List<GetBarangAllDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBarangAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetBarangAllDto>>> Handle(GetBarangAllQuery query, CancellationToken cancellationToken)
        {
            var barang = await _unitOfWork.Repository<MasterBarang>().Entities.Select(p => new GetBarangAllDto
            {
                Id = p.Id,
                Sku = p.Sku,
                NamaBarang = p.NamaBarang,
                Varian = p.Varian.NamaVarian,
            })
            .ProjectTo<GetBarangAllDto>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);

            return await Result<List<GetBarangAllDto>>.SuccessAsync(barang, "Successfully fetch data");
        }
    }
}