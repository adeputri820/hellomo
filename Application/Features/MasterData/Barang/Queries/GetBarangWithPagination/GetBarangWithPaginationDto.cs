﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Barang.Queries.GetBarangWithPagination
{
    public class GetBarangWithPaginationDto : IMapFrom<GetBarangWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("id_varian")]
        public Guid IdVarian { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("harga_default")]
        public int HargaDefault { get; set; }

        [JsonPropertyName("varian")]
        public string Varian { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdateAt { get; set; }
        public DateTime Update_At { get; set; }
    }
}