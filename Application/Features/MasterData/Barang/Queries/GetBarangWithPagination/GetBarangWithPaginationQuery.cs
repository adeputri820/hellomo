﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Barang.Queries.GetBarangWithPagination
{
    public record GetBarangWithPaginationQuery : IRequest<PaginatedResult<GetBarangWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }

        public GetBarangWithPaginationQuery(int pageNumber, int pageSize, string? searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
        public GetBarangWithPaginationQuery()
        {
            
        }
    }

    internal class GetBarangWithPaginationQueryHandler : IRequestHandler<GetBarangWithPaginationQuery, PaginatedResult<GetBarangWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBarangWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetBarangWithPaginationDto>> Handle(GetBarangWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterBarang>()
            .FindByCondition(x => (query.SearchTerm == null) || (query.SearchTerm.ToLower() == x.NamaBarang.ToLower())
            || (query.SearchTerm.ToLower() == x.Sku.ToLower()) || (query.SearchTerm.ToLower() == x.Varian.NamaVarian.ToLower()))
            .Select(p => new GetBarangWithPaginationDto
            {
                Id = p.Id,
                IdVarian = p.IdVarian,
                Sku = p.Sku,
                NamaBarang = p.NamaBarang,
                Varian = p.Varian.NamaVarian,
                HargaDefault = p.HargaDefault,
                UpdateAt = p.UpdatedAt.Value.ToString("dd-MM-yyyy"),
                Update_At = p.UpdatedAt.Value
            })
            .OrderByDescending(x => x.Update_At)
            .ProjectTo<GetBarangWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}