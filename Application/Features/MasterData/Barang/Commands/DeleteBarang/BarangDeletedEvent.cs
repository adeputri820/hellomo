﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.DeleteBarang
{
    public class BarangDeletedEvent : BaseEvent
    {
        public MasterBarang MasterBarang { get; set; }

        public BarangDeletedEvent(MasterBarang masterBarang)
        {
            MasterBarang = masterBarang;
        }
    }
}