﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.DeleteBarang
{
    public class DeleteBarangRequest : IRequest<Result<Guid>>, IMapFrom<MasterBarang>
    {
        public Guid Id { get; set; }

        public DeleteBarangRequest(Guid id)
        {
            Id = id;
        }

        public DeleteBarangRequest()
        {
        }
    }
}