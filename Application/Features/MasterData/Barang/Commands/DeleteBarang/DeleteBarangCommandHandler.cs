﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.DeleteBarang
{
    internal class DeleteBarangCommandHandler : IRequestHandler<DeleteBarangRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteBarangCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteBarangRequest request, CancellationToken cancellationToken)
        {
            var barangDelete = await _unitOfWork.Repository<MasterBarang>().GetByIdAsync(request.Id);
            if (barangDelete != null)
            {
                barangDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterBarang>().DeleteAsync(barangDelete);
                barangDelete.AddDomainEvent(new BarangDeletedEvent(barangDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(barangDelete.Id, "Barang deleted.");
            }
            return await Result<Guid>.FailureAsync("Barang not found");
        }
    }
}