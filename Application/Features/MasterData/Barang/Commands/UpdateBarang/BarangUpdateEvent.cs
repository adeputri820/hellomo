﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.UpdateBarang
{
    public class BarangUpdateEvent : BaseEvent
    {
        public MasterBarang MasterBarang { get; set; }

        public BarangUpdateEvent(MasterBarang masterBarang)
        {
            MasterBarang = masterBarang;
        }
    }
}