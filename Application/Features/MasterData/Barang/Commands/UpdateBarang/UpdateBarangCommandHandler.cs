﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.UpdateBarang
{
    internal class UpdateBarangCommandHandler : IRequestHandler<UpdateBarangRequest, Result<MasterBarang>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateBarangCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<MasterBarang>> Handle(UpdateBarangRequest request, CancellationToken cancellationToken)
        {
            var barangUpdate = await _unitOfWork.Repository<MasterBarang>().GetByIdAsync(request.Id);

            if (barangUpdate != null)
            {
                barangUpdate.IdVarian = request.IdVarian;
                barangUpdate.Sku = request.Sku;
                barangUpdate.NamaBarang = request.NamaBarang;
                barangUpdate.HargaDefault = request.HargaDefault;
                barangUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterBarang>().UpdateAsync(barangUpdate);
                barangUpdate.AddDomainEvent(new BarangUpdateEvent(barangUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<MasterBarang>.SuccessAsync(barangUpdate, "Barang updated");
            }
            return await Result<MasterBarang>.FailureAsync("Barang not found");
        }
    }
}