﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.UpdateBarang
{
    public class UpdateBarangRequest : IRequest<Result<MasterBarang>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("id_varian")]
        public Guid IdVarian { get; set; }


        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("harga_default")]
        public int HargaDefault { get; set; }
    }
}