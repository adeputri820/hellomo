﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.CreateBarang
{
    internal class CreateBarangCommandHandler : IRequestHandler<CreateBarangRequest, Result<CreateBarangResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IBarangRepository _barangRepository;

        public CreateBarangCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IBarangRepository ekspedisiRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _barangRepository = ekspedisiRepository;
        }

        public async Task<Result<CreateBarangResponseDto>> Handle(CreateBarangRequest request, CancellationToken cancellationToken)
        {
            var barang =  _mapper.Map<MasterBarang>(request);

            var validateLokasi = await _barangRepository.ValidateData(barang);
            if (validateLokasi != false)
            {
                return await Result<CreateBarangResponseDto>.FailureAsync("Barang already exist.");
            }
            barang.IdVarian = request.IdVarian;
            barang.Sku = request.Sku;
            barang.NamaBarang = request.NamaBarang;
            barang.HargaDefault = request.HargaDefault;
            barang.CreatedAt = DateTime.UtcNow;
            barang.UpdatedAt = DateTime.UtcNow;
            
            await _unitOfWork.Repository<MasterBarang>().AddAsync(barang);
            barang.AddDomainEvent(new BarangCreatedEvent(barang));
            await _unitOfWork.Save(cancellationToken);
            var barangResponse = _mapper.Map<CreateBarangResponseDto>(barang);
            return await Result<CreateBarangResponseDto>.SuccessAsync(barangResponse, "Barang created.");
        }
    }
}