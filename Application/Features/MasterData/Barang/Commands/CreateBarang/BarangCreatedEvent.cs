﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.CreateBarang
{
    public class BarangCreatedEvent : BaseEvent
    {
        public MasterBarang MasterBarang { get; set; }

        public BarangCreatedEvent(MasterBarang masterBarang)
        {
            MasterBarang = masterBarang;
        }
    }
}