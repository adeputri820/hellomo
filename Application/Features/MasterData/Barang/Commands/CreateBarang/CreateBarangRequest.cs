﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Barang.Commands.CreateBarang
{
    public class CreateBarangRequest : IRequest<Result<CreateBarangResponseDto>>
    {
        [JsonPropertyName("id_varian")]
        public Guid IdVarian { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("harga_default")]
        public int HargaDefault { get; set; }
    }
}