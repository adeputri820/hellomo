﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Varian.Queries.DropdownVarian
{
    public record GetVarianAllQuery : IRequest<Result<List<GetVarianAllDto>>>;

    internal class GetVarianAllQueryHandler : IRequestHandler<GetVarianAllQuery, Result<List<GetVarianAllDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetVarianAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetVarianAllDto>>> Handle(GetVarianAllQuery query, CancellationToken cancellationToken)
        {
            var usedVarianIds = await _unitOfWork.Repository<MasterBarang>()
                     .Entities
                     .Select(b => b.IdVarian)
                     .ToListAsync(cancellationToken);
            var varian = await _unitOfWork.Repository<MasterVarian>()
                .Entities
                .Where(v => !usedVarianIds.Contains(v.Id))
                .ProjectTo<GetVarianAllDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
            return await Result<List<GetVarianAllDto>>.SuccessAsync(varian, "Successfully fetch data");
        }
    }
}