﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Varian.Queries.DropdownVarian
{
    public class GetVarianAllDto : IMapFrom<MasterVarian>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("varian")]
        public string NamaVarian { get; set; }
    }
}