﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Varian.Queries.GetVarianWithPagination
{
    public record GetVarianWithPaginationQuery : IRequest<PaginatedResult<GetVarianWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetVarianWithPaginationQuery()
        {
        }

        public GetVarianWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetVarianWithPaginationQueryHandler : IRequestHandler<GetVarianWithPaginationQuery, PaginatedResult<GetVarianWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetVarianWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetVarianWithPaginationDto>> Handle(GetVarianWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterVarian>().FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.NamaVarian.ToLower())
            .Select(o => new GetVarianWithPaginationDto
            {
                Id = o.Id,
                NamaVarian = o.NamaVarian,
                UpdateAt = o.UpdatedAt.Value.ToString("dd-MM-yyyy"),
                Update_At = o.UpdatedAt.Value
            })
            .OrderByDescending(o => o.Update_At)
            .ProjectTo<GetVarianWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}