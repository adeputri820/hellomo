﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Varian.Queries.GetVarianWithPagination
{
    public class GetVarianWithPaginationDto : IMapFrom<GetVarianWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("varian")]
        public string NamaVarian { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdateAt { get; set; }
        public DateTime Update_At { get; set; }
    }
}