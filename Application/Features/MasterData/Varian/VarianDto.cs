﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Varian
{
    public record VarianDto
    {
        [JsonPropertyName("varian")]
        public string NamaVarian { get; set; }
    }
    public sealed record CreateVarianResponseDto : VarianDto { }
}