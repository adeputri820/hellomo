﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.UpdateVarian
{
    public class VarianUpdateEvent : BaseEvent
    {
        public MasterVarian MasterVarian { get; set; }

        public VarianUpdateEvent(MasterVarian masterVarian)
        {
            MasterVarian = masterVarian;
        }
    }
}