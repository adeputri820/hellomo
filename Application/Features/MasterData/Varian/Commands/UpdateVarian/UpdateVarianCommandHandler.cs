﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.UpdateVarian
{
    internal class UpdateVarianCommandHandler : IRequestHandler<UpdateVarianRequest, Result<MasterVarian>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateVarianCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<MasterVarian>> Handle(UpdateVarianRequest request, CancellationToken cancellationToken)
        {
            var varianUpdate = await _unitOfWork.Repository<MasterVarian>().GetByIdAsync(request.Id);
            if (varianUpdate != null)
            {
                varianUpdate.NamaVarian = request.namaVarian;
                varianUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterVarian>().UpdateAsync(varianUpdate);
                varianUpdate.AddDomainEvent(new VarianUpdateEvent(varianUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<MasterVarian>.SuccessAsync(varianUpdate, "Varian updated");
            }
            return await Result<MasterVarian>.FailureAsync("Varian not found");
        }
    }
}