﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.UpdateVarian
{
    public class UpdateVarianRequest : IRequest<Result<MasterVarian>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("varian")]
        public string namaVarian { get; set; }
    }
}