﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.CreateVarian
{
    internal class CreateVarianCommandHandler : IRequestHandler<CreateVarianRequest, Result<CreateVarianResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IVarianRepository _varianRepository;

        public CreateVarianCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IVarianRepository varianRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _varianRepository = varianRepository;
        }

        public async Task<Result<CreateVarianResponseDto>> Handle(CreateVarianRequest request, CancellationToken cancellationToken)
        {
            var varian = _mapper.Map<MasterVarian>(request);

            var validateVarian = await _varianRepository.ValidateData(varian);
            if (validateVarian == true)
            {
                return await Result<CreateVarianResponseDto>.FailureAsync("Varian already exist.");
            }
            varian.NamaVarian = request.NamaVarian;
            varian.CreatedAt = DateTime.UtcNow;
            varian.UpdatedAt = DateTime.UtcNow;
            await _unitOfWork.Repository<MasterVarian>().AddAsync(varian);
            varian.AddDomainEvent(new VarianCreatedEvent(varian));
            await _unitOfWork.Save(cancellationToken);
            var varianResponse = _mapper.Map<CreateVarianResponseDto>(varian);
            return await Result<CreateVarianResponseDto>.SuccessAsync(varianResponse, "Varian created.");
        }
    }
}