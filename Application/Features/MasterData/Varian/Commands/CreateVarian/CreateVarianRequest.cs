﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.CreateVarian
{
    public class CreateVarianRequest : IRequest<Result<CreateVarianResponseDto>>
    {
        [JsonPropertyName("varian")]
        public string NamaVarian { get; set; }
    }
}