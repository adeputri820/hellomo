﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.CreateVarian
{
    public class VarianCreatedEvent : BaseEvent
    {
        public MasterVarian MasterVarian { get; set; }

        public VarianCreatedEvent(MasterVarian masterVarian)
        {
            MasterVarian = masterVarian;
        }
    }
}