﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.DeleteVarian
{
    public class DeleteVarianRequest : IRequest<Result<Guid>>, IMapFrom<MasterVarian>
    {
        public Guid Id { get; set; }

        public DeleteVarianRequest()
        {
        }

        public DeleteVarianRequest(Guid id)
        {
            Id = id;
        }
    }
}