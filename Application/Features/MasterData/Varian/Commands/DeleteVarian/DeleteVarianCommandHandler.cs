﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.DeleteVarian
{
    internal class DeleteVarianCommandHandler : IRequestHandler<DeleteVarianRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteVarianCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteVarianRequest request, CancellationToken cancellationToken)
        {
            var rakDelete = await _unitOfWork.Repository<MasterVarian>().GetByIdAsync(request.Id);
            if (rakDelete != null)
            {
                rakDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterVarian>().DeleteAsync(rakDelete);
                rakDelete.AddDomainEvent(new VarianDeletedEvent(rakDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(rakDelete.Id, "Varian deleted.");
            }
            return await Result<Guid>.FailureAsync("Varian not found");
        }
    }
}