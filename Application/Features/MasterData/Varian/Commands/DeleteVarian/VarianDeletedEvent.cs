﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Varian.Commands.DeleteVarian
{
    public class VarianDeletedEvent : BaseEvent
    {
        public MasterVarian MasterVarian { get; set; }

        public VarianDeletedEvent(MasterVarian masterVarian)
        {
            MasterVarian = masterVarian;
        }
    }
}