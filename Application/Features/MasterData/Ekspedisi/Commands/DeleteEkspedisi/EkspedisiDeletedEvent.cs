﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.DeleteEkspedisi
{
    public class EkspedisiDeletedEvent : BaseEvent
    {
        public MasterEkspedisi MasterEkspedisi { get; set; }

        public EkspedisiDeletedEvent(MasterEkspedisi masterEkspedisi)
        {
            MasterEkspedisi = masterEkspedisi;
        }
    }
}