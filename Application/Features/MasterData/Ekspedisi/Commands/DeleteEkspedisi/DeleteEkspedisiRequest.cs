﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.DeleteEkspedisi
{
    public class DeleteEkspedisiRequest : IRequest<Result<Guid>>, IMapFrom<MasterEkspedisi>
    {
        public Guid Id { get; set; }

        public DeleteEkspedisiRequest()
        {
        }

        public DeleteEkspedisiRequest(Guid id)
        {
            Id = id;
        }
    }
}