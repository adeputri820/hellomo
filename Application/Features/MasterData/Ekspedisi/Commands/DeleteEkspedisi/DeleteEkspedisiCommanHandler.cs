﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.DeleteEkspedisi
{
    internal class DeleteEkspedisiCommanHandler : IRequestHandler<DeleteEkspedisiRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteEkspedisiCommanHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteEkspedisiRequest request, CancellationToken cancellationToken)
        {
            var ekspedisiDelete = await _unitOfWork.Repository<MasterEkspedisi>().GetByIdAsync(request.Id);
            if (ekspedisiDelete != null)
            {
                ekspedisiDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterEkspedisi>().DeleteAsync(ekspedisiDelete);
                ekspedisiDelete.AddDomainEvent(new EkspedisiDeletedEvent(ekspedisiDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(ekspedisiDelete.Id, "Ekspedisi deleted.");
            }
            return await Result<Guid>.FailureAsync("Ekspedisi not found");
        }
    }
}