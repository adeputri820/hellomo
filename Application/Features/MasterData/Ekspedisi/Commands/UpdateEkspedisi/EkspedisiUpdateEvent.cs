﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.UpdateEkspedisi
{
    public class EkspedisiUpdateEvent : BaseEvent
    {
        public MasterEkspedisi MasterEkspedisi { get; set; }

        public EkspedisiUpdateEvent(MasterEkspedisi masterEkspedisi)
        {
            MasterEkspedisi = masterEkspedisi;
        }
    }
}