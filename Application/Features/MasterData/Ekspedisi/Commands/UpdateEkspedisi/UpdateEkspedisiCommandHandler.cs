﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.UpdateEkspedisi
{
    internal class UpdateEkspedisiCommandHandler : IRequestHandler<UpdateEkspedisiRequest, Result<MasterEkspedisi>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateEkspedisiCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<MasterEkspedisi>> Handle(UpdateEkspedisiRequest request, CancellationToken cancellationToken)
        {
            var ekspedisiUpdate = await _unitOfWork.Repository<MasterEkspedisi>().GetByIdAsync(request.Id);

            if (ekspedisiUpdate != null)
            {
                ekspedisiUpdate.NamaEkspedisi = request.NamaEkspedisi;
                ekspedisiUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterEkspedisi>().UpdateAsync(ekspedisiUpdate);
                ekspedisiUpdate.AddDomainEvent(new EkspedisiUpdateEvent(ekspedisiUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<MasterEkspedisi>.SuccessAsync(ekspedisiUpdate, "Ekspedisi updated");
            }
            return await Result<MasterEkspedisi>.FailureAsync("Ekspedisi not found");
        }
    }
}