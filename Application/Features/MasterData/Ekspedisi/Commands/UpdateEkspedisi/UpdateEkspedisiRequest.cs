﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.UpdateEkspedisi
{
    public class UpdateEkspedisiRequest : IRequest<Result<MasterEkspedisi>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_ekspedisi")]
        public string NamaEkspedisi { get; set; }
    }
}