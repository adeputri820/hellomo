﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.CreateEkspedisi
{
    public class EkspedisiCreatedEvent : BaseEvent
    {
        public MasterEkspedisi MasterEkspedisi { get; set; }

        public EkspedisiCreatedEvent(MasterEkspedisi masterEkspedisi)
        {
            MasterEkspedisi = masterEkspedisi;
        }
    }
}