﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.CreateEkspedisi
{
    internal class CreateEkspedisiCommandHandler : IRequestHandler<CreateEkspedisiRequest, Result<CreateResponseEkspedisiDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEkspedisiRepository _ekspedisiRepository;

        public CreateEkspedisiCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IEkspedisiRepository ekspedisiRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ekspedisiRepository = ekspedisiRepository;
        }

        public async Task<Result<CreateResponseEkspedisiDto>> Handle(CreateEkspedisiRequest request, CancellationToken cancellationToken)
        {
            var ekspedisi = _mapper.Map<MasterEkspedisi>(request);

            var validateEkspedisi = await _ekspedisiRepository.ValidateData(ekspedisi);
            if (validateEkspedisi == true)
            {
                return await Result<CreateResponseEkspedisiDto>.FailureAsync("Ekspedisi already exist.");
            }
            ekspedisi.NamaEkspedisi = request.NamaEkspedisi;
            ekspedisi.CreatedAt = DateTime.UtcNow;
            ekspedisi.UpdatedAt = DateTime.UtcNow;
            await _unitOfWork.Repository<MasterEkspedisi>().AddAsync(ekspedisi);
            ekspedisi.AddDomainEvent(new EkspedisiCreatedEvent(ekspedisi));
            await _unitOfWork.Save(cancellationToken);
            var ekspedisiResponse = _mapper.Map<CreateResponseEkspedisiDto>(ekspedisi);
            return await Result<CreateResponseEkspedisiDto>.SuccessAsync(ekspedisiResponse, "Ekspedisi created.");
        }
    }
}