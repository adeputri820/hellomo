﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Commands.CreateEkspedisi
{
    public class CreateEkspedisiRequest : IRequest<Result<CreateResponseEkspedisiDto>>
    {
        [JsonPropertyName("nama_ekspedisi")]
        public string NamaEkspedisi { get; set; }
    }
}