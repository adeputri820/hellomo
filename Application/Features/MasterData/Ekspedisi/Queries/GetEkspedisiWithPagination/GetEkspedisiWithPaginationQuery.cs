﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Queries.GetEkspedisiWithPagination
{
    public record GetEkspedisiWithPaginationQuery : IRequest<PaginatedResult<GetEkspedisiWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetEkspedisiWithPaginationQuery()
        {
        }

        public GetEkspedisiWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetEkspedisiWithPaginationQueryHandler : IRequestHandler<GetEkspedisiWithPaginationQuery, PaginatedResult<GetEkspedisiWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetEkspedisiWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetEkspedisiWithPaginationDto>> Handle(GetEkspedisiWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterEkspedisi>().FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.NamaEkspedisi.ToLower())
            .Select(o => new GetEkspedisiWithPaginationDto
            {
                Id = o.Id,
                NamaEkspedisi = o.NamaEkspedisi,
                UpdatedAt = o.UpdatedAt.Value.ToString("dd-MM-yyyy"),
                Updated_At = o.UpdatedAt.Value
            })
            .OrderByDescending(x => x.Updated_At)
            .ProjectTo<GetEkspedisiWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}