﻿using FluentValidation;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Queries.GetEkspedisiWithPagination
{
    public class GetEkspedisiWithPaginationValidator : AbstractValidator<GetEkspedisiWithPaginationQuery>
    {
        public GetEkspedisiWithPaginationValidator()
        {
            RuleFor(x => x.PageNumber)
               .GreaterThanOrEqualTo(1)
               .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}