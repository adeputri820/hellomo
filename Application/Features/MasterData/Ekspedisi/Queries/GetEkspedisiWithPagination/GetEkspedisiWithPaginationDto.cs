﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Queries.GetEkspedisiWithPagination
{
    public class GetEkspedisiWithPaginationDto : IMapFrom<GetEkspedisiWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_ekspedisi")]
        public string NamaEkspedisi { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdatedAt { get; set; }
        public DateTime Updated_At { get; set; }
    }
}