﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Queries.DropdownEkspedisi
{
    public record GetEkspedisiAllQuery : IRequest<Result<List<GetEkspedisiAllDto>>>;

    internal class GetEkspedisiAllQueryHandler : IRequestHandler<GetEkspedisiAllQuery, Result<List<GetEkspedisiAllDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetEkspedisiAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetEkspedisiAllDto>>> Handle(GetEkspedisiAllQuery query, CancellationToken cancellationToken)
        {
            var ekspedisi = await _unitOfWork.Repository<MasterEkspedisi>().Entities
                            .ProjectTo<GetEkspedisiAllDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetEkspedisiAllDto>>.SuccessAsync(ekspedisi, "Successfully fetch data");
        }
    }
}