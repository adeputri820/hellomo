﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi.Queries.DropdownEkspedisi
{
    public class GetEkspedisiAllDto : IMapFrom<MasterEkspedisi>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_ekspedisi")]
        public string NamaEkspedisi { get; set; }
    }
}