﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Ekspedisi
{
    public record EkspedisiDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("nama_ekspedisi")]
        public string NamaEkspedisi { get; set; }
    }
    public sealed record CreateResponseEkspedisiDto : EkspedisiDto { }
}