﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Queries.DropdownSupplier
{
    public class GetSupplierAllDto : IMapFrom<MasterSupplier>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_supplier")]
        public string NamaSupplier { get; set; }
    }
}