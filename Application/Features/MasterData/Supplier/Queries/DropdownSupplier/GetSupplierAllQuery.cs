﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Queries.DropdownSupplier
{
    public record GetSupplierAllQuery : IRequest<Result<List<GetSupplierAllDto>>>;

    internal class GetSupplierAllQueryHandler : IRequestHandler<GetSupplierAllQuery, Result<List<GetSupplierAllDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetSupplierAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetSupplierAllDto>>> Handle(GetSupplierAllQuery query, CancellationToken cancellationToken)
        {
            var supplier = await _unitOfWork.Repository<MasterSupplier>().Entities
                            .ProjectTo<GetSupplierAllDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetSupplierAllDto>>.SuccessAsync(supplier, "Successfully fetch data");
        }
    }
}