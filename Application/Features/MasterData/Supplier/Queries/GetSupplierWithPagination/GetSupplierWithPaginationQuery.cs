﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Queries.GetSupplierWithPagination
{
    public record GetSupplierWithPaginationQuery : IRequest<PaginatedResult<GetSupplierWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetSupplierWithPaginationQuery()
        {
        }
        public GetSupplierWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetSupplierWithPaginationQueryHandler : IRequestHandler<GetSupplierWithPaginationQuery, PaginatedResult<GetSupplierWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetSupplierWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetSupplierWithPaginationDto>> Handle(GetSupplierWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterSupplier>().FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.NamaSupplier.ToLower())
            .Select(o => new GetSupplierWithPaginationDto
            {
                Id = o.Id,
                NamaSupplier = o.NamaSupplier,
                UpdateAt = o.UpdatedAt.Value.ToString("dd-MM-yyyy")
            })
            .OrderByDescending(o => o.Update_At)
            .ProjectTo<GetSupplierWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}