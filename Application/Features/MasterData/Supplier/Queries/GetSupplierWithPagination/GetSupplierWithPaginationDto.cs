﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Queries.GetSupplierWithPagination
{
    public class GetSupplierWithPaginationDto : IMapFrom<GetSupplierWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_supplier")]
        public string NamaSupplier { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdateAt { get; set; }
        public DateTime Update_At { get; set; }
    }
}