﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.CreateSupplier
{
    public class CreateSupplierRequest : IRequest<Result<CreateSupplierResponseDto>>
    {
        [JsonPropertyName("nama_supplier")]
        public string NamaSupplier { get; set; }
    }
}