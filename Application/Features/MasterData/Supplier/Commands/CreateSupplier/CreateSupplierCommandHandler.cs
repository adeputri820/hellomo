﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.CreateSupplier
{
    internal class CreateSupplierCommandHandler : IRequestHandler<CreateSupplierRequest, Result<CreateSupplierResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISupplierRepository _supplierRepository;

        public CreateSupplierCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ISupplierRepository supplierRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _supplierRepository = supplierRepository;
        }

        public async Task<Result<CreateSupplierResponseDto>> Handle(CreateSupplierRequest request, CancellationToken cancellationToken)
        {
            var supplier = _mapper.Map<MasterSupplier>(request);

            var validateSupplier = await _supplierRepository.ValidateData(supplier);
            if (validateSupplier == true)
            {
                return await Result<CreateSupplierResponseDto>.FailureAsync("Supplier already exist.");
            }
            supplier.NamaSupplier = request.NamaSupplier;
            supplier.CreatedAt = DateTime.UtcNow;
            supplier.UpdatedAt = DateTime.UtcNow;
            await _unitOfWork.Repository<MasterSupplier>().AddAsync(supplier);
            supplier.AddDomainEvent(new SupplierCreatedEvent(supplier));
            await _unitOfWork.Save(cancellationToken);
            var supplierResponse = _mapper.Map<CreateSupplierResponseDto>(supplier);
            return await Result<CreateSupplierResponseDto>.SuccessAsync(supplierResponse, "Supplier created.");
        }
    }
}