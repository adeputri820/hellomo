﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.CreateSupplier
{
    public class SupplierCreatedEvent : BaseEvent
    {
        public MasterSupplier MasterMarketplace { get; set; }

        public SupplierCreatedEvent(MasterSupplier masterMarketplace)
        {
            MasterMarketplace = masterMarketplace;
        }
    }
}