﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.DeleteSupplier
{
    public class DeleteSupplierRequest : IRequest<Result<Guid>>, IMapFrom<MasterSupplier>
    {
        public Guid Id { get; set; }

        public DeleteSupplierRequest(Guid id)
        {
            Id = id;
        }

        public DeleteSupplierRequest()
        {
        }
    }
}