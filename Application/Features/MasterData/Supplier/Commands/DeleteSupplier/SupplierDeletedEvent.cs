﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.DeleteSupplier
{
    public class SupplierDeletedEvent : BaseEvent
    {
        public MasterSupplier MasterSupplier { get; set; }

        public SupplierDeletedEvent(MasterSupplier supplier)
        {
            MasterSupplier = supplier;
        }
    }
}