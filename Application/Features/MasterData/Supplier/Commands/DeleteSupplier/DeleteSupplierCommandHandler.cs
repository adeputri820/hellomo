﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.DeleteSupplier
{
    internal class DeleteSupplierCommandHandler : IRequestHandler<DeleteSupplierRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteSupplierCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteSupplierRequest request, CancellationToken cancellationToken)
        {
            var supplierDelete = await _unitOfWork.Repository<MasterSupplier>().GetByIdAsync(request.Id);
            if (supplierDelete != null)
            {
                supplierDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterSupplier>().DeleteAsync(supplierDelete);
                supplierDelete.AddDomainEvent(new SupplierDeletedEvent(supplierDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(supplierDelete.Id, "Supplier deleted.");
            }
            return await Result<Guid>.FailureAsync("Supplier not found");
        }
    }
}