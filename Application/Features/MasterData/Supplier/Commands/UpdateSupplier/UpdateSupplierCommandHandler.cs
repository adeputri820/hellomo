﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.UpdateSupplier
{
    internal class UpdateSupplierCommandHandler : IRequestHandler<UpdateSupplierRequest, Result<MasterSupplier>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateSupplierCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<MasterSupplier>> Handle(UpdateSupplierRequest request, CancellationToken cancellationToken)
        {
            var rakUpdate = await _unitOfWork.Repository<MasterSupplier>().GetByIdAsync(request.Id);
            if (rakUpdate != null)
            {
                rakUpdate.NamaSupplier = request.NamaSupplier;
                rakUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterSupplier>().UpdateAsync(rakUpdate);
                rakUpdate.AddDomainEvent(new SupplierUpdateEvent(rakUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<MasterSupplier>.SuccessAsync(rakUpdate, "Supplier updated");
            }
            return await Result<MasterSupplier>.FailureAsync("Supplier not found");
        }
    }
}