﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.UpdateSupplier
{
    public class UpdateSupplierRequest : IRequest<Result<MasterSupplier>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_supplier")]
        public string NamaSupplier { get; set; }
    }
}