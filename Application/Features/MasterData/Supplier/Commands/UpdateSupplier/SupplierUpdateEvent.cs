﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Supplier.Commands.UpdateSupplier
{
    public class SupplierUpdateEvent : BaseEvent
    {
        public MasterSupplier MasterSupplier { get; set; }

        public SupplierUpdateEvent(MasterSupplier masterSupplier)
        {
            MasterSupplier = masterSupplier;
        }
    }
}