﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Supplier
{
    public record SupplierDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("nama_supplier")]
        public string NamaSupplier { get; set; }
    }
    public sealed record CreateSupplierResponseDto : SupplierDto { }
}