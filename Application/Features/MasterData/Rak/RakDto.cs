﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Rak
{
    public record RakDto
    {
        [JsonPropertyName("no_rak")]
        public string NomorRak { get; set; }
    }
    public sealed record CreateRakResponseDto : RakDto { }
}