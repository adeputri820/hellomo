﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Rak.Queries.GetRakWithPagination
{
    public record GetRakWithPaginationQuery : IRequest<PaginatedResult<GetRakWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetRakWithPaginationQuery()
        {
        }

        public GetRakWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetRakWithPaginationQueryHandler : IRequestHandler<GetRakWithPaginationQuery, PaginatedResult<GetRakWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetRakWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetRakWithPaginationDto>> Handle(GetRakWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterRak>().FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.NomorRak.ToLower())
            .OrderBy(o => o.NomorUrut).Select(p => new GetRakWithPaginationDto
            {
                Id = p.Id,
                NomorRak = p.NomorRak,
                UpdateAt = p.UpdatedAt.Value.ToString("dd-MM-yyyy")
            })
            .ProjectTo<GetRakWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}