﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Rak.Queries.GetRakWithPagination
{
    public class GetRakWithPaginationDto : IMapFrom<GetRakWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("no_rak")]
        public string NomorRak { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdateAt { get; set; }
    }
}