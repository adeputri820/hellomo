﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Rak.Queries.DropdownRak
{
    public class GetRakAllDto : IMapFrom<MasterRak>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nomor_rak")]
        public string NomorRak { get; set; }
    }
}