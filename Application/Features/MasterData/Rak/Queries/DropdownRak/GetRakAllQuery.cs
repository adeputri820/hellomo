﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Rak.Queries.DropdownRak
{
    public record GetRakAllQuery : IRequest<Result<List<GetRakAllDto>>>;

    internal class GetRakAllQueryHandler : IRequestHandler<GetRakAllQuery, Result<List<GetRakAllDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetRakAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetRakAllDto>>> Handle(GetRakAllQuery query, CancellationToken cancellationToken)
        {
            var rak = await _unitOfWork.Repository<MasterRak>().Entities.OrderBy(o => o.NomorUrut).ThenBy(p => p.NomorRak)
                            .ProjectTo<GetRakAllDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetRakAllDto>>.SuccessAsync(rak, "Successfully fetch data");
        }
    }
}