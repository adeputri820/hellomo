﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.DeleteRak
{
    public class RakDeletedEvent : BaseEvent
    {
        public MasterRak MasterRak { get; set; }

        public RakDeletedEvent(MasterRak masterRak)
        {
            MasterRak = masterRak;
        }
    }
}