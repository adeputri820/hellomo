﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.DeleteRak
{
    public class DeleteRakRequest : IRequest<Result<Guid>>, IMapFrom<MasterRak>
    {
        public Guid Id { get; set; }

        public DeleteRakRequest(Guid id)
        {
            Id = id;
        }

        public DeleteRakRequest()
        {
        }
    }
}