﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.DeleteRak
{
    internal class DeleteRakCommandHandler : IRequestHandler<DeleteRakRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteRakCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteRakRequest request, CancellationToken cancellationToken)
        {
            var rakDelete = await _unitOfWork.Repository<MasterRak>().GetByIdAsync(request.Id);
            if (rakDelete != null)
            {
                rakDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterRak>().DeleteAsync(rakDelete);
                rakDelete.AddDomainEvent(new RakDeletedEvent(rakDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(rakDelete.Id, "Rak deleted.");
            }
            return await Result<Guid>.FailureAsync("Rak not found");
        }
    }
}