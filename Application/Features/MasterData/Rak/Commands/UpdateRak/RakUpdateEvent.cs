﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.UpdateRak
{
    public class RakUpdateEvent : BaseEvent
    {
        public MasterRak MasterRak { get; set; }

        public RakUpdateEvent(MasterRak masterRak)
        {
            MasterRak = masterRak;
        }
    }
}