﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.UpdateRak
{
    public class UpdateRakRequest : IRequest<Result<MasterRak>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("no_rak")]
        public string NomorRak { get; set; }
    }
}