﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.UpdateRak
{
    internal class UpdateRakCommandHandler : IRequestHandler<UpdateRakRequest, Result<MasterRak>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateRakCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<MasterRak>> Handle(UpdateRakRequest request, CancellationToken cancellationToken)
        {
            var rakUpdate = await _unitOfWork.Repository<MasterRak>().GetByIdAsync(request.Id);
            if (rakUpdate != null)
            {
                rakUpdate.NomorRak = request.NomorRak;
                rakUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterRak>().UpdateAsync(rakUpdate);
                rakUpdate.AddDomainEvent(new RakUpdateEvent(rakUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<MasterRak>.SuccessAsync(rakUpdate, "Nomor rak updated");
            }
            return await Result<MasterRak>.FailureAsync("Nomor rak not found");
        }
    }
}