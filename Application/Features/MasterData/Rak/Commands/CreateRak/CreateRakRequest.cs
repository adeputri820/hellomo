﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.CreateRak
{
    public class CreateRakRequest : IRequest<Result<CreateRakResponseDto>>
    {
        [JsonPropertyName("no_rak")]
        public string NomorRak { get; set; }
    }
}