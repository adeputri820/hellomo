﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.CreateRak
{
    public class RakCreatedEvent : BaseEvent
    {
        public MasterRak MasterRak { get; set; }

        public RakCreatedEvent(MasterRak masterRak)
        {
            MasterRak = masterRak;
        }
    }
}