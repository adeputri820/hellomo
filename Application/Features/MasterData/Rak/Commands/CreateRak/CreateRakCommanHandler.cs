﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Rak.Commands.CreateRak
{
    internal class CreateRakCommanHandler : IRequestHandler<CreateRakRequest, Result<CreateRakResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IRakRepository _rakRepository;

        public CreateRakCommanHandler(IUnitOfWork unitOfWork, IMapper mapper, IRakRepository rakRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _rakRepository = rakRepository;
        }

        public async Task<Result<CreateRakResponseDto>> Handle(CreateRakRequest request, CancellationToken cancellationToken)
        {
            var rak = _mapper.Map<MasterRak>(request);

            var validateRak = await _rakRepository.ValidateData(rak);
            if (validateRak == true)
            {
                return await Result<CreateRakResponseDto>.FailureAsync("Rak already exist.");
            }
            rak.NomorRak = request.NomorRak;
            rak.CreatedAt = DateTime.UtcNow;
            rak.UpdatedAt = DateTime.UtcNow;
            await _unitOfWork.Repository<MasterRak>().AddAsync(rak);
            rak.AddDomainEvent(new RakCreatedEvent(rak));
            await _unitOfWork.Save(cancellationToken);
            var rakResponse = _mapper.Map<CreateRakResponseDto>(rak);
            return await Result<CreateRakResponseDto>.SuccessAsync(rakResponse, "Rak created.");
        }
    }
}