﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKembali
{
    public record BarangKembaliDto
    {
        public Guid Id { get; set; }
        [JsonPropertyName("id_transaksi")]
        public string IdTransaksi { get; set; }
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("quantity")]
        public int Quantity { get; set; }
        [JsonPropertyName("catatan")]
        public string Catatan {  get; set; }
        [JsonPropertyName("pic")]
        public string Pic {  get; set; }
    }
    public sealed record CreateResponseBarangKembaliDto : BarangKembaliDto { }
}