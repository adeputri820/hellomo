﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.CreateBarangKembali
{
    public class CreateBarangKembaliRequest : IRequest<Result<List<CreateResponseBarangKembaliDto>>>
    {
        [JsonPropertyName("id_transaksi")]
        public string IdTransaksi { get; set; }
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }
        [JsonPropertyName("barang_kembali")]
        public List<CreateResponseBarangKembaliDto> BarangKembali { get; set; }
    }
}