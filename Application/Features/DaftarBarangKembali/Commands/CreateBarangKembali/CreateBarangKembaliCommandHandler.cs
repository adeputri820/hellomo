﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.CreateBarangKembali
{
    internal class CreateBarangKembaliCommandHandler : IRequestHandler<CreateBarangKembaliRequest, Result<List<CreateResponseBarangKembaliDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateBarangKembaliCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<CreateResponseBarangKembaliDto>>> Handle(CreateBarangKembaliRequest request, CancellationToken cancellationToken)
        {
            var response = new List<CreateResponseBarangKembaliDto>();
           var barangKembali = new BarangKembali();

                barangKembali.IdTransaksi = request.IdTransaksi;
                barangKembali.IdLokasi = request.IdLokasi;
                barangKembali.Tanggal = DateOnly.FromDateTime(DateTime.Now);
            foreach (var data in request.BarangKembali)
            {
                barangKembali.Id = Guid.NewGuid();
                barangKembali.IdBarang = data.IdBarang;
                barangKembali.CreatedAt = DateTime.UtcNow;
                barangKembali.UpdatedAt = DateTime.UtcNow;
                barangKembali.Quantity = data.Quantity;
                barangKembali.Status = "Dalam Perjalanan";
                barangKembali.Catatan = data.Catatan;
                barangKembali.Pic = data.Pic;
                response.AddRange(request.BarangKembali);
                await _unitOfWork.Repository<BarangKembali>().AddAsync(barangKembali);
                barangKembali.AddDomainEvent(new BarangKembaliCreatedEvent(barangKembali));
                await _unitOfWork.Save(cancellationToken);
            }
            return await Result<List<CreateResponseBarangKembaliDto>>.SuccessAsync(response, "Created.");
        }
    }
}