﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.CreateBarangKembali
{
    public class BarangKembaliCreatedEvent : BaseEvent
    {
        public BarangKembali BarangKembali { get; set; }

        public BarangKembaliCreatedEvent(BarangKembali barangKembali)
        {
            BarangKembali = barangKembali;
        }
    }
}