﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.UpdateBarangKembali
{
    public class BarangKembaliUpdateEvent : BaseEvent
    {
        public BarangKembali BarangKembali { get; set; }

        public BarangKembaliUpdateEvent(BarangKembali barangKembali)
        {
            BarangKembali = barangKembali;
        }
    }
}