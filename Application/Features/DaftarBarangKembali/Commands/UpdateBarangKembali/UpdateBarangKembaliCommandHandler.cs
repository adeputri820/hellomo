﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.UpdateBarangKembali
{
    internal class UpdateBarangKembaliCommandHandler : IRequestHandler<UpdateBarangKembaliRequest, Result<BarangKembali>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateBarangKembaliCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<BarangKembali>> Handle(UpdateBarangKembaliRequest request, CancellationToken cancellationToken)
        {
            foreach (var item in request.BarangKembali)
            {
                var barangKembali = await _unitOfWork.Repository<BarangKembali>().GetByIdAsync(item.Id);

                if (barangKembali != null)
                {
                    barangKembali.IdBarang = item.IdBarang;
                    barangKembali.IdLokasi = item.IdLokasi;
                    barangKembali.Quantity = item.Quantity;
                    barangKembali.Catatan = item.Catatan;
                    barangKembali.Pic = item.Pic;
                    barangKembali.UpdatedAt = DateTime.UtcNow;

                    await _unitOfWork.Repository<BarangKembali>().UpdateAsync(barangKembali);
                    barangKembali.AddDomainEvent(new BarangKembaliUpdateEvent(barangKembali));

                    await _unitOfWork.Save(cancellationToken);
                }

                //return await Result<BarangKembali>.FailureAsync("Barang not found");
            }
            return await Result<BarangKembali>.SuccessAsync("Barang updated");
        }
    }
}