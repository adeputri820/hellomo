﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.UpdateBarangKembali
{
    public class UpdateBarangKembaliRequest : IRequest<Result<BarangKembali>>
    {
        [JsonPropertyName("barang_kembali")]
        public List<CreateResponseBarangKembaliDto> BarangKembali { get; set; }
    }
}