﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.DeleteBarangKembali
{
    public class DeleteBarangKembaliRequest : IRequest<Result<string>>, IMapFrom<BarangKembali>
    {
        public string IdTransaksi { get; set; }

        public DeleteBarangKembaliRequest()
        {
        }

        public DeleteBarangKembaliRequest(string idTransaksi)
        {
            IdTransaksi = idTransaksi;
        }
    }
}