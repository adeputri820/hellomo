﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.DeleteBarangKembali
{
    public class BarangKembaliDeletedEvent : BaseEvent
    {
        public BarangKembali BarangKembali { get; set; }

        public BarangKembaliDeletedEvent(BarangKembali barangKembali)
        {
            BarangKembali = barangKembali;
        }
    }
}