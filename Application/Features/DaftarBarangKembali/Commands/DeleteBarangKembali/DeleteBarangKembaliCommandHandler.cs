﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.DeleteBarangKembali
{
    internal class DeleteBarangKembaliCommandHandler : IRequestHandler<DeleteBarangKembaliRequest, Result<string>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteBarangKembaliCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<string>> Handle(DeleteBarangKembaliRequest request, CancellationToken cancellationToken)
        {
            var barangKembaliDelete = _unitOfWork.Repository<BarangKembali>().FindByCondition(o => o.IdTransaksi == request.IdTransaksi).ToList();
            foreach(var item in barangKembaliDelete)
            {
                await _unitOfWork.Repository<BarangKembali>().DeleteAsync(item);
                item.AddDomainEvent(new BarangKembaliDeletedEvent(item));
                await _unitOfWork.Save(cancellationToken);
            }
            return await Result<string>.FailureAsync("deleted");
        }
    }
}