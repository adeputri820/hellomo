﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.KonfirmasiBarangKeluar;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.KonfirmasiBarangKembali
{
    internal class KonfirmasiBarangKembaliCommandHandler : IRequestHandler<KonfirmasiBarangKembaliRequest, Result<UpdateStatusDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public KonfirmasiBarangKembaliCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<UpdateStatusDto>> Handle(KonfirmasiBarangKembaliRequest request, CancellationToken cancellationToken)
        {
            var data = new UpdateStatusDto();
            var sql = await _unitOfWork.Repository<BarangKembali>().Entities
                .Where(x => x.IdTransaksi == request.IdTransaksi)
                .OrderByDescending(x => x.UpdatedAt)
                .Select(x => new UpdateStatusDto
                {
                    Id = x.Id,
                    IdTransaksi = x.IdTransaksi,
                }).ToListAsync(cancellationToken);

            foreach (var daftarData in sql)
            {
                var konfirmasiBarangKembali = await _unitOfWork.Repository<BarangKembali>().GetByIdAsync(daftarData.Id);
                if (konfirmasiBarangKembali != null)
                {
                    konfirmasiBarangKembali.Status = "Selesai";
                    konfirmasiBarangKembali.UpdatedAt = DateTime.UtcNow;

                    await _unitOfWork.Repository<BarangKembali>().UpdateAsync(konfirmasiBarangKembali);
                    konfirmasiBarangKembali.AddDomainEvent(new KonfirmasiBarangKembaliUpdateEvent(konfirmasiBarangKembali));

                    await _unitOfWork.Save(cancellationToken);
                }
            }
            return await Result<UpdateStatusDto>.SuccessAsync(data, "Status updated");
        }
    }
}