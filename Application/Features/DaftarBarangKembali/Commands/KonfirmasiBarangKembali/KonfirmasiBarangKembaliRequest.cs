﻿using MediatR;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.KonfirmasiBarangKeluar;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.KonfirmasiBarangKembali
{
    public class KonfirmasiBarangKembaliRequest : IRequest<Result<UpdateStatusDto>>
    {
        [JsonIgnore]
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("id_transaksi")]
        public string IdTransaksi { get; set; }
    }
}