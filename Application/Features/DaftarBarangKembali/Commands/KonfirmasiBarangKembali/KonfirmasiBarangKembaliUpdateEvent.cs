﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Commands.KonfirmasiBarangKembali
{
    public class KonfirmasiBarangKembaliUpdateEvent : BaseEvent
    {
        public BarangKembali BarangKembali { get; set; }

        public KonfirmasiBarangKembaliUpdateEvent(BarangKembali barangKembali)
        {
            BarangKembali = barangKembali;
        }
    }
}