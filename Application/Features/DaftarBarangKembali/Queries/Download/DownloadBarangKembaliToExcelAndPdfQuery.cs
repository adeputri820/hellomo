﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Queries.Download
{
    public record class DownloadBarangKembaliToExcelAndPdfQuery : IRequest<DownloadBarangKembaliToExcelAndPdfDto>
    {
        public string IdTransaksi { get; set; }

        public DownloadBarangKembaliToExcelAndPdfQuery() { }

        public DownloadBarangKembaliToExcelAndPdfQuery(string idTransaksi)
        {
            IdTransaksi = idTransaksi;
        }
    }

    internal class DownloadBarangToExcellAndPdfQueryHandler : IRequestHandler<DownloadBarangKembaliToExcelAndPdfQuery, DownloadBarangKembaliToExcelAndPdfDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IJumlahStokRepository _repository;

        public DownloadBarangToExcellAndPdfQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IJumlahStokRepository repository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<DownloadBarangKembaliToExcelAndPdfDto> Handle(DownloadBarangKembaliToExcelAndPdfQuery getList, CancellationToken cancellationToken)
        {
            var data = _repository.DownloadBarangKembaliToExcelAndPdfDto(getList.IdTransaksi);

            return await data;
        }
    }
}