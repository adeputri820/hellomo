﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetBarangKembaliWithPagination;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Queries.Download
{
    public class DownloadBarangKembaliToExcelAndPdfDto : IMapFrom<DownloadBarangKembaliToExcelAndPdfDto>
    {
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [JsonPropertyName("id_transaksi")]
        public string IdTransaksi { get; set; }

        [JsonPropertyName("lokasi_tujuan")]
        public string LokasiTujuan { get; set; }

        [JsonPropertyName("pic")]
        public string Pic { get; set; }

        [JsonPropertyName("created_at")]
        public DateOnly? Tanggal { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("data_brg")]
        public List<RiwayatDataDto> DataBrg { get; set; }
    }
}