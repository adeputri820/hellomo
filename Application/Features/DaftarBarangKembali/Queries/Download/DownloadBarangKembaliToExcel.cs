﻿using ClosedXML.Excel;
using SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.DownloadBarangKeluarToExcell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Queries.Download
{
    public class DownloadBarangKembaliToExcel
    {
        private readonly Task<DownloadBarangKembaliToExcelAndPdfDto> _obj;

        public DownloadBarangKembaliToExcel(Task<DownloadBarangKembaliToExcelAndPdfDto> data)
        {
            _obj = data;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "Id Transaksi";
                worksheet.Cell(1, 2).Value = "Lokasi Tujuan";
                worksheet.Cell(1, 3).Value = "Tanggal";
                worksheet.Cell(1, 4).Value = "Status";
                worksheet.Cell(1, 5).Value = "PIC";
                worksheet.Cell(1, 6).Value = "SKU";
                worksheet.Cell(1, 7).Value = "Nama Barang";
                worksheet.Cell(1, 8).Value = "Varian";
                worksheet.Cell(1, 9).Value = "Catatan";
                worksheet.Cell(1, 10).Value = "Quantity";

                worksheet.Cell(2, 1).Value = _obj.Result.IdTransaksi;
                worksheet.Cell(2, 2).Value = _obj.Result.LokasiTujuan;
                worksheet.Cell(2, 3).Value = _obj.Result.Tanggal;
                worksheet.Cell(2, 4).Value = _obj.Result.Status;
                worksheet.Cell(2, 5).Value = _obj.Result.Pic;

                for (int i = 0; i < _obj.Result.DataBrg.Count(); i++)
                {
                    worksheet.Cell(i + 2, 6).Value = _obj.Result.DataBrg.ElementAt(i).Sku;
                    worksheet.Cell(i + 2, 7).Value = _obj.Result.DataBrg.ElementAt(i).NamaBarang;
                    worksheet.Cell(i + 2, 8).Value = _obj.Result.DataBrg.ElementAt(i).Varian;
                    worksheet.Cell(i + 2, 9).Value = _obj.Result.DataBrg.ElementAt(i).Catatan;
                    worksheet.Cell(i + 2, 10).Value = _obj.Result.DataBrg.ElementAt(i).Quantity;
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Barang_Kembali_{DateTime.Now.ToString("yyyy-MMMM-dddd")}.xlsx";
                }
            }
        }
    }
}