﻿using SkeletonApi.Application.Common.Mappings;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetByIdTransaksi
{
    public class GetByIdTransaksiDto : IMapFrom<GetByIdTransaksiDto>
    {
        public string IdTransaksi { get; set; }
        public string LokasiAsal { get; set; }
        public string Status { get; set; }
    }
}