﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetByIdTransaksi
{
    public record GetByIdTransaksiQuery : IRequest<Result<List<GetByIdTransaksiDto>>>
    {
        public string IdTransaksi { get; set; }
        public GetByIdTransaksiQuery(string idTransaksi)
        {
            IdTransaksi = idTransaksi;
        }
    }

    internal class GetByIdTransaksiQueryHandler : IRequestHandler<GetByIdTransaksiQuery, Result<List<GetByIdTransaksiDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetByIdTransaksiQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetByIdTransaksiDto>>> Handle(GetByIdTransaksiQuery query, CancellationToken cancellationToken)
        {
            var brg = await _unitOfWork.Repository<BarangKeluar>().Entities.Where(o => o.IdTransaksi == query.IdTransaksi && o.Status == "Selesai").Select(o => new GetByIdTransaksiDto
            {
                IdTransaksi = o.IdTransaksi,
                LokasiAsal = o.LokasiAsal,
                Status = o.Status
            }).Distinct()
            .ProjectTo<GetByIdTransaksiDto>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);
            if (brg.Count() == 0)
            {
                return await Result<List<GetByIdTransaksiDto>>.FailureAsync(brg, "Id Transaksi tidak ditemukan");
            }

            return await Result<List<GetByIdTransaksiDto>>.SuccessAsync(brg, "Successfully fetch data");
        }
    }
}