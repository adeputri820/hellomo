﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetBarangKembaliWithPagination
{
    public record GetBarangKembaliWithPaginationQuery : IRequest<PaginatedResult<GetBarangKembaliWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }

        public GetBarangKembaliWithPaginationQuery() { }

        public GetBarangKembaliWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetBarangKembaliWithPaginationQueryHandler : IRequestHandler<GetBarangKembaliWithPaginationQuery, PaginatedResult<GetBarangKembaliWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IJumlahStokRepository _jumlahStokRepository;

        public GetBarangKembaliWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IJumlahStokRepository jumlahStokRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _jumlahStokRepository = jumlahStokRepository;
        }

        public async Task<PaginatedResult<GetBarangKembaliWithPaginationDto>> Handle(GetBarangKembaliWithPaginationQuery query, CancellationToken cancellationToken)
        {
            var data = await _jumlahStokRepository.GetBarangKembaliWithPagination();
            return await data.ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}