﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetBarangKembaliWithPagination
{
    public class GetBarangKembaliWithPaginationDto : IMapFrom<GetBarangKembaliWithPaginationDto>
    {
        //public Guid Id { get; set; }
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }
        //[JsonPropertyName("id_barang")]
        //public Guid IdBarang { get; set; }
        [JsonPropertyName("id_transaksi")]
        public string IdTransaksi { get; set; }
        [JsonPropertyName("lokasi_tujuan")]
        public string LokasiTujuan { get; set; }
        [JsonPropertyName("pic")]
        public string Pic {  get; set; }
        [JsonPropertyName("created_at")]
        public DateOnly? Tanggal { get; set; }
        [JsonPropertyName("status")]
        public string Status { get; set; }  
        [JsonPropertyName("data_brg")]
        public List<RiwayatDataDto> DataBrg { get; set; }
    }

    public class RiwayatDataDto
    {
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("sku")]
        public string Sku { get; set; }
        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }
        [JsonPropertyName("varian")]
        public string Varian { get; set; }
        [JsonPropertyName("quantity")]
        public int Quantity { get; set; }
        [JsonPropertyName("catatan")]
        public string Catatan {  get; set; }
    }
}