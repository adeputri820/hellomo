﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DataPeringatanStok
{
    public record PeringatanStokDto
    {
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }
        [JsonPropertyName("minimal_stok")]
        public int? MinimalStok { get; set; }
        [JsonPropertyName("status")]
        public string? Status { get; set; }
    }
    public sealed record CreatePeringatanStokResponseDto : PeringatanStokDto { }
}