﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.CreatePeringatanStok
{
    internal class CreatePeringatanStokCommandHandler : IRequestHandler<CreatePeringatanStokRequest, Result<List<CreatePeringatanStokResponseDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEkspedisiRepository _ekspedisiRepository;

        public CreatePeringatanStokCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IEkspedisiRepository ekspedisiRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ekspedisiRepository = ekspedisiRepository;
        }

        public async Task<Result<List<CreatePeringatanStokResponseDto>>> Handle(CreatePeringatanStokRequest request, CancellationToken cancellationToken)
        {
            var output = new List<CreatePeringatanStokResponseDto>();
            foreach (var data in request.DaftarPeringatanStok)
            {
                //var ekspedisi = _mapper.Map<BarangMasuk>(data);
                var peringatanStok = new PeringatanStok();
                peringatanStok.Id = Guid.NewGuid();
                peringatanStok.IdLokasi = data.IdLokasi;
                peringatanStok.IdBarang = data.IdBarang;
                peringatanStok.MinimalStok = data.MinimalStok;
                peringatanStok.CreatedAt = DateTime.UtcNow;
                peringatanStok.UpdatedAt = DateTime.UtcNow;
                await _unitOfWork.Repository<PeringatanStok>().AddAsync(peringatanStok);
                peringatanStok.AddDomainEvent(new PeringatanStokCreatedEvent(peringatanStok));
                await _unitOfWork.Save(cancellationToken);
            }
            return await Result<List<CreatePeringatanStokResponseDto>>.SuccessAsync(output, "Ekspedisi created.");
        }
    }
}