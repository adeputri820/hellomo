﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.CreatePeringatanStok
{
    public class CreatePeringatanStokRequest : IRequest<Result<List<CreatePeringatanStokResponseDto>>>
    {
        [JsonPropertyName("daftar_peringatan_stok")]
        public List<CreatePeringatanStokResponseDto> DaftarPeringatanStok { get; set; }
    }
}