﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.CreatePeringatanStok
{
    public class PeringatanStokCreatedEvent : BaseEvent
    {
        public PeringatanStok PeringatanStok { get; set; }

        public PeringatanStokCreatedEvent(PeringatanStok peringatanStok)
        {
            PeringatanStok = peringatanStok;
        }
    }
}