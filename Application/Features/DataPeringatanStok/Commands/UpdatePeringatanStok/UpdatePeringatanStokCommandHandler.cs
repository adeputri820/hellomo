﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.UpdatePeringatanStok
{
    internal class UpdatePeringatanStokCommandHandler : IRequestHandler<UpdatePeringatanStokRequest, Result<PeringatanStok>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdatePeringatanStokCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<PeringatanStok>> Handle(UpdatePeringatanStokRequest request, CancellationToken cancellationToken)
        {
            var peringatanStokUpdate = await _unitOfWork.Repository<PeringatanStok>().GetByIdAsync(request.Id);

            if (peringatanStokUpdate != null)
            {
                peringatanStokUpdate.IdBarang = request.IdBarang;
                peringatanStokUpdate.IdLokasi = request.IdLokasi;
                peringatanStokUpdate.MinimalStok = request.MinimalStok;
                peringatanStokUpdate.Status = request.Status;
                peringatanStokUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<PeringatanStok>().UpdateAsync(peringatanStokUpdate);
                peringatanStokUpdate.AddDomainEvent(new PeringatanStokUpdateEvent(peringatanStokUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<PeringatanStok>.SuccessAsync(peringatanStokUpdate, "Peringatan Stok updated");
            }
            return await Result<PeringatanStok>.FailureAsync("Peringatan Stok found");
        }
    }
}