﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.UpdatePeringatanStok
{
    public class UpdatePeringatanStokRequest : IRequest<Result<PeringatanStok>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }

        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [JsonPropertyName("minimal_stok")]
        public int? MinimalStok { get; set; }

        [JsonPropertyName("status")]
        public string? Status { get; set; }
    }
}