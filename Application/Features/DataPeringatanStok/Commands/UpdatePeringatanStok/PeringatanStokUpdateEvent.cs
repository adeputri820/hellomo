﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.UpdatePeringatanStok
{
    public class PeringatanStokUpdateEvent : BaseEvent
    {
        public PeringatanStok PeringatanStok { get; set; }

        public PeringatanStokUpdateEvent(PeringatanStok peringatanStok)
        {
            PeringatanStok = peringatanStok;
        }
    }
}