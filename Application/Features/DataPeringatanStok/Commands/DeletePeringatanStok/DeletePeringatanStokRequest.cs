﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.DeletePeringatanStok
{
    public class DeletePeringatanStokRequest : IRequest<Result<Guid>>, IMapFrom<PeringatanStok>
    {
        public Guid Id { get; set; }

        public DeletePeringatanStokRequest(Guid id)
        {
            Id = id;
        }

        public DeletePeringatanStokRequest()
        {
        }
    }
}