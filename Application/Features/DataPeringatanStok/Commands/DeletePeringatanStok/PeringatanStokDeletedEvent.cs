﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.DeletePeringatanStok
{
    public class PeringatanStokDeletedEvent : BaseEvent
    {
        public PeringatanStok PeringatanStok { get; set; }

        public PeringatanStokDeletedEvent(PeringatanStok peringatanStok)
        {
            PeringatanStok = peringatanStok;
        }
    }
}