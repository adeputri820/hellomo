﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Commands.DeletePeringatanStok
{
    internal class DeletePeringatanStokCommandHandler : IRequestHandler<DeletePeringatanStokRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePeringatanStokCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeletePeringatanStokRequest request, CancellationToken cancellationToken)
        {
            var barangMasukDelete = await _unitOfWork.Repository<PeringatanStok>().GetByIdAsync(request.Id);
            if (barangMasukDelete != null)
            {
                barangMasukDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<PeringatanStok>().DeleteAsync(barangMasukDelete);
                barangMasukDelete.AddDomainEvent(new PeringatanStokDeletedEvent(barangMasukDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(barangMasukDelete.Id, "Peringatan Stok deleted.");
            }
            return await Result<Guid>.FailureAsync("Peringatan Stok not found");
        }
    }
}