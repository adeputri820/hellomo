﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Queries.GetPeringatanStokWithPagination
{
    public class GetPeringatanStokWithPaginationDto : IMapFrom<GetPeringatanStokWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }

        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("varian")]
        public string Varian { get; set; }

        [JsonPropertyName("MinimalStok")]
        public int? MinimalStok { get; set; }

        [JsonPropertyName("lokasi")]
        public string Lokasi { get; set; }

        [JsonPropertyName("status")]
        public string? Status { get; set; }

        [JsonPropertyName("created_at")]
        public string Update_At { get; set; }

        [JsonPropertyName("tanggal")]
        public DateTime UpdateAt { get; set; }
    }
}