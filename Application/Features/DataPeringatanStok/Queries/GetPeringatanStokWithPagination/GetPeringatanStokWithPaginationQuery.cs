﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Queries.GetPeringatanStokWithPagination
{
    public record GetPeringatanStokWithPaginationQuery : IRequest<PaginatedResult<GetPeringatanStokWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }

        public GetPeringatanStokWithPaginationQuery() { }

        public GetPeringatanStokWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetPeringatanStokWithPaginationQueryHandler : IRequestHandler<GetPeringatanStokWithPaginationQuery, PaginatedResult<GetPeringatanStokWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPeringatanStokWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetPeringatanStokWithPaginationDto>> Handle(GetPeringatanStokWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<PeringatanStok>()
            .FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
             || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.MasterLokasi.NamaLokasi.ToLower())
            .Select(p => new GetPeringatanStokWithPaginationDto
            {
                Id = p.Id,
                IdBarang = p.IdBarang,
                IdLokasi = p.IdLokasi,
                Sku = p.MasterBarang.Sku,
                NamaBarang = p.MasterBarang.NamaBarang,
                Varian = p.MasterBarang.Varian.NamaVarian,
                Lokasi = p.MasterLokasi.NamaLokasi,
                MinimalStok = p.MinimalStok,
                Status = p.Status,
                UpdateAt = p.UpdatedAt.Value,
                Update_At = p.UpdatedAt.Value.ToString("dd-MM-yyyy")
            })
            .OrderByDescending(x => x.UpdateAt)
            .ProjectTo<GetPeringatanStokWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}