﻿using ClosedXML.Excel;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Queries.GetPeringatanStokWithPagination
{
    public class DownloadPeringatanStokToExcell
    {
        private readonly PaginatedResult<GetPeringatanStokWithPaginationDto> pg;

        public DownloadPeringatanStokToExcell(PaginatedResult<GetPeringatanStokWithPaginationDto> paginated)
        {
            pg = paginated;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "SKU";
                worksheet.Cell(1, 2).Value = "Nama Barang";
                worksheet.Cell(1, 3).Value = "Varian";
                worksheet.Cell(1, 4).Value = "Lokasi";
                worksheet.Cell(1, 5).Value = "Minimal Stok";
                worksheet.Cell(1, 6).Value = "Tanggal";

                for (int i = 0; i < pg.Data.Count(); i++)
                {
                    worksheet.Cell(i + 2, 1).Value = pg.Data.ElementAt(i).Sku;
                    worksheet.Cell(i + 2, 2).Value = pg.Data.ElementAt(i).NamaBarang;
                    worksheet.Cell(i + 2, 3).Value = pg.Data.ElementAt(i).Varian;
                    worksheet.Cell(i + 2, 4).Value = pg.Data.ElementAt(i).Lokasi;
                    worksheet.Cell(i + 2, 5).Value = pg.Data.ElementAt(i).MinimalStok;
                    worksheet.Cell(i + 2, 6).Value = pg.Data.ElementAt(i).UpdateAt;
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Peringatan_Stok_{DateTime.Now.ToString("yyyy-MMMM-dddd")}.xlsx";
                }
            }
        }
    }
}