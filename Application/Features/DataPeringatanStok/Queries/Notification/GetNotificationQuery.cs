﻿using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Queries.Notification
{
    public record class GetNotificationQuery : IRequest<Result<List<GetNotificationDto>>>;

    internal class GetNotificationQueryHandler : IRequestHandler<GetNotificationQuery, Result<List<GetNotificationDto>>>
    {
        private readonly INotificationStokRepository _notificationStokRepository;

        public GetNotificationQueryHandler(INotificationStokRepository notificationStokRepository)
        {
            _notificationStokRepository = notificationStokRepository;
        }

        public async Task<Result<List<GetNotificationDto>>> Handle(GetNotificationQuery query, CancellationToken cancellationToken)
        {
            var notif = await _notificationStokRepository.NotificationStok();
            return await Result<List<GetNotificationDto>>.SuccessAsync(notif, "Successfully fetch data");
        }
    }
}