﻿using SkeletonApi.Application.Common.Mappings;

namespace SkeletonApi.Application.Features.DataPeringatanStok.Queries.Notification
{
    public class GetNotificationDto : IMapFrom<GetNotificationDto>
    {
        public int Value { get; set; }
        public string message { get; set; }
    }
}