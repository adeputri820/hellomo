﻿using ClosedXML.Excel;
using SkeletonApi.Application.Features.DashboardInventory.Queries.GetBarangKeluar;
using SkeletonApi.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.DaftarPembelian.Queries.GetPembelianWithPagination
{
    public class DownloadPembelianToExcel
    {
        private readonly PaginatedResult<GetPembelianWithPaginationDto> pg;

        public DownloadPembelianToExcel(PaginatedResult<GetPembelianWithPaginationDto> paginated)
        {
            pg = paginated;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "No Pesanan";
                worksheet.Cell(1, 2).Value = "Supplier";
                worksheet.Cell(1, 3).Value = "Lokasi";
                worksheet.Cell(1, 4).Value = "Tanggal Order";
                worksheet.Cell(1, 5).Value = "Status";

                for (int i = 0; i < pg.Data.Count(); i++)
                {
                    worksheet.Cell(i + 2, 1).Value = pg.Data.ElementAt(i).NoPesanan;
                    worksheet.Cell(i + 2, 2).Value = pg.Data.ElementAt(i).NamaSupplier;
                    worksheet.Cell(i + 2, 3).Value = pg.Data.ElementAt(i).Lokasi;
                    worksheet.Cell(i + 2, 4).Value = pg.Data.ElementAt(i).TglOrder;
                    worksheet.Cell(i + 2, 5).Value = pg.Data.ElementAt(i).Status;
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Pembelian_{DateTime.Now.ToString("yyyy-MMMM-dddd")}.xlsx";
                }
            }
        }
    }
}