﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarPembelian.Queries.GetPembelianWithPagination
{
    public record GetPembelianWithPaginationQuery : IRequest<PaginatedResult<GetPembelianWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? Lokasi { get; set; }
        public string? Status { get; set; }

        public GetPembelianWithPaginationQuery() { }

        public GetPembelianWithPaginationQuery(int pageNumber, int pageSize, string lokasi, string status)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            Lokasi = lokasi;
            Status = status;
        }
    }

    internal class GetPembelianWithPaginationQueryHandler : IRequestHandler<GetPembelianWithPaginationQuery, PaginatedResult<GetPembelianWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPembelianWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetPembelianWithPaginationDto>> Handle(GetPembelianWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Pembelian>()
            .FindByCondition(x => (query.Lokasi == null || query.Lokasi.ToLower() == x.MasterLokasi.NamaLokasi.ToLower())
             && (query.Status == null ||query.Status.ToLower() == x.Status.ToLower()))
            .Select(p => new GetPembelianWithPaginationDto
            {
                NoPesanan = p.NoPesanan,
                NamaSupplier = p.MasterSupplier.NamaSupplier,
                Lokasi = p.MasterLokasi.NamaLokasi,
                TglOrder = p.TglOrder.ToString("dd-MM-yyyy"),
                Quantity = p.Quantity,
                Tgl_Order = p.TglOrder,
                //TotalSupplier = (double)(p.Quantity * p.HargaSatuanCny),
                //TotalEkspedisi = (double)(p.Cbm * p.HargaPerCbm),
                Status = p.Status,
            }).Distinct().OrderByDescending(o => o.Tgl_Order).ProjectTo<GetPembelianWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}