﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarPembelian.Queries.GetPembelianWithPagination
{
    public class GetPembelianWithPaginationDto : IMapFrom<GetPembelianWithPaginationDto>
    {

        [JsonPropertyName("no_pesanan")]
        public string NoPesanan { get; set; }

        [JsonPropertyName("nama_supplier")]
        public string NamaSupplier { get; set; }

        [JsonPropertyName("lokasi")]
        public string Lokasi { get; set; }

        [JsonPropertyName("tgl_order")]
        public string TglOrder { get; set; }
        [JsonPropertyName("tglorder")]
        public DateTime Tgl_Order { get; set; }
        public int Quantity {  get; set; }
        //public double TotalSupplier { get; set; }
        //public double TotalEkspedisi { get; set; }

        [JsonPropertyName("status")]
        public string? Status { get; set; }
    }
}