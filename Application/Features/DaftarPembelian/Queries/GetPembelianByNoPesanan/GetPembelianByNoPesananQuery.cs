﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetByIdTransaksi;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarPembelian.Queries.GetPembelianByNoPesanan
{
    public record GetPembelianByNoPesananQuery : IRequest<Result<GetPembelianByNoPesananDto>>
    {
        public string NoPesanan { get; set; }
        public GetPembelianByNoPesananQuery(string username)
        {
            NoPesanan = username;
        }
    }

    internal class GetPembelianByNoPesananQueryHandler : IRequestHandler<GetPembelianByNoPesananQuery, Result<GetPembelianByNoPesananDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPembelianByNoPesananQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<GetPembelianByNoPesananDto>> Handle(GetPembelianByNoPesananQuery query, CancellationToken cancellationToken)
        {
            var brg = await _unitOfWork.Repository<Pembelian>().Entities.Where(o => o.NoPesanan == query.NoPesanan && o.Status == "Selesai")
            .GroupBy(g => new {g.NoPesanan, g.Status, g.Pic, g.MasterLokasi.NamaLokasi, g.MasterSupplier.NamaSupplier, g.TglOrder, g.Keterangan, g.EstimasiTiba, g.MasterEkspedisi.NamaEkspedisi,g.Cbm,g.HargaPerCbm, g.KodeMarking })
            .Select(o => new GetPembelianByNoPesananDto
            {
                NoPesanan = o.Key.NoPesanan,
                EstimasiTiba = o.Key.EstimasiTiba.ToString("dd-MM-yyyy"),
                NamaSupplier = o.Key.NamaSupplier,
                Lokasi = o.Key.NamaLokasi,
                TglOrder = o.Key .TglOrder.ToString("dd-MM-yyyy"),
                NamaEkspedisi = o.Key.NamaEkspedisi,
                KodeMarking = o.Key.KodeMarking,
                Cbm = (double)o.Key.Cbm,
                HargaPerCbm = (double)o.Key.HargaPerCbm,
                Tagihan = (double)o.Key.Cbm * (double)o.Key.HargaPerCbm,
                Keterangan = o.Key.Keterangan,
                Pic = o.Key.Pic,
                DaftarPembelian = o.Select(s => new Data
                {
                    Id = s.Id,
                    IdBarang = s.IdBarang,
                    NamaBarang = s.MasterBarang.NamaBarang,
                    Sku = s.MasterBarang.NamaBarang,
                    Quantity = s.Quantity,
                    HargaSatuanCny = s.HargaSatuanCny,
                    KonversiCny = s.KonversiCny,
                    HargaSatuanIdr = s.HargaSatuanIdr,
                    TotalCny = s.Quantity * s.HargaSatuanCny,
                    TotalIdr = s.Quantity * s.HargaSatuanIdr
                }).ToList()
            })
            .ProjectTo<GetByIdTransaksiDto>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);

            return await Result<GetPembelianByNoPesananDto>.SuccessAsync("Successfully fetch data");
        }
    }
}