﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarPembelian.Queries.GetPembelianByNoPesanan
{
    public class GetPembelianByNoPesananDto : IMapFrom<GetPembelianByNoPesananDto>
    {
        [JsonPropertyName("no_pesanan")]
        public string NoPesanan { get; set; }

        [JsonPropertyName("estimasi_tiba")]
        public string EstimasiTiba { get; set; }

        [JsonPropertyName("nama_supplier")]
        public string NamaSupplier { get; set; }

        [JsonPropertyName("lokasi")]
        public string Lokasi { get; set; }

        [JsonPropertyName("tgl_order")]
        public string TglOrder { get; set; }

        [JsonPropertyName("keterangan")]
        public string Keterangan { get; set; }

        [JsonPropertyName("nama_ekspedisi")]
        public string NamaEkspedisi { get; set; }

        [JsonPropertyName("kode_marking")]
        public string KodeMarking { get; set; }

        [JsonPropertyName("cbm")]
        public double Cbm { get; set; }

        [JsonPropertyName("harga_per_cbm")]
        public double HargaPerCbm { get; set; }

        [JsonPropertyName("tagihan")]
        public double Tagihan { get; set; }

        [JsonPropertyName("total_supplier")]
        public double TotalSupplier { get; set; }

        [JsonPropertyName("pic")]
        public string Pic { get; set; }

        [JsonPropertyName("data_pembelian")]
        public List<Data> DaftarPembelian { get; set; }
    }

    public class Data
    {
        public Guid Id { get; set; }

        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }
        [JsonPropertyName("sku")]
        public string Sku {  get; set; }

        [JsonPropertyName("quantity")]
        public int Quantity { get; set; }

        [JsonPropertyName("harga_satuan_cny")]
        public double? HargaSatuanCny { get; set; }

        [JsonPropertyName("konversi_cny")]
        public double? KonversiCny { get; set; }

        [JsonPropertyName("harga_satuan_idr")]
        public double? HargaSatuanIdr { get; set; }
        [JsonPropertyName("total_cny")]
        public double? TotalCny { get; set; }
        [JsonPropertyName("total_idr")]
        public double? TotalIdr {  get; set; }
    }
}