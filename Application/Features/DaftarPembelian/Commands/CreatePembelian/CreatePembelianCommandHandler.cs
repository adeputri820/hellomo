﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.CreateBarangMasuk;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarPembelian.Commands.CreatePembelian
{
    internal class CreatePembelianCommandHandler : IRequestHandler<CreatePembelianRequest, Result<List<CreatePembelianResponseDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreatePembelianCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<CreatePembelianResponseDto>>> Handle(CreatePembelianRequest request, CancellationToken cancellationToken)
        {
            var pembelian = new Pembelian();
            pembelian.NoPesanan = DateTime.Now.ToString("PO-yyMMddhhmmssfff");
            pembelian.CreatedAt = DateTime.UtcNow;
            pembelian.UpdatedAt = DateTime.UtcNow;
            pembelian.Status = "Dalam Perjalanan";
            pembelian.IdSupplier = request.IdSupplier;
            pembelian.IdLokasi = request.IdLokasi;
            pembelian.IdEkspedisi = request.IdEkspedisi;
            pembelian.EstimasiTiba = request.EstimasiTiba;
            pembelian.TglOrder = request.TglOrder;
            pembelian.Keterangan = request.Keterangan;
            pembelian.KodeMarking = request.KodeMarking;
            pembelian.Cbm = request.Cbm;
            pembelian.HargaPerCbm = request.HargaPerCbm;
            pembelian.Pic = request.Pic;
            foreach (var data in request.DaftarPembelian)
            {
                pembelian.Id = Guid.NewGuid();
                pembelian.IdBarang = data.IdBarang;
                pembelian.Quantity = data.Quantity;
                pembelian.HargaSatuanCny = data.HargaSatuanCny;
                pembelian.KonversiCny = data.KonversiCny;
                pembelian.HargaSatuanIdr = data.HargaSatuanIdr;
            await _unitOfWork.Repository<Pembelian>().AddAsync(pembelian);
            pembelian.AddDomainEvent(new PembelianCreateEvent(pembelian));
            await _unitOfWork.Save(cancellationToken);
               
            }
            return await Result<List<CreatePembelianResponseDto>>.SuccessAsync("Created.");
        }
    }
}