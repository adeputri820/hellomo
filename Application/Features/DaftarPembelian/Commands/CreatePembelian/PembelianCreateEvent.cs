﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarPembelian.Commands.CreatePembelian
{
    public class PembelianCreateEvent : BaseEvent
    {
        public Pembelian Pembelian { get; set; }

        public PembelianCreateEvent(Pembelian pembelian)
        {
            Pembelian = pembelian;
        }
    }
}