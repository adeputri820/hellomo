﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.DaftarPembelian.Commands.DeletePembelian
{
    public class PembelianDeletedEvent : BaseEvent
    {
        public Pembelian Pembelian { get; set; }

        public PembelianDeletedEvent(Pembelian pembelian)
        {
            Pembelian = pembelian;
        }
    }
}