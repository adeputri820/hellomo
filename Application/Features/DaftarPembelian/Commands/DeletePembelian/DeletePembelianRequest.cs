﻿using MediatR;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarPembelian.Commands.DeletePembelian
{
    public class DeletePembelianRequest : IRequest<Result<string>>
    {
        public string NoPesanan { get; set; }

        public DeletePembelianRequest(string noPesanan)
        {
            NoPesanan = noPesanan;
        }

        public DeletePembelianRequest()
        {
        }
    }
}