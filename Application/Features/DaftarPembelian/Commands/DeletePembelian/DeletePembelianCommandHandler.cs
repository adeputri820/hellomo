﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarPembelian.Commands.DeletePembelian
{
    internal class DeletePembelianCommandHandler : IRequestHandler<DeletePembelianRequest, Result<string>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePembelianCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<string>> Handle(DeletePembelianRequest request, CancellationToken cancellationToken)
        {
            var pembelianDelete = _unitOfWork.Repository<Pembelian>().FindByCondition(o => o.NoPesanan == request.NoPesanan).ToList();
            foreach (var item in pembelianDelete)
            {
                await _unitOfWork.Repository<Pembelian>().DeleteAsync(item);
                item.AddDomainEvent(new PembelianDeletedEvent(item));
                await _unitOfWork.Save(cancellationToken);
            }
            return await Result<string>.FailureAsync("deleted");
        }
    }
}