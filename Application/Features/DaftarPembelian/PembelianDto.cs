﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarPembelian
{
    public record PembelianDto
    {
        public Guid Id { get; set; }
        [JsonPropertyName("id_supplier")]
        public Guid IdSupplier { get; set; }
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }
        [JsonPropertyName("id_ekspedisi")]
        public Guid IdEkspedisi { get; set; }
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("quantity")]
        public int Quantity { get; set; }
        [JsonPropertyName("harga_satuan_cny")]
        public double? HargaSatuanCny { get; set; }
        [JsonPropertyName("konversi_cny")]
        public double? KonversiCny { get; set; }
        [JsonPropertyName("harga_satuan_idr")]
        public double? HargaSatuanIdr { get; set; }
        [JsonPropertyName("no_pesanan")]
        public string NoPesanan { get; set; }
        [JsonPropertyName("estimasi_tiba")]
        public DateTime EstimasiTiba { get; set; }
        [JsonPropertyName("tgl_order")]
        public DateTime TglOrder { get; set; }
        [JsonPropertyName("keterangan")]
        public string? Keterangan { get; set; }
        [JsonPropertyName("kode_marking")]
        public string? KodeMarking { get; set; }
        [JsonPropertyName("cbm")]
        public double? Cbm { get; set; }
        [JsonPropertyName("harga_per_cbm")]
        public double? HargaPerCbm { get; set; }
        [JsonPropertyName("pic")]
        public string? Pic { get; set; }
        [JsonPropertyName("status")]
        public string? Status {  get; set; }
    }
    public sealed record CreatePembelianResponseDto : PembelianDto { }
}