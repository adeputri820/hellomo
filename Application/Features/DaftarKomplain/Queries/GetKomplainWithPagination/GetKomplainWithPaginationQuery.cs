﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarKomplain.Queries.GetKomplainWithPagination
{
    public record GetKomplainWithPaginationQuery : IRequest<PaginatedResult<GetKomplainWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetKomplainWithPaginationQuery()
        {
        }

        public GetKomplainWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetKomplainWithPaginationQueryHandler : IRequestHandler<GetKomplainWithPaginationQuery, PaginatedResult<GetKomplainWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetKomplainWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetKomplainWithPaginationDto>> Handle(GetKomplainWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Komplain>().FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower()
            || query.SearchTerm.ToLower() == x.MasterMarketplace.NamaMarketplace.ToLower() || query.SearchTerm.ToLower() == x.Username.ToLower()
            || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower())
            .Select(o => new GetKomplainWithPaginationDto
            {
                Id = o.Id,
                IdBarang = o.IdBarang,
                NamaBarang = o.MasterBarang.NamaBarang,
                Sku = o.MasterBarang.Sku,
                IdMarketplace = o.IdMarketplace,
                NamaMarketplace = o.MasterMarketplace.NamaMarketplace,
                NoPesanan = o.NoPesanan,
                Username = o.Username,
                Harga = o.Harga,
                Qty = o.Qty,
                JumlahRefund = o.Harga * o.Qty,
                Permasalahan = o.Permasalahan,
                Status = o.Status,
                Notifikasi = o.Notifikasi.ToString("dd-MM-yyyy"),
                UpdatedAt = o.UpdatedAt.Value.ToString("dd-MM-yyyy"),
                Updated_At = o.UpdatedAt.Value
            })
            .OrderByDescending(x => x.Updated_At)
            .ProjectTo<GetKomplainWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}