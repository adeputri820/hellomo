﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarKomplain.Queries.GetKomplainWithPagination
{
    public class GetKomplainWithPaginationDto : IMapFrom<GetKomplainWithPaginationDto>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("sku")]
        public string Sku { get; set; }
        [JsonPropertyName("nama_barang")]
        public string NamaBarang {  get; set; }
        [JsonPropertyName("id_marketplace")]
        public Guid IdMarketplace { get; set; }

        [JsonPropertyName("nama_marketplace")]
        public string NamaMarketplace { get; set; }

        [JsonPropertyName("no_pesanan")]
        public string NoPesanan { get; set; }

        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonPropertyName("harga")]
        public double Harga { get; set; }

        [JsonPropertyName("qty")]
        public int Qty { get; set; }
        [JsonPropertyName("jumlah_refund")]
        public double JumlahRefund {  get; set; }
        [JsonPropertyName("permasalahan")]
        public string Permasalahan { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("notifikasi")]
        public string Notifikasi { get; set; }
        [JsonPropertyName("created_at")]
        public string UpdatedAt { get; set; }
        public DateTime Updated_At { get; set; }
    }
}