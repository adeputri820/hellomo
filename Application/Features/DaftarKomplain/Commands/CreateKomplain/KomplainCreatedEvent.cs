﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarKomplain.Commands.CreateKomplain
{
    public class KomplainCreatedEvent : BaseEvent
    {
        public Komplain Komplain { get; set; }

        public KomplainCreatedEvent(Komplain komplain)
        {
            Komplain = komplain;
        }
    }
}