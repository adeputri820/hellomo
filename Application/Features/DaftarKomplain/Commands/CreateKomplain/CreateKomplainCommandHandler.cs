﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarKomplain.Commands.CreateKomplain
{
    internal class CreateKomplainCommandHandler : IRequestHandler<CreateKomplainRequest, Result<CreateKomplainResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CreateKomplainCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<CreateKomplainResponseDto>> Handle(CreateKomplainRequest request, CancellationToken cancellationToken)
        {
            var komplain = _mapper.Map<Komplain>(request);
            komplain.CreatedAt = DateTime.UtcNow;
            komplain.UpdatedAt = DateTime.UtcNow;
            await _unitOfWork.Repository<Komplain>().AddAsync(komplain);
            komplain.AddDomainEvent(new KomplainCreatedEvent(komplain));
            await _unitOfWork.Save(cancellationToken);
            var ekspedisiResponse = _mapper.Map<CreateKomplainResponseDto>(komplain);
            return await Result<CreateKomplainResponseDto>.SuccessAsync(ekspedisiResponse, "Created.");
        }
    }
}