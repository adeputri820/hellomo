﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarKomplain.Commands.DeleteKomplain
{
    public class DeleteKomplainRequest : IRequest<Result<Guid>>, IMapFrom<Komplain>
    {
        public Guid Id { get; set; }

        public DeleteKomplainRequest(Guid id)
        {
            Id = id;
        }

        public DeleteKomplainRequest()
        {
        }
    }
}