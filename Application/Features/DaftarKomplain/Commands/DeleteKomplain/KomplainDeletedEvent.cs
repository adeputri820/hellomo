﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarKomplain.Commands.DeleteKomplain
{
    public class KomplainDeletedEvent : BaseEvent
    {
        public Komplain Komplain { get; set; }

        public KomplainDeletedEvent(Komplain komplain)
        {
            Komplain = komplain;
        }
    }
}