﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarKomplain.Commands.DeleteKomplain
{
    internal class DeleteKomplainCommandHandler : IRequestHandler<DeleteKomplainRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteKomplainCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteKomplainRequest request, CancellationToken cancellationToken)
        {
            var komplainDelete = await _unitOfWork.Repository<Komplain>().GetByIdAsync(request.Id);
            if (komplainDelete != null)
            {
                komplainDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Komplain>().DeleteAsync(komplainDelete);
                komplainDelete.AddDomainEvent(new KomplainDeletedEvent(komplainDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(komplainDelete.Id, "deleted.");
            }
            return await Result<Guid>.FailureAsync("not found");
        }
    }
}