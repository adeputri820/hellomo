﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarKomplain
{
    public record KomplainDto
    {
        public Guid Id { get; set; }
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }

        [JsonPropertyName("id_marketplace")]
        public Guid IdMarketplace { get; set; }

        [JsonPropertyName("no_pesanan")]
        public string NoPesanan { get; set; }

        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonPropertyName("harga")]
        public double Harga { get; set; }

        [JsonPropertyName("qty")]
        public int Qty { get; set; }

        [JsonPropertyName("permasalahan")]
        public string Permasalahan { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("notifikasi")]
        public DateTime Notifikasi { get; set; }
    }
    public sealed record CreateKomplainResponseDto : KomplainDto { }
}