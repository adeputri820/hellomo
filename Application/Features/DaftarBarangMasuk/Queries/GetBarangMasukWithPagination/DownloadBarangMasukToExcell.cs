﻿using ClosedXML.Excel;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Queries.GetBarangMasukWithPagination
{
    public class DownloadBarangMasukToExcell
    {
        private readonly PaginatedResult<GetBarangMasukWithPaginationDto> pg;

        public DownloadBarangMasukToExcell(PaginatedResult<GetBarangMasukWithPaginationDto> paginated)
        {
            pg = paginated;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "SKU";
                worksheet.Cell(1, 2).Value = "Nama Barang";
                worksheet.Cell(1, 3).Value = "Varian";
                worksheet.Cell(1, 4).Value = "Lokasi";
                worksheet.Cell(1, 5).Value = "Rak";
                worksheet.Cell(1, 6).Value = "Quantity";
                worksheet.Cell(1, 7).Value = "Tanggal Masuk";
                worksheet.Cell(1, 8).Value = "PIC";

                for (int i = 0; i < pg.Data.Count(); i++)
                {
                    worksheet.Cell(i + 2, 1).Value = pg.Data.ElementAt(i).Sku;
                    worksheet.Cell(i + 2, 2).Value = pg.Data.ElementAt(i).NamaBarang;
                    worksheet.Cell(i + 2, 3).Value = pg.Data.ElementAt(i).Varian;
                    worksheet.Cell(i + 2, 4).Value = pg.Data.ElementAt(i).Lokasi;
                    worksheet.Cell(i + 2, 5).Value = pg.Data.ElementAt(i).Rak;
                    worksheet.Cell(i + 2, 6).Value = pg.Data.ElementAt(i).Quantity;
                    worksheet.Cell(i + 2, 7).Value = pg.Data.ElementAt(i).TanggalMasuk;
                    worksheet.Cell(i + 2, 8).Value = pg.Data.ElementAt(i).UserName;
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Barang_Masuk_{DateTime.Now.ToString("yyyy-MMMM-dddd")}.xlsx";
                }
            }
        }
    }
}