﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Queries.GetBarangMasukWithPagination
{
    public record GetBarangMasukWithPaginationQuery : IRequest<PaginatedResult<GetBarangMasukWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }
        public string? Lokasi { get; set; }
        public string? Type {  get; set; }
        public DateTime? Start {  get; set; }
        public DateTime? End { get; set; }

        public GetBarangMasukWithPaginationQuery() { }

        public GetBarangMasukWithPaginationQuery(int pageNumber, int pageSize, string searchTerm, string lokasi,string type, DateTime start, DateTime end)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
            Lokasi = lokasi;
            Type = type;
            Start = start;
            End = end;
        }
    }

    internal class GetBarangMasukWithPaginationQueryHandler : IRequestHandler<GetBarangMasukWithPaginationQuery, PaginatedResult<GetBarangMasukWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBarangMasukWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetBarangMasukWithPaginationDto>> Handle(GetBarangMasukWithPaginationQuery query, CancellationToken cancellationToken)
        {
            if (query.Type == "default")
            {
                return await _unitOfWork.Repository<BarangMasuk>()
                .FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
                 || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.MasterLokasi.NamaLokasi.ToLower()
                 || query.SearchTerm.ToLower() == x.MasterRak.NomorRak.ToLower())
                .Select(p => new GetBarangMasukWithPaginationDto
                {
                    Id = p.Id,
                    IdBarang = p.IdBarang,
                    IdLokasi = p.IdLokasi,
                    IdRak = p.IdRak,
                    Sku = p.MasterBarang.Sku,
                    NamaBarang = p.MasterBarang.NamaBarang,
                    Varian = p.MasterBarang.Varian.NamaVarian,
                    Lokasi = p.MasterLokasi.NamaLokasi,
                    Rak = p.MasterRak.NomorRak,
                    Quantity = p.Quantity,
                    TanggalMasuk = p.Tanggal,
                    UserName = p.UserName,
                })
                .OrderByDescending(x => x.TanggalMasuk)
                .ProjectTo<GetBarangMasukWithPaginationDto>(_mapper.ConfigurationProvider)
                .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
            }
            return await _unitOfWork.Repository<BarangMasuk>()
            .FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
             || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.MasterLokasi.NamaLokasi.ToLower()
             || query.SearchTerm.ToLower() == x.MasterRak.NomorRak.ToLower())
            .Select(p => new GetBarangMasukWithPaginationDto
            {
                Id = p.Id,
                IdBarang = p.IdBarang,
                IdLokasi = p.IdLokasi,
                IdRak = p.IdRak,
                Sku = p.MasterBarang.Sku,
                NamaBarang = p.MasterBarang.NamaBarang,
                Varian = p.MasterBarang.Varian.NamaVarian,
                Lokasi = p.MasterLokasi.NamaLokasi,
                Rak = p.MasterRak.NomorRak,
                Quantity = p.Quantity,
                TanggalMasuk = p.Tanggal,
                UserName = p.UserName,
            })
            .OrderByDescending(x => x.TanggalMasuk)
            .ProjectTo<GetBarangMasukWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}