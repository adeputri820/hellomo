﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Queries.GetBarangMasukWithPagination
{
    public class GetBarangMasukWithPaginationDto : IMapFrom<GetBarangMasukWithPaginationDto>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }

        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [JsonPropertyName("id_rak")]
        public Guid IdRak { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("varian")]
        public string Varian { get; set; }

        [JsonPropertyName("quantity")]
        public int? Quantity { get; set; }

        [JsonPropertyName("lokasi")]
        public string Lokasi { get; set; }

        [JsonPropertyName("rak")]
        public string Rak { get; set; }

        [JsonPropertyName("tanggal_masuk")]
        public DateOnly? TanggalMasuk { get; set; }

        [JsonPropertyName("PIC")]
        public string? UserName { get; set; }
    }
}