﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.MasterData.Barang.Queries.DropdownBarang;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Queries.GetSkuBarangMasuk
{
    public record GetSkuBarangMasukQuery : IRequest<Result<List<GetSkuBarangMasukDto>>>;

    internal class GetBarangAllQueryHandler : IRequestHandler<GetSkuBarangMasukQuery, Result<List<GetSkuBarangMasukDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBarangAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetSkuBarangMasukDto>>> Handle(GetSkuBarangMasukQuery query, CancellationToken cancellationToken)
        {
            var barang = await _unitOfWork.Repository<BarangMasuk>().Entities.Select(p => new GetSkuBarangMasukDto
            {
                Id = p.IdBarang,
                Sku = p.MasterBarang.Sku,
                NamaBarang = p.MasterBarang.NamaBarang,
                Varian = p.MasterBarang.Varian.NamaVarian
            }).Distinct()
            .ProjectTo<GetSkuBarangMasukDto>(_mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);

            return await Result<List<GetSkuBarangMasukDto>>.SuccessAsync(barang, "Successfully fetch data");
        }
    }
}