﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.DeleteBarangMasuk
{
    public class DeleteBarangMasukRequest : IRequest<Result<Guid>>, IMapFrom<BarangMasuk>
    {
        public Guid Id { get; set; }

        public DeleteBarangMasukRequest(Guid id)
        {
            Id = id;
        }

        public DeleteBarangMasukRequest()
        {
        }
    }
}