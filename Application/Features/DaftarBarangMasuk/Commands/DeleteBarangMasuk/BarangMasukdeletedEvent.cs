﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.DeleteBarangMasuk
{
    public class BarangMasukdeletedEvent : BaseEvent
    {
        public BarangMasuk BarangMasuk { get; set; }

        public BarangMasukdeletedEvent(BarangMasuk barangMasuk)
        {
            BarangMasuk = barangMasuk;
        }
    }
}