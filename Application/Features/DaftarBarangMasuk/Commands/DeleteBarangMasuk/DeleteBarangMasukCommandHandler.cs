﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.DeleteBarangMasuk
{
    internal class DeleteBarangMasukCommandHandler : IRequestHandler<DeleteBarangMasukRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteBarangMasukCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteBarangMasukRequest request, CancellationToken cancellationToken)
        {
            var barangMasukDelete = await _unitOfWork.Repository<BarangMasuk>().GetByIdAsync(request.Id);
            if (barangMasukDelete != null)
            {
                barangMasukDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<BarangMasuk>().DeleteAsync(barangMasukDelete);
                barangMasukDelete.AddDomainEvent(new BarangMasukdeletedEvent(barangMasukDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(barangMasukDelete.Id, "Barang deleted.");
            }
            return await Result<Guid>.FailureAsync("Barang not found");
        }
    }
}