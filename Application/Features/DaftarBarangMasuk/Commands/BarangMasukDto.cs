﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands
{
    public record BarangMasukDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }
        [JsonPropertyName("id_rak")]
        public Guid IdRak { get; set; }
        [JsonPropertyName("quantity")]
        public int? Quantity { get; set; }
        [JsonPropertyName("status")]
        public string? Status { get; set; }
        [JsonPropertyName("username")]
        public string? UserName { get; set; }
    }
    public sealed record CreateResponseBarangMasukDto : BarangMasukDto { }
}