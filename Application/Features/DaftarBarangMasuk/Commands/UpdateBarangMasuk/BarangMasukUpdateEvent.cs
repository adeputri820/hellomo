﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.UpdateBarangMasuk
{
    public class BarangMasukUpdateEvent : BaseEvent
    {
        public BarangMasuk BarangMasuk { get; set; }

        public BarangMasukUpdateEvent(BarangMasuk barangMasuk)
        {
            BarangMasuk = barangMasuk;
        }
    }
}