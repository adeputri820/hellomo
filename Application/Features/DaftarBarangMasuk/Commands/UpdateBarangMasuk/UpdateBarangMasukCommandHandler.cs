﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.UpdateBarangMasuk
{
    internal class UpdateBarangMasukCommandHandler : IRequestHandler<UpdateBarangMasukRequest, Result<BarangMasuk>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateBarangMasukCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<BarangMasuk>> Handle(UpdateBarangMasukRequest request, CancellationToken cancellationToken)
        {
            var barangMasukUpdate = await _unitOfWork.Repository<BarangMasuk>().GetByIdAsync(request.Id);

            if (barangMasukUpdate != null)
            {
                barangMasukUpdate.IdBarang = request.IdBarang;
                barangMasukUpdate.IdRak = request.IdRak;
                barangMasukUpdate.IdLokasi = request.IdLokasi;
                barangMasukUpdate.Quantity = request.Quantity;
                barangMasukUpdate.UpdatedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<BarangMasuk>().UpdateAsync(barangMasukUpdate);
                barangMasukUpdate.AddDomainEvent(new BarangMasukUpdateEvent(barangMasukUpdate));

                await _unitOfWork.Save(cancellationToken);
                return await Result<BarangMasuk>.SuccessAsync(barangMasukUpdate, "Barang updated");
            }
            return await Result<BarangMasuk>.FailureAsync("Barang not found");
        }
    }
}