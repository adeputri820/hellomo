﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.UpdateBarangMasuk
{
    public class UpdateBarangMasukRequest : IRequest<Result<BarangMasuk>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }

        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [JsonPropertyName("id_rak")]
        public Guid IdRak { get; set; }

        [JsonPropertyName("quantity")]
        public int? Quantity { get; set; }
    }
}