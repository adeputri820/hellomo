﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.CreateBarangMasuk
{
    public class CreateBarangMasukReques : IRequest<Result<List<CreateResponseBarangMasukDto>>>
    {
        [JsonPropertyName("daftar_barang_masuk")]
        public List<CreateResponseBarangMasukDto> DaftarBarangMasuk { get; set; }
    }
}