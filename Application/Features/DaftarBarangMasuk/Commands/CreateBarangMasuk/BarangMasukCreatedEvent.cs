﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.CreateBarangMasuk
{
    public class BarangMasukCreatedEvent : BaseEvent
    {
        public BarangMasuk BarangMasuk { get; set; }

        public BarangMasukCreatedEvent(BarangMasuk barangMasuk)
        {
            BarangMasuk = barangMasuk;
        }
    }
}