﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangMasuk.Commands.CreateBarangMasuk
{
    internal class CreateBarangMasukCommandHandler : IRequestHandler<CreateBarangMasukReques, Result<List<CreateResponseBarangMasukDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateBarangMasukCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<CreateResponseBarangMasukDto>>> Handle(CreateBarangMasukReques request, CancellationToken cancellationToken)
        {
            var output = new List<CreateResponseBarangMasukDto>();
            foreach (var data in request.DaftarBarangMasuk)
            {
                //var ekspedisi = _mapper.Map<BarangMasuk>(data);
                var barangMasuk = new BarangMasuk();
                barangMasuk.IdRak = data.IdRak;
                barangMasuk.Id = Guid.NewGuid();
                barangMasuk.IdLokasi = data.IdLokasi;
                barangMasuk.IdBarang = data.IdBarang;
                barangMasuk.CreatedAt = DateTime.UtcNow;
                barangMasuk.UpdatedAt = DateTime.UtcNow;
                barangMasuk.Quantity = data.Quantity;
                barangMasuk.UserName = data.UserName;
                barangMasuk.Status = "Dalam Perjalanan";
                barangMasuk.Tanggal = DateOnly.FromDateTime(DateTime.Now);
                await _unitOfWork.Repository<BarangMasuk>().AddAsync(barangMasuk);
                barangMasuk.AddDomainEvent(new BarangMasukCreatedEvent(barangMasuk));
                await _unitOfWork.Save(cancellationToken);
            }
            return await Result<List<CreateResponseBarangMasukDto>>.SuccessAsync(output, "Created.");
        }
    }
}