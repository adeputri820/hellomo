﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisTanggaldanRak
{
    public class OtomatisTanggaldanRakDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("id_rak")]
        public Guid IdRak {  get; set; }

        [JsonPropertyName("nama_rak")]
        public string NamaRak { get; set; }

        [JsonPropertyName("tanggal_masuk")]
        public DateOnly? Tanggal { get; set; }
    }
}