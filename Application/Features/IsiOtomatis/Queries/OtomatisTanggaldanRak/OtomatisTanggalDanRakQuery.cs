﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisTanggaldanRak
{
    public record class OtomatisTanggalDanRakQuery : IRequest<Result<List<OtomatisTanggaldanRakDto>>>
    {
        public Guid IdBarang { get; set; }
        public Guid IdLokasi { get; set; }

        public OtomatisTanggalDanRakQuery() { }

        public OtomatisTanggalDanRakQuery(Guid idBarang, Guid idLokasi)
        {
            IdBarang = idBarang;
            IdLokasi = idLokasi;
        }
    }

    internal class OtomatisTanggalDanRakQueryHandler : IRequestHandler<OtomatisTanggalDanRakQuery, Result<List<OtomatisTanggaldanRakDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OtomatisTanggalDanRakQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<OtomatisTanggaldanRakDto>>> Handle(OtomatisTanggalDanRakQuery getList, CancellationToken cancellationToken)
        {
            var tanggalPertama = await _unitOfWork.Repository<BarangMasuk>().Entities
                .Where(x => x.IdBarang == getList.IdBarang && x.IdLokasi == getList.IdLokasi)
                .OrderByDescending(x => x.Tanggal)
                .Select(x => new OtomatisTanggaldanRakDto
                {
                    Tanggal = x.Tanggal,
                }).FirstOrDefaultAsync(cancellationToken);

            var sql = await _unitOfWork.Repository<BarangMasuk>().Entities
                .Where(x => x.IdBarang == getList.IdBarang && x.IdLokasi == getList.IdLokasi && x.Tanggal == tanggalPertama.Tanggal)
                .OrderByDescending(x => x.Tanggal)
                .Select(x => new OtomatisTanggaldanRakDto
                {
                    Id = x.Id,
                    IdRak = x.IdRak,
                    NamaRak = x.MasterRak.NomorRak,
                    Tanggal = x.Tanggal,
                }).ToListAsync(cancellationToken);

            return await Result<List<OtomatisTanggaldanRakDto>>.SuccessAsync(sql, "Successfully fetch data");
        }
    }
}