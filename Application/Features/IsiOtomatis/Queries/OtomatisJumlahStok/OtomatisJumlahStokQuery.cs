﻿using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisJumlahStok
{
    public record class OtomatisJumlahStokQuery : IRequest<Result<OtomatisJumlahStokDto>>
    {
        public Guid Id { get; set; }
        public Guid IdLokasi { get; set; }

        public OtomatisJumlahStokQuery() { }

        public OtomatisJumlahStokQuery(Guid id, Guid idLokasi)
        {
            Id = id;
            IdLokasi = idLokasi;
        }
    }

    internal class OtomatisJumlahStokQueryHandler : IRequestHandler<OtomatisJumlahStokQuery, Result<OtomatisJumlahStokDto>>
    {
        private readonly IJumlahStokRepository _jumlahStokRepository;

        public OtomatisJumlahStokQueryHandler(IJumlahStokRepository jumlahStokRepository)
        {
            _jumlahStokRepository = jumlahStokRepository;
        }

        public async Task<Result<OtomatisJumlahStokDto>> Handle(OtomatisJumlahStokQuery query, CancellationToken cancellationToken)
        {
            var notif = await _jumlahStokRepository.JumlahStok(query.Id, query.IdLokasi);
            return await Result<OtomatisJumlahStokDto>.SuccessAsync(notif, "Successfully fetch data");
        }
    }
}