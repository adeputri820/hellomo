﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisJumlahStok
{
    public class OtomatisJumlahStokDto
    {
        [JsonPropertyName("stok")]
        public int? JumlahPersediaan { get; set; }

        [JsonIgnore]
        [JsonPropertyName("id_rak")]
        public Guid Id { get; set; }

        [JsonIgnore]
        [JsonPropertyName("id_lokasi")]
        public Guid IdLokasi { get; set; }
    }
}