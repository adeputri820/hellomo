﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Commands
{
    public record BarangKeluarDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("id_lokasi")]
        public Guid? IdLokasi { get; set; }

        [JsonPropertyName("lokasi_asal")]
        public string LokasiAsal { get; set; }
        [JsonPropertyName("id_barang")]
        public Guid IdBarang { get; set; }
        [JsonPropertyName("id_rak")]
        public Guid IdRak { get; set; }
        [JsonPropertyName("jumlah_rak")]
        public int? JumlahRak { get; set; }
        [JsonPropertyName("quantity")]
        public int? Quantity { get; set; }
        [JsonPropertyName("status")]
        public string? Status { get; set; }
        [JsonPropertyName("pic")]
        public string? PIC { get; set; }
        [JsonPropertyName("id_transaksi")]
        public string? IdTransaksi { get; set; }
        [JsonPropertyName("tanggal_transfer")]
        public DateOnly? Tanggal { get; set; }
        [JsonPropertyName("tanggal_barang_keluar")]
        public DateOnly? TanggalBarangKeluar { get; set; }
    }
    public sealed record CreateResponseBarangKeluarDto : BarangKeluarDto
    {
        [JsonPropertyName("id_barang_masuk")]
        public Guid IdBarangMasuk { get; set; }
    }
}