﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.KonfirmasiBarangKeluar
{
    public class UpdateKonfirmasiBarangKeluarRequest : IRequest<Result<UpdateStatusDto>>
    {
        [JsonIgnore]
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("id_transaksi")]
        public string? IdTransaksi { get; set; }
    }

    public class UpdateStatusDto
    {
        [JsonIgnore]
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("id_transaksi")]
        public string? IdTransaksi { get; set; }
    }
}