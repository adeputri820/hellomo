﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.KonfirmasiBarangKeluar
{
    public class KonfirmasiBarangKeluarEvent : BaseEvent
    {
        public BarangKeluar BarangKeluar { get; set; }

        public KonfirmasiBarangKeluarEvent(BarangKeluar barangKeluar)
        {
            BarangKeluar = barangKeluar;
        }
    }
}