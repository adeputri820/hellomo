﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.KonfirmasiBarangKeluar
{
    internal class UpdateKonfirmasiBarangKeluarCommandHandler : IRequestHandler<UpdateKonfirmasiBarangKeluarRequest, Result<UpdateStatusDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateKonfirmasiBarangKeluarCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<UpdateStatusDto>> Handle(UpdateKonfirmasiBarangKeluarRequest request, CancellationToken cancellationToken)
        {
            var data = new UpdateStatusDto();
            var sql = await _unitOfWork.Repository<BarangKeluar>().Entities
                .Where(x => x.IdTransaksi == request.IdTransaksi)
                .OrderByDescending(x => x.Tanggal)
                .Select(x => new UpdateStatusDto
                {
                    Id = x.Id,
                    IdTransaksi = x.IdTransaksi,
                }).ToListAsync(cancellationToken);

            foreach (var daftarData in sql)
            {
                var konfirmasiBarangKeluar = await _unitOfWork.Repository<BarangKeluar>().GetByIdAsync(daftarData.Id);
                if (konfirmasiBarangKeluar != null)
                {
                    konfirmasiBarangKeluar.Status = "Selesai";
                    konfirmasiBarangKeluar.UpdatedAt = DateTime.UtcNow;

                    await _unitOfWork.Repository<BarangKeluar>().UpdateAsync(konfirmasiBarangKeluar);
                    konfirmasiBarangKeluar.AddDomainEvent(new KonfirmasiBarangKeluarEvent(konfirmasiBarangKeluar));

                    await _unitOfWork.Save(cancellationToken);
                }
            }
            return await Result<UpdateStatusDto>.SuccessAsync(data, "Barang updated");
        }
    }
}