﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.CreateBarangKeluar
{
    public class CreateBarangKeluarRequest : IRequest<Result<List<CreateResponseBarangKeluarDto>>>
    {

        [JsonPropertyName("lokasi_tujuan")]
        public string LokasiTujuan { get; set; }
        [JsonPropertyName("daftar_barang_keluar")]
        public List<CreateResponseBarangKeluarDto> DaftarBarangKeluar { get; set; }
    }
}