﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.CreateBarangKeluar
{
    public class BarangKeluarCreatedEvent : BaseEvent
    {
        public BarangKeluar BarangKeluar { get; set; }

        public BarangKeluarCreatedEvent(BarangKeluar barangKeluar)
        {
            BarangKeluar = barangKeluar;
        }
    }
}