﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Commands.CreateBarangKeluar
{
    internal class CreateBarangKeluarCommandHandler : IRequestHandler<CreateBarangKeluarRequest, Result<List<CreateResponseBarangKeluarDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IUpdateAndDeleteRepository _updateAndDeleteRepository;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public CreateBarangKeluarCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IUpdateAndDeleteRepository updateAndDeleteRepository, IServiceScopeFactory serviceScopeFactory)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _updateAndDeleteRepository = updateAndDeleteRepository;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public async Task<Result<List<CreateResponseBarangKeluarDto>>> Handle(CreateBarangKeluarRequest request, CancellationToken cancellationToken)
        {
            var output = new List<CreateResponseBarangKeluarDto>();
            var barangKeluar = new BarangKeluar();
            barangKeluar.IdTransaksi = DateTime.Now.ToString("TR-yyMMddhhmmssfff");
            barangKeluar.CreatedAt = DateTime.UtcNow;
            barangKeluar.UpdatedAt = DateTime.UtcNow;
            barangKeluar.Status = "Dalam Perjalanan";
            barangKeluar.LokasiTujuan = request.LokasiTujuan;
            foreach (var data in request.DaftarBarangKeluar)
            {
                barangKeluar.Id = Guid.NewGuid();
                barangKeluar.PIC = data.PIC;
                barangKeluar.Quantity = data.Quantity;
                barangKeluar.IdRak = data.IdRak;
                barangKeluar.LokasiAsal = data.LokasiAsal;
                barangKeluar.TanggalBarang = data.TanggalBarangKeluar;
                barangKeluar.IdBarang = data.IdBarang;
                barangKeluar.Tanggal = DateOnly.FromDateTime(DateTime.Now);
                await _unitOfWork.Repository<BarangKeluar>().AddAsync(barangKeluar);
                barangKeluar.AddDomainEvent(new BarangKeluarCreatedEvent(barangKeluar));
                await _unitOfWork.Save(cancellationToken);

                var jumlahQuantity = await _unitOfWork.Repository<BarangMasuk>().Entities
               .Where(x => x.Id == data.IdBarangMasuk)
               .Select(x => new
               {
                   Quantity = x.Quantity
               }).FirstOrDefaultAsync(cancellationToken);

                int? validasi = jumlahQuantity.Quantity - data.Quantity;

                if (validasi > 0)
                {
                    var barangMasukUpdate = await _unitOfWork.Repository<BarangMasuk>().GetByIdAsync(data.IdBarangMasuk);
                    barangMasukUpdate.Quantity = jumlahQuantity.Quantity - data.Quantity;
                    barangMasukUpdate.UpdatedAt = DateTime.UtcNow;

                    await _unitOfWork.Repository<BarangMasuk>().UpdateAsync(barangMasukUpdate);
                    await _unitOfWork.Save(cancellationToken);
                }
                else
                {
                    var barangMasukDelete = await _unitOfWork.Repository<BarangMasuk>().GetByIdAsync(data.IdBarangMasuk);
                    await _unitOfWork.Repository<BarangMasuk>().DeleteAsync(barangMasukDelete);
                    await _unitOfWork.Save(cancellationToken);
                }
            }

            return await Result<List<CreateResponseBarangKeluarDto>>.SuccessAsync(output, "Ekspedisi created.");
        }
    }
}