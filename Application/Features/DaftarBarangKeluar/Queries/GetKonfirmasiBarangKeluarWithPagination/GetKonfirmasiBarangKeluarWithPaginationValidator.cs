﻿using FluentValidation;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.GetKonfirmasiBarangKeluarWithPagination
{
    public class GetKonfirmasiBarangKeluarWithPaginationValidator : AbstractValidator<GetKonfirmasiBarangKeluarWithPaginationQuery>
    {
        public GetKonfirmasiBarangKeluarWithPaginationValidator()
        {
            RuleFor(x => x.PageNumber)
              .GreaterThanOrEqualTo(1)
              .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}