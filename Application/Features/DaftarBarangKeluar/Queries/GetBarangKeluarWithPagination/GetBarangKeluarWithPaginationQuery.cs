﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.GetBarangKeluarWithPagination
{
    public record GetBarangKeluarWithPaginationQuery : IRequest<PaginatedResult<GetBarangKeluarWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }

        public GetBarangKeluarWithPaginationQuery() { }

        public GetBarangKeluarWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetBarangKeluarWithPaginationQueryHandler : IRequestHandler<GetBarangKeluarWithPaginationQuery, PaginatedResult<GetBarangKeluarWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBarangKeluarWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetBarangKeluarWithPaginationDto>> Handle(GetBarangKeluarWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<BarangKeluar>()
            .FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
             || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.LokasiTujuan.ToLower()
             || query.SearchTerm.ToLower() == x.MasterRak.NomorRak.ToLower()).GroupBy(x => new { x.PIC, x.IdTransaksi, x.LokasiTujuan, x.Tanggal })
            .Select(p => new GetBarangKeluarWithPaginationDto
            {
                IdTransaksi = p.Key.IdTransaksi,
                PIC = p.Key.PIC,
                LokasiTujuan = p.Key.LokasiTujuan,
                TanggalTransfer = p.Key.Tanggal,
                RiwayatData = p.Select(s => new RiwayatDataBarangKeluarDto
                {
                    Sku = s.MasterBarang.Sku,
                    LokasiAsal = s.LokasiAsal,
                    Rak = s.MasterRak.NomorRak,
                    Varian = s.MasterBarang.Varian.NamaVarian,
                    Quantity = s.Quantity,
                    Tanggal = s.TanggalBarang,
                    NamaBarang = s.MasterBarang.NamaBarang
                }).ToList(),
            })
            .OrderByDescending(x => x.IdTransaksi).ProjectTo
            <GetBarangKeluarWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}