﻿using ClosedXML.Excel;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.DownloadBarangKeluarToExcell
{
    public class DownloadBarangToExcell
    {
        private readonly Task<DownloadBarangToExcellAndPdfDto> _obj;

        public DownloadBarangToExcell(Task<DownloadBarangToExcellAndPdfDto> data)
        {
            _obj = data;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "Id Transaksi";
                worksheet.Cell(1, 2).Value = "PIC";
                worksheet.Cell(1, 3).Value = "Lokasi Tujuan";
                worksheet.Cell(1, 4).Value = "Tanggal Transfer";
                worksheet.Cell(1, 5).Value = "SKU";
                worksheet.Cell(1, 6).Value = "Nama Barang";
                worksheet.Cell(1, 7).Value = "Varian";
                worksheet.Cell(1, 8).Value = "Lokasi Asal";
                worksheet.Cell(1, 9).Value = "Nomor Rak";
                worksheet.Cell(1, 10).Value = "Quantity";
                worksheet.Cell(1, 11).Value = "Tanggal Barang";

                worksheet.Cell(2, 1).Value = _obj.Result.IdTransaksi;
                worksheet.Cell(2, 2).Value = _obj.Result.PIC;
                worksheet.Cell(2, 3).Value = _obj.Result.LokasiTujuan;
                worksheet.Cell(2, 4).Value = _obj.Result.TanggalTransfer;

                for (int i = 0; i < _obj.Result.RiwayatData.Count(); i++)
                {
                    worksheet.Cell(i + 2, 5).Value = _obj.Result.RiwayatData.ElementAt(i).Sku;
                    worksheet.Cell(i + 2, 6).Value = _obj.Result.RiwayatData.ElementAt(i).NamaBarang;
                    worksheet.Cell(i + 2, 7).Value = _obj.Result.RiwayatData.ElementAt(i).Varian;
                    worksheet.Cell(i + 2, 8).Value = _obj.Result.RiwayatData.ElementAt(i).LokasiAsal;
                    worksheet.Cell(i + 2, 9).Value = _obj.Result.RiwayatData.ElementAt(i).Rak;
                    worksheet.Cell(i + 2, 10).Value = _obj.Result.RiwayatData.ElementAt(i).Quantity;
                    worksheet.Cell(i + 2, 11).Value = _obj.Result.RiwayatData.ElementAt(i).Tanggal;
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Barang_Keluar_{DateTime.Now.ToString("yyyy-MMMM-dddd")}.xlsx";
                }
            }
        }
    }
}