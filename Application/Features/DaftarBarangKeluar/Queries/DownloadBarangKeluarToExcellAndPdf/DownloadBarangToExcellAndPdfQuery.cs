﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.DaftarBarangKeluar.Queries.DownloadBarangKeluarToExcell
{
    public record class DownloadBarangToExcellAndPdfQuery : IRequest<DownloadBarangToExcellAndPdfDto>
    {
        public string IdTransaksi { get; set; }

        public DownloadBarangToExcellAndPdfQuery() { }

        public DownloadBarangToExcellAndPdfQuery(string idTransaksi)
        {
            IdTransaksi = idTransaksi;
        }
    }

    internal class DownloadBarangToExcellAndPdfQueryHandler : IRequestHandler<DownloadBarangToExcellAndPdfQuery, DownloadBarangToExcellAndPdfDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DownloadBarangToExcellAndPdfQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<DownloadBarangToExcellAndPdfDto> Handle(DownloadBarangToExcellAndPdfQuery getList, CancellationToken cancellationToken)
        {
            var data = await _unitOfWork.Repository<BarangKeluar>().Entities
                .Where(x => x.IdTransaksi.ToLower() == getList.IdTransaksi.ToLower()).GroupBy(x => new { x.PIC, x.IdTransaksi, x.LokasiTujuan, x.Tanggal })
            .Select(p => new DownloadBarangToExcellAndPdfDto
            {
                IdTransaksi = p.Key.IdTransaksi,
                PIC = p.Key.PIC,
                LokasiTujuan = p.Key.LokasiTujuan,
                TanggalTransfer = p.Key.Tanggal,
                RiwayatData = p.Select(s => new RiwayatDataBarangKeluarExcellAndPdfDto
                {
                    Sku = s.MasterBarang.Sku,
                    LokasiAsal = s.LokasiAsal,
                    Rak = s.MasterRak.NomorRak,
                    Varian = s.MasterBarang.Varian.NamaVarian,
                    Quantity = s.Quantity,
                    Tanggal = s.TanggalBarang,
                    NamaBarang = s.MasterBarang.NamaBarang
                }).ToList(),
            }).FirstOrDefaultAsync();
            return data;
        }
    }
}