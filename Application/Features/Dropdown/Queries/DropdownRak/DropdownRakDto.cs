﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Dropdown.Queries.DropdownRak
{
    public class DropdownRakDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_rak")]
        public string NamaRak { get; set; }
    }
}