﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Dropdown.Queries.DropdownRak
{
    public record class DropdownRakQuery : IRequest<Result<List<DropdownRakDto>>>;

    internal class DropdownRakQueryHandler : IRequestHandler<DropdownRakQuery, Result<List<DropdownRakDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DropdownRakQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<DropdownRakDto>>> Handle(DropdownRakQuery getList, CancellationToken cancellationToken)
        {
            var sql = await _unitOfWork.Repository<MasterRak>().Entities
                .Where(x => x.DeletedAt == null)
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new DropdownRakDto
                {
                    Id = x.Id,
                    NamaRak = x.NomorRak,
                }).ToListAsync(cancellationToken);

            return await Result<List<DropdownRakDto>>.SuccessAsync(sql, "Successfully fetch data");
        }
    }
}