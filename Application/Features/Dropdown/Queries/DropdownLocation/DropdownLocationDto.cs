﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Dropdown.Queries.DropdownLocation
{
    public class DropdownLocationDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("nama_lokasi")]
        public string NamaLokasi { get; set; }
    }
}