﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Dropdown.Queries.DropdownLocation
{
    public record class DropdownLocationQuery : IRequest<Result<List<DropdownLocationDto>>>;

    internal class DropdownLocationQueryHandler : IRequestHandler<DropdownLocationQuery, Result<List<DropdownLocationDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DropdownLocationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<DropdownLocationDto>>> Handle(DropdownLocationQuery getList, CancellationToken cancellationToken)
        {
            var sql = await _unitOfWork.Repository<MasterLokasi>().Entities
                .Where(x => x.DeletedAt == null)
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new DropdownLocationDto
                {
                    Id = x.Id,
                    NamaLokasi = x.NamaLokasi,
                }).ToListAsync(cancellationToken);

            return await Result<List<DropdownLocationDto>>.SuccessAsync(sql, "Successfully fetch data");
        }
    }
}