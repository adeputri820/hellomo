﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Dropdown.Queries.DropdownSKUdanBarang
{
    public class DropdownSKUdanBarangDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("nama_variant")]
        public string NamaVariant { get; set; }
    }
}