﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Dropdown.Queries.DropdownSKUdanBarang
{
    public record class DropdownSKUdanBarangQuery : IRequest<Result<List<DropdownSKUdanBarangDto>>>;

    internal class DropdownSKUdanBarangQueryHandler : IRequestHandler<DropdownSKUdanBarangQuery, Result<List<DropdownSKUdanBarangDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DropdownSKUdanBarangQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<DropdownSKUdanBarangDto>>> Handle(DropdownSKUdanBarangQuery getList, CancellationToken cancellationToken)
        {
            var sql = await _unitOfWork.Repository<MasterBarang>().Entities
                .Where(x => x.DeletedAt == null)
                .OrderByDescending(x => x.CreatedAt)
                .Select(x => new DropdownSKUdanBarangDto
                {
                    Id = x.Id,
                    Sku = x.Sku,
                    NamaBarang = x.NamaBarang,
                    NamaVariant = x.Varian.NamaVarian
                }).ToListAsync(cancellationToken);

            return await Result<List<DropdownSKUdanBarangDto>>.SuccessAsync(sql, "Successfully fetch data");
        }
    }
}