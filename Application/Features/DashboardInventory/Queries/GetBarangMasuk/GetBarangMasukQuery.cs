﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetBarangMasuk
{
    public record GetBarangMasukQuery : IRequest<PaginatedResult<GetBarangMasukDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string? Type { get; set; }
        public string? Lok { get; set; }

        public GetBarangMasukQuery() { }

        public GetBarangMasukQuery(int pageNumber, int pageSize, string searchTerm, DateTime? start, DateTime? end, string type, string lok)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
            Start = start;
            End = end;
            Type = type;
            Lok = lok;
        }
    }

    internal class GetBarangMasukQueryHandler : IRequestHandler<GetBarangMasukQuery, PaginatedResult<GetBarangMasukDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBarangMasukQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetBarangMasukDto>> Handle(GetBarangMasukQuery query, CancellationToken cancellationToken)
        {
            if (query.Type == "default")
            {
                return await _unitOfWork.Repository<BarangMasuk>()
                .FindByCondition(x => (query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
                 || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.MasterLokasi.NamaLokasi.ToLower()
                 || query.SearchTerm.ToLower() == x.MasterRak.NomorRak.ToLower()) &&
                 (x.MasterLokasi.NamaLokasi == query.Lok))
                .Select(p => new GetBarangMasukDto
                {
                    Id = p.Id,
                    IdBarang = p.IdBarang,
                    IdLokasi = p.IdLokasi,
                    IdRak = p.IdRak,
                    Sku = p.MasterBarang.Sku,
                    NamaBarang = p.MasterBarang.NamaBarang,
                    Varian = p.MasterBarang.Varian.NamaVarian,
                    Lokasi = p.MasterLokasi.NamaLokasi,
                    Rak = p.MasterRak.NomorRak,
                    Quantity = p.Quantity,
                    TanggalMasuk = p.Tanggal,
                    UserName = p.UserName,
                    Status = "Selesai"
                })
                .OrderByDescending(x => x.TanggalMasuk)
                .ProjectTo<GetBarangMasukDto>(_mapper.ConfigurationProvider)
                .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
            }
            //if (query.End.Value.Date < query.Start.Value.Date)
            //{
            //    throw new ArgumentException("Tanggal terakhir tidak boleh melebihi tanggal awal");
            //}
            //else
            //{
                 return await _unitOfWork.Repository<BarangMasuk>()
                .FindByCondition(x =>
                 (query.SearchTerm == null ||
                 query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() ||
                 query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower() ||
                 query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() ||
                 query.SearchTerm.ToLower() == x.MasterLokasi.NamaLokasi.ToLower() ||
                 query.SearchTerm.ToLower() == x.MasterRak.NomorRak.ToLower()) &&
                 (x.MasterLokasi.NamaLokasi == query.Lok) &&
                 (x.Tanggal >= DateOnly.FromDateTime(query.Start.Value) && x.Tanggal <= DateOnly.FromDateTime(query.End.Value))
             )
             .Select(p => new GetBarangMasukDto
             {
                 Id = p.Id,
                 IdBarang = p.IdBarang,
                 IdLokasi = p.IdLokasi,
                 IdRak = p.IdRak,
                 Sku = p.MasterBarang.Sku,
                 NamaBarang = p.MasterBarang.NamaBarang,
                 Varian = p.MasterBarang.Varian.NamaVarian,
                 Lokasi = p.MasterLokasi.NamaLokasi,
                 Rak = p.MasterRak.NomorRak,
                 Quantity = p.Quantity,
                 TanggalMasuk = p.Tanggal,
                 UserName = p.UserName,
                 Status = "Selesai"
             })
             .OrderByDescending(x => x.TanggalMasuk)
             .ProjectTo<GetBarangMasukDto>(_mapper.ConfigurationProvider)
             .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
            }
        
    }
}