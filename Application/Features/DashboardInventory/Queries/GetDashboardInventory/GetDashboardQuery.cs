﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetDashboard
{
    public class GetDashboardQuery : IRequest<Result<GetDashboardDto>>
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string? Type { get; set; }

        public GetDashboardQuery(DateTime startTime, DateTime endTIme, string type)
        {
            Start = startTime;
            End = endTIme;
            Type = type;
        }

        public GetDashboardQuery()
        {
        }
    }

    internal class GetRakAllQueryHandler : IRequestHandler<GetDashboardQuery, Result<GetDashboardDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IDashboardRepository _repository;

        public GetRakAllQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IDashboardRepository repository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<Result<GetDashboardDto>> Handle(GetDashboardQuery query, CancellationToken cancellationToken)
        {
            var dt = await _repository.GetDashboardAsync(query.Start.Date, query.End.Date, query.Type);

            return await Result<GetDashboardDto>.SuccessAsync(dt, "Successfully fetch data");
        }
    }
}