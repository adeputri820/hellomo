﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetDashboard
{
    public class GetDashboardDto
    {
        [JsonPropertyName("data_barang")]
        public int DataBarang { get; set; }

        [JsonPropertyName("barang_masuk")]
        public int BarangMasuk { get; set; }

        [JsonPropertyName("barang_keluar")]
        public int BarangKeluar { get; set; }

        [JsonPropertyName("total_user")]
        public int TotalUser { get; set; }
    }
}