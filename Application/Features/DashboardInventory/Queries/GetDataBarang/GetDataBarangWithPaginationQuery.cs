﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetDataBarang
{
    public record GetDataBarangWithPaginationQuery : IRequest<PaginatedResult<GetDataBarangWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }

        public GetDataBarangWithPaginationQuery() { }

        public GetDataBarangWithPaginationQuery(int pageNumber, int pageSize, string searchTerm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
        }
    }

    internal class GetDataBarangWithPaginationQueryHandler : IRequestHandler<GetDataBarangWithPaginationQuery, PaginatedResult<GetDataBarangWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetDataBarangWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetDataBarangWithPaginationDto>> Handle(GetDataBarangWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<BarangMasuk>()
            .FindByCondition(x => query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
             || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.MasterLokasi.NamaLokasi.ToLower())
            .GroupBy(p => new
            {
                p.MasterBarang.NamaBarang,
                p.MasterBarang.Sku,
                p.MasterBarang.Varian.NamaVarian,
                p.MasterLokasi.NamaLokasi,
            })
            .Select(p => new GetDataBarangWithPaginationDto
            {
                Sku = p.Key.Sku,
                NamaBarang = p.Key.NamaBarang,
                Varian = p.Key.NamaVarian,
                Lokasi = p.Key.NamaLokasi,
                JumlahStokLokasi = p.Sum(o => o.Quantity),
                UpdateAt = p.Max(o => o.UpdatedAt.Value),
            }).OrderByDescending(o => o.UpdateAt)
            .ProjectTo<GetDataBarangWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}