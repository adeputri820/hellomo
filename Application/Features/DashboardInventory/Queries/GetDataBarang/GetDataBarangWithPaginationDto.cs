﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetDataBarang
{
    public class GetDataBarangWithPaginationDto : IMapFrom<GetDataBarangWithPaginationDto>
    {
        //[JsonPropertyName("id")]
        //public Guid Id { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("nama_barang")]
        public string NamaBarang { get; set; }

        [JsonPropertyName("varian")]
        public string Varian { get; set; }

        [JsonPropertyName("lokasi")]
        public string Lokasi { get; set; }

        [JsonPropertyName("jumlah_stok")]
        public int? JumlahStokLokasi { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdateAt { get; set; }
    }
}