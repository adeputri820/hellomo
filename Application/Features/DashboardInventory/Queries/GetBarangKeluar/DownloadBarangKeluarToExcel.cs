﻿using ClosedXML.Excel;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetBarangKeluar
{
    public class DownloadBarangKeluarToExcel
    {
        private readonly PaginatedResult<GetBarangKeluarDto> pg;

        public DownloadBarangKeluarToExcel(PaginatedResult<GetBarangKeluarDto> paginated)
        {
            pg = paginated;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "ID Transaksi";
                worksheet.Cell(1, 2).Value = "Lokasi Tujuan";
                worksheet.Cell(1, 3).Value = "Lokasi Asal";
                worksheet.Cell(1, 4).Value = "SKU";
                worksheet.Cell(1, 5).Value = "Nama Barang";
                worksheet.Cell(1, 6).Value = "Varian";
                worksheet.Cell(1, 7).Value = "Quantity";
                worksheet.Cell(1, 8).Value = "Rak";
                worksheet.Cell(1, 9).Value = "Tanggal Barang Keluar";
                worksheet.Cell(1, 10).Value = "PIC";
                worksheet.Cell(1, 11).Value = "Status";

                for (int i = 0; i < pg.Data.Count(); i++)
                {
                    worksheet.Cell(i + 2, 1).Value = pg.Data.ElementAt(i).IdTransaksi;
                    worksheet.Cell(i + 2, 2).Value = pg.Data.ElementAt(i).LokasiTujuan;
                    worksheet.Cell(i + 2, 3).Value = pg.Data.ElementAt(i).LokasiAsal;
                    worksheet.Cell(i + 2, 4).Value = pg.Data.ElementAt(i).Sku;
                    worksheet.Cell(i + 2, 5).Value = pg.Data.ElementAt(i).NamaBarang;
                    worksheet.Cell(i + 2, 6).Value = pg.Data.ElementAt(i).Varian;
                    worksheet.Cell(i + 2, 7).Value = pg.Data.ElementAt(i).Quantity;
                    worksheet.Cell(i + 2, 8).Value = pg.Data.ElementAt(i).Rak;
                    worksheet.Cell(i + 2, 9).Value = pg.Data.ElementAt(i).Tanggal;
                    worksheet.Cell(i + 2, 10).Value = pg.Data.ElementAt(i).PIC;
                    worksheet.Cell(i + 2, 11).Value = pg.Data.ElementAt(i).Status;
                }
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Barang_Keluar_{DateTime.Now.ToString("yyyy-MMMM-dddd")}.xlsx";
                }
            }
        }
    }
}