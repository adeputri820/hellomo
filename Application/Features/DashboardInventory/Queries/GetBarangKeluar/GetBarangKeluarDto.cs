﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetBarangKeluar
{
    public class GetBarangKeluarDto : IMapFrom<GetBarangKeluarDto>
    {
        [JsonPropertyName("id_transaksi")]
        public string IdTransaksi {  get; set; }
        [JsonPropertyName("lokasi_tujuan")]
        public string LokasiTujuan { get; set; }
        [JsonPropertyName("lokasi_asal")]
        public string LokasiAsal { get; set; }
        [JsonPropertyName("sku")]
        public string Sku { get; set; }

        [JsonPropertyName("namaBarang")]
        public string NamaBarang { get; set; }
       
        [JsonPropertyName("varian")]
        public string Varian { get; set; }

        [JsonPropertyName("jumlah")]
        public int? Quantity { get; set; }

        [JsonPropertyName("rak")]
        public string Rak { get; set; }

        [JsonPropertyName("tanggal_barang_keluar")]
        public DateOnly? Tanggal { get; set; }

        [JsonPropertyName("pic")]
        public string PIC { get; set; }
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}