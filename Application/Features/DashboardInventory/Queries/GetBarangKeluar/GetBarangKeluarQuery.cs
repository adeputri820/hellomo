﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetBarangKeluar
{
    public record GetBarangKeluarQuery : IRequest<PaginatedResult<GetBarangKeluarDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string? Type { get; set; }
        public string? Status { get; set; }

        public GetBarangKeluarQuery() { }

        public GetBarangKeluarQuery(int pageNumber, int pageSize, string searchTerm, DateTime? start, DateTime? end, string type, string status)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchTerm;
            Start = start;
            End = end;
            Type = type;
            Status = status;
        }
    }

    internal class GetBarangKeluarQueryHandler : IRequestHandler<GetBarangKeluarQuery, PaginatedResult<GetBarangKeluarDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBarangKeluarQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetBarangKeluarDto>> Handle(GetBarangKeluarQuery query, CancellationToken cancellationToken)
        {
            if (query.Type == "default")
            {
                return await _unitOfWork.Repository<BarangKeluar>()
                .FindByCondition(x => (query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
                 || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.LokasiTujuan.ToLower()
                 || query.SearchTerm.ToLower() == x.MasterRak.NomorRak.ToLower() || query.SearchTerm.ToLower() == x.Status.ToLower() || query.SearchTerm.ToLower() == x.LokasiAsal.ToLower()))
                .Select(p => new GetBarangKeluarDto
                {
                    IdTransaksi = p.IdTransaksi,
                    LokasiTujuan = p.LokasiTujuan,
                    LokasiAsal = p.LokasiAsal,
                    Sku = p.MasterBarang.Sku,
                    NamaBarang = p.MasterBarang.NamaBarang,
                    Varian = p.MasterBarang.Varian.NamaVarian,
                    Quantity = p.Quantity,
                    Rak = p.MasterRak.NomorRak,
                    Tanggal = p.Tanggal,
                    PIC = p.PIC,
                    Status = p.Status
                }).ProjectTo
                <GetBarangKeluarDto>(_mapper.ConfigurationProvider)
                .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
            }
            return await _unitOfWork.Repository<BarangKeluar>()
               .FindByCondition(x => (query.SearchTerm == null || query.SearchTerm.ToLower() == x.MasterBarang.Sku.ToLower() || query.SearchTerm.ToLower() == x.MasterBarang.NamaBarang.ToLower()
                || query.SearchTerm.ToLower() == x.MasterBarang.Varian.NamaVarian.ToLower() || query.SearchTerm.ToLower() == x.LokasiTujuan.ToLower()
                || query.SearchTerm.ToLower() == x.MasterRak.NomorRak.ToLower()) && (x.Status == query.Status)
                && (x.Status == query.Status) 
                && (x.Tanggal >= DateOnly.FromDateTime(query.Start.Value) && x.Tanggal <= DateOnly.FromDateTime(query.End.Value)))
               .Select(p => new GetBarangKeluarDto
               {
                   IdTransaksi = p.IdTransaksi,
                   LokasiTujuan = p.LokasiTujuan,
                   LokasiAsal = p.LokasiAsal,
                   Sku = p.MasterBarang.Sku,
                   NamaBarang = p.MasterBarang.NamaBarang,
                   Varian = p.MasterBarang.Varian.NamaVarian,
                   Quantity = p.Quantity,
                   Rak = p.MasterRak.NomorRak,
                   Tanggal = p.Tanggal,
                   PIC = p.PIC,
                   Status = p.Status
               }).ProjectTo
               <GetBarangKeluarDto>(_mapper.ConfigurationProvider)
               .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}