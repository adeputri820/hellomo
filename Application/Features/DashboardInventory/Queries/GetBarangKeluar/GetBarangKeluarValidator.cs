﻿using FluentValidation;

namespace SkeletonApi.Application.Features.DashboardInventory.Queries.GetBarangKeluar
{
    public class GetBarangKeluarValidator : AbstractValidator<GetBarangKeluarQuery>
    {
        public GetBarangKeluarValidator()
        {
            RuleFor(x => x.PageNumber)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}