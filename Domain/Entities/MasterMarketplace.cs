﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterMarketplace : BaseAuditableEntity
    {
        [Column("nama_marketplace")]
        public string NamaMarketplace { get; set; }
        public ICollection<Komplain> Komplain { get; } = new List<Komplain>();
    }
}