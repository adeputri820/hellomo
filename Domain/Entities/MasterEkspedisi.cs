﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterEkspedisi : BaseAuditableEntity
    {
        [Column("nama_ekspedisi")]
        public string NamaEkspedisi { get; set; }
        public ICollection<Pembelian> Pembelian { get; } = new List<Pembelian>();
    }
}