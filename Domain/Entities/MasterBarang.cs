﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterBarang : BaseAuditableEntity
    {
        [Column("id_varian")]
        public Guid IdVarian { get; set; }

        [Column("sku")]
        public string Sku { get; set; }

        [Column("nama_barang")]
        public string NamaBarang { get; set; }

        [Column("harga_default")]
        public int HargaDefault { get; set; }

        public MasterVarian Varian { get; set; }

        public ICollection<BarangMasuk> DaftarBarangMasuk { get; } = new List<BarangMasuk>();
        public ICollection<BarangKembali> BarangKembali { get; } = new List<BarangKembali>();
        public ICollection<BarangKeluar> DaftarBarangKeluar { get; } = new List<BarangKeluar>();
        public ICollection<PeringatanStok> DaftarPeringatanStok { get; } = new List<PeringatanStok>();
        public ICollection<Pembelian> Pembelian { get; } = new List<Pembelian>();
        public ICollection<Komplain> Komplain { get; } = new List<Komplain>();
    }
}