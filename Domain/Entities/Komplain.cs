﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Komplain : BaseAuditableEntity
    {
        [Column("id_barang")]
        public Guid IdBarang { get; set; }

        [Column("id_marketplace")]
        public Guid IdMarketplace { get; set; }

        [Column("no_pesanan")]
        public string NoPesanan { get; set; }

        [Column("username")]
        public string Username { get; set; }

        [Column("harga")]
        public double Harga { get; set; }

        [Column("qty")]
        public int Qty { get; set; }

        [Column("permasalahan")]
        public string Permasalahan { get; set; }

        [Column("status")]
        public string Status { get; set; }

        [Column("notifikasi")]
        public DateTime Notifikasi { get; set; }
        public MasterBarang MasterBarang { get; set; }
        public MasterMarketplace MasterMarketplace { get; set; }
       
    }
}