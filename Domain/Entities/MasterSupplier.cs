﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterSupplier : BaseAuditableEntity
    {
        [Column("nama_supplier")]
        public string NamaSupplier { get; set; }
        public ICollection<Pembelian> Pembelian { get; } = new List<Pembelian>();
    }
}