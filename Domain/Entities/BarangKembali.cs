﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SkeletonApi.Domain.Entities
{
    public class BarangKembali : BaseAuditableEntity
    {
        [Column("id_transaksi")]
        public string IdTransaksi { get; set; }

        [Column("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [Column("id_barang")]
        public Guid IdBarang { get; set; }

        [Column("quantity")]
        public int Quantity { get; set; }

        [Column("status")]
        public string Status { get; set; }

        [Column("pic")]
        public string Pic { get; set; }

        [Column("catatan")]
        public string Catatan { get; set; }

        [Column("tanggal")]
        public DateOnly? Tanggal { get; set; }

        public MasterBarang MasterBarang { get; set; }
        public MasterLokasi MasterLokasi { get; set; }
    }
}