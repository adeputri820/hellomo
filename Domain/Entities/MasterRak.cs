﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterRak : BaseAuditableEntity
    {
        [Column("nomor_rak")]
        public string NomorRak { get; set; }

        [Column("nomor_urut")]
        public int? NomorUrut { get; set; }

        public ICollection<BarangMasuk> DaftarBarangMasuk { get; } = new List<BarangMasuk>();
        public ICollection<BarangKeluar> DaftarBarangKeluar { get; } = new List<BarangKeluar>();
    }
}