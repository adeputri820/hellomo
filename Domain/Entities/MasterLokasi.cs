﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterLokasi : BaseAuditableEntity
    {
        [Column("nama_lokasi")]
        public string NamaLokasi { get; set; }

        public ICollection<BarangMasuk> DaftarBarangMasuk { get; } = new List<BarangMasuk>();
        public ICollection<Pembelian> Pembelian { get; } = new List<Pembelian>();
        public ICollection<BarangKembali> BarangKembali { get; } = new List<BarangKembali>();
        public ICollection<PeringatanStok> DaftarPeringatanStok { get; } = new List<PeringatanStok>();
    }
}