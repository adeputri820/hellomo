﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Pembelian : BaseAuditableEntity
    {
        [Column("id_supplier")]
        public Guid IdSupplier { get; set; }
        [Column("id_lokasi")]
        public Guid IdLokasi {  get; set; }
        [Column("id_ekspedisi")]
        public Guid IdEkspedisi { get; set; }
        [Column("id_barang")]
        public Guid IdBarang {  get; set; }
        [Column("quantity")]
        public int Quantity { get; set; }
        [Column("harga_satuan_cny")]
        public double? HargaSatuanCny {  get; set; }
        [Column("konversi_cny")]
        public double? KonversiCny {  get; set; }
        [Column("harga_satuan_idr")]
        public double? HargaSatuanIdr { get; set; }
        [Column("no_pesanan")]
        public string NoPesanan {  get; set; }
        [Column("estimasi_tiba")]
        public DateTime EstimasiTiba { get; set; }
        [Column("tgl_order")]
        public DateTime TglOrder {  get; set; }
        [Column("keterangan")]
        public string? Keterangan {  get; set; }
        [Column("kode_marking")]
        public string? KodeMarking {  get; set; }
        [Column("cbm")]
        public double? Cbm {  get; set; }
        [Column("harga_per_cbm")]
        public double? HargaPerCbm {  get; set; }
        [Column("pic")]
        public string? Pic {  get; set; }
        [Column("status")]
        public string? Status {  get; set; }

        public MasterSupplier MasterSupplier { get; set; }
        public MasterLokasi MasterLokasi { get; set; }
        public MasterBarang MasterBarang { get; set; }
        public MasterEkspedisi MasterEkspedisi { get; set; }
    }
}