﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class BarangKeluar : BaseAuditableEntity
    {
        [Column("lokasi_asal")]
        public string LokasiAsal { get; set; }

        [Column("lokasi_tujuan")]
        public string LokasiTujuan { get; set; }

        [Column("id_barang")]
        public Guid IdBarang { get; set; }

        [Column("id_rak")]
        public Guid IdRak { get; set; }

        [Column("quantity")]
        public int? Quantity { get; set; }

        [Column("status")]
        public string? Status { get; set; }

        [Column("pic")]
        public string? PIC { get; set; }

        [Column("id_transaksi")]
        public string? IdTransaksi { get; set; }

        [Column("tgl_transfer")]
        public DateOnly? Tanggal { get; set; }

        [Column("tanggal_barang_keluar")]
        public DateOnly? TanggalBarang { get; set; }

        public MasterBarang MasterBarang { get; set; }
        public MasterRak MasterRak { get; set; }
    }
}