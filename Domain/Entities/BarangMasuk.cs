﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class BarangMasuk : BaseAuditableEntity
    {
        [Column("id_barang")]
        public Guid IdBarang { get; set; }

        [Column("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [Column("id_rak")]
        public Guid IdRak { get; set; }

        [Column("quantity")]
        public int? Quantity { get; set; }

        [Column("status")]
        public string? Status { get; set; }

        [Column("tanggal_masuk")]
        public DateOnly? Tanggal { get; set; }

        [Column("username")]
        public string? UserName { get; set; }

        public MasterBarang MasterBarang { get; set; }
        public MasterLokasi MasterLokasi { get; set; }
        public MasterRak MasterRak { get; set; }
    }
}