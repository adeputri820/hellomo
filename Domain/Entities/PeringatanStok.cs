﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class PeringatanStok : BaseAuditableEntity
    {
        [Column("id_barang")]
        public Guid IdBarang { get; set; }

        [Column("id_lokasi")]
        public Guid IdLokasi { get; set; }

        [Column("minimal_stok")]
        public int? MinimalStok { get; set; }

        [Column("status")]
        public string? Status { get; set; }

        public MasterBarang MasterBarang { get; set; }
        public MasterLokasi MasterLokasi { get; set; }
    }
}