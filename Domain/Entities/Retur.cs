﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Retur
    {
        [Column("id_transaksi")]
        public Guid IdTransaksi { get; set; }

        [Column("id_brg")]
        public Guid IdBrg { get; set; }

        [Column("qty")]
        public int Qty { get; set; }

        [Column("catatan")]
        public string Catatan { get; set; }
    }
}