﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterVarian : BaseAuditableEntity
    {
        [Column("nama_varian")]
        public string NamaVarian { get; set; }

        public ICollection<MasterBarang> Barang { get; } = new List<MasterBarang>();
    }
}