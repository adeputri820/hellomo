﻿namespace SkeletonApi.Domain.Common.Interfaces
{
    public interface IEntity
    {
        public Guid Id { get; set; }
    }
}