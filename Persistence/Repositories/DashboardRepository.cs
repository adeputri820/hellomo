﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.DashboardInventory.Queries.GetDashboard;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class DashboardRepository : IDashboardRepository
    {
        private readonly IGenericRepository<BarangMasuk> _brgMasuk;
        private readonly IGenericRepository<BarangKeluar> _brgKeluar;
        private readonly UserManager<User> _userManager;

        public DashboardRepository(IGenericRepository<BarangMasuk> brgMasuk, IGenericRepository<BarangKeluar> brgKeluar, UserManager<User> user)
        {
            _brgMasuk = brgMasuk;
            _brgKeluar = brgKeluar;
            _userManager = user;
        }

        public async Task<GetDashboardDto> GetDashboardAsync(DateTime start, DateTime end, string type)
        {
            var data = new GetDashboardDto();
            if (type == "default")
            {
                var totMasuk = await _brgMasuk.FindByCondition(o => o.Tanggal == DateOnly.FromDateTime(DateTime.Now)).GroupBy(p => new { p.IdLokasi, p.IdBarang, p.IdRak }).CountAsync();
                var totMasuk2 = await _brgMasuk.FindByCondition(o => o.Tanggal == DateOnly.FromDateTime(DateTime.Now)).CountAsync();
                var totKeluar = await _brgKeluar.FindByCondition(o => o.TanggalBarang == DateOnly.FromDateTime(DateTime.Now)).CountAsync();
                var totUser = await _userManager.Users.CountAsync();

                data = new GetDashboardDto
                {
                    DataBarang = totMasuk,
                    BarangMasuk = totMasuk2,
                    BarangKeluar = totKeluar,
                    TotalUser = totUser,
                };
            }
            else
            {
                var totMasuk = await _brgMasuk.FindByCondition(o => o.Tanggal <= DateOnly.FromDateTime(start.Date) && o.Tanggal >= DateOnly.FromDateTime(end.Date)).GroupBy(p => new { p.IdLokasi, p.IdBarang, p.IdRak }).CountAsync();
                var totMasuk2 = await _brgMasuk.FindByCondition(o => o.Tanggal <= DateOnly.FromDateTime(start.Date) && o.Tanggal >= DateOnly.FromDateTime(end.Date)).CountAsync();
                var totKeluar = await _brgKeluar.FindByCondition(o => o.TanggalBarang <= DateOnly.FromDateTime(start.Date) && o.Tanggal >= DateOnly.FromDateTime(end.Date)).CountAsync();
                var totUser = await _userManager.Users.Where(o => o.CreatedAt.Value.Date <= start.Date && o.CreatedAt.Value.Date == end.Date).CountAsync();

                data = new GetDashboardDto
                {
                    DataBarang = totMasuk,
                    BarangMasuk = totMasuk2,
                    BarangKeluar = totKeluar,
                    TotalUser = 1
                };
            }
            return data;
        }
    }
}