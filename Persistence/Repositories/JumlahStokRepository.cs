﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.Download;
using SkeletonApi.Application.Features.DaftarBarangKembali.Queries.GetBarangKembaliWithPagination;
using SkeletonApi.Application.Features.IsiOtomatis.Queries.OtomatisJumlahStok;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class JumlahStokRepository : IJumlahStokRepository
    {
        private readonly IDapperReadDbConnection _dapperReadDbConnection;
        private readonly IGenericRepository<BarangKembali> _repository;

        public JumlahStokRepository(IDapperReadDbConnection dapperReadDbConnection, IGenericRepository<BarangKembali> repository)
        {
            _dapperReadDbConnection = dapperReadDbConnection;
            _repository = repository;
        }

        public async Task<DownloadBarangKembaliToExcelAndPdfDto> DownloadBarangKembaliToExcelAndPdfDto(string idTransaksi)
        {
            var dt =  _repository.Entities.Where(o => o.IdTransaksi == idTransaksi).GroupBy(p => new { p.Pic, p.IdTransaksi, p.MasterLokasi.NamaLokasi, p.IdLokasi, p.Status, p.Tanggal })
            .Select(o => new DownloadBarangKembaliToExcelAndPdfDto
            {
                IdTransaksi = o.Key.IdTransaksi,
                IdLokasi = o.Key.IdLokasi,
                LokasiTujuan = o.Key.NamaLokasi,
                Status = o.Key.Status,
                Pic = o.Key.Pic,
                Tanggal = o.Key.Tanggal,
                DataBrg = o.Select(o => new RiwayatDataDto
                {
                    IdBarang = o.IdBarang,
                    Sku = o.MasterBarang.Sku,
                    NamaBarang = o.MasterBarang.NamaBarang,
                    Varian = o.MasterBarang.Varian.NamaVarian,
                    Catatan = o.Catatan,
                    Quantity = o.Quantity,
                }).ToList()
            }).FirstOrDefault();

            return dt;
        }

        public async Task<List<GetBarangKembaliWithPaginationDto>> GetBarangKembaliWithPagination()
        {
            var data =  _repository.Entities.GroupBy(p => new {p.Pic, p.IdTransaksi, p.MasterLokasi.NamaLokasi, p.IdLokasi, p.Status, p.Tanggal }).Select(o => new GetBarangKembaliWithPaginationDto
            {
                IdTransaksi = o.Key.IdTransaksi,
                IdLokasi = o.Key.IdLokasi,
                LokasiTujuan = o.Key.NamaLokasi,
                Status = o.Key.Status,
                Pic = o.Key.Pic,
                Tanggal = o.Key.Tanggal,
                DataBrg = o.Select(o => new RiwayatDataDto 
                {
                    IdBarang = o.IdBarang,
                    Sku = o.MasterBarang.Sku,
                    NamaBarang = o.MasterBarang.NamaBarang,
                    Varian = o.MasterBarang.Varian.NamaVarian,
                    Catatan = o.Catatan,
                    Quantity = o.Quantity,
                }).ToList()
            }).OrderByDescending(p => p.Tanggal).ToList();
           return data;
        }

        public async Task<OtomatisJumlahStokDto> JumlahStok(Guid idRak, Guid idLokasi)
        {
            var dataStok = await _dapperReadDbConnection.QueryAsync<BarangMasuk>
               ($@"SELECT * FROM ""BarangMasuk"" WHERE id = '{idRak}' And id_lokasi = '{idLokasi}'");
            var JumlahStok = dataStok.Select(g => new OtomatisJumlahStokDto
            {
                Id = g.IdRak,
                IdLokasi = g.IdLokasi,
                JumlahPersediaan = g.Quantity
            }).FirstOrDefault();
            return JumlahStok;
        }
    }
}