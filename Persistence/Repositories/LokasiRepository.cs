﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class LokasiRepository : ILokasiRepository
    {
        private readonly IGenericRepository<MasterLokasi> _masterLokasiRepository;

        public LokasiRepository(IGenericRepository<MasterLokasi> repository)
        {
            _masterLokasiRepository = repository;
        }

        public async Task<bool> ValidateData(MasterLokasi lokasi)
        {
            var x = await _masterLokasiRepository.FindByCondition(o => o.NamaLokasi.ToLower() == lokasi.NamaLokasi.ToLower()).CountAsync();
            if (x > 0)
            {
                return true;
            }
            return false;
        }
    }
}