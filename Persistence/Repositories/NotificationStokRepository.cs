﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.DataPeringatanStok.Queries.Notification;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class NotificationStokRepository : INotificationStokRepository
    {
        private readonly IDapperReadDbConnection _dapperReadDbConnection;
        private readonly IUnitOfWork _unitOfWork;

        public NotificationStokRepository(IDapperReadDbConnection dapperReadDbConnection, IUnitOfWork unitOfWork)
        {
            _dapperReadDbConnection = dapperReadDbConnection;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<GetNotificationDto>> NotificationStok()
        {
            var data = new List<GetNotificationDto>();
            var barangMasuk = await _unitOfWork.Repository<BarangMasuk>().Entities
               .Select(x => new
               {
                   Id = x.Id,
                   IdBarang = x.IdBarang,
                   IdLokasi = x.IdLokasi,
                   SKU = x.MasterBarang.Sku,
                   NamaBarang = x.MasterBarang.NamaBarang,
                   Varian = x.MasterBarang.Varian.NamaVarian,
                   Quantity = x.Quantity
               }).ToListAsync();
            var JumlahStok = barangMasuk.GroupBy(p => new { p.IdBarang, p.IdLokasi, p.SKU, p.NamaBarang, p.Varian }).Select(g => new
            {
                id_barang = g.Key.IdBarang,
                id_lokasi = g.Key.IdLokasi,
                nama_brg = g.Key.NamaBarang,
                varian = g.Key.Varian,
                JumlahStok = g.Sum(x => x.Quantity),
                SKU = g.Key.SKU
            }).ToList();

            foreach (var condition in JumlahStok)
            {
                var peringatanstok = await _dapperReadDbConnection.QueryAsync<PeringatanStok>
               ($@"SELECT * FROM ""PeringatanStok"" WHERE id_barang = '{condition.id_barang}' And id_lokasi = '{condition.id_lokasi}'
                And minimal_stok > {condition.JumlahStok} ");

                var notification = peringatanstok.Select(g => new GetNotificationDto
                {
                    Value = Convert.ToInt32(condition.JumlahStok),
                    message = $"Stok Dari Sku:{condition.SKU} Nama Barang:{condition.nama_brg} Varian: {condition.varian} Kurang Dari Batas Peringatan Stok"
                }).FirstOrDefault();
                if (notification != null)
                {
                    data.Add(notification);
                }
            }
            return data;
        }
    }
}