﻿using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Persistence.Interfaces;
using SkeletonApi.Persistence.Repositories.Dapper;

namespace SkeletonApi.Persistence.Repositories
{
    public class UpdateAndDeleteBarangMasukRepository : IUpdateAndDeleteRepository
    {
        private readonly IDapperCreateUnitOfWork _dapperUwow;
        private readonly IGetConnection _getConnection;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateAndDeleteBarangMasukRepository(DapperUnitOfWorkContext dapperUwow, IUnitOfWork unitOfWork)
        {
            _dapperUwow = dapperUwow;
            _getConnection = dapperUwow;
            _unitOfWork = unitOfWork;
        }
    }
}