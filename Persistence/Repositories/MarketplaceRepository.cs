﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class MarketplaceRepository : IMarketplaceRepository
    {
        private readonly IGenericRepository<MasterMarketplace> _masterMarketplaceRepository;

        public MarketplaceRepository(IGenericRepository<MasterMarketplace> repository)
        {
            _masterMarketplaceRepository = repository;
        }

        public async Task<bool> ValidateData(MasterMarketplace masterMarketplace)
        {
            var x = await _masterMarketplaceRepository.FindByCondition(o => o.NamaMarketplace.ToLower() == masterMarketplace.NamaMarketplace.ToLower()).CountAsync();
            if (x > 0)
            {
                return true;
            }
            return false;
        }
    }
}