﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class RakRepository : IRakRepository
    {
        private readonly IGenericRepository<MasterRak> _masterRakRepository;

        public RakRepository(IGenericRepository<MasterRak> repository)
        {
            _masterRakRepository = repository;
        }

        public async Task<bool> ValidateData(MasterRak rak)
        {
            var x = await _masterRakRepository.FindByCondition(o => o.NomorRak.ToLower() == rak.NomorRak.ToLower()).CountAsync();
            if (x > 0)
            {
                return true;
            }
            return false;
        }
    }
}