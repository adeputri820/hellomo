﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly IGenericRepository<MasterSupplier> _supplier;

        public SupplierRepository(IGenericRepository<MasterSupplier> repository)
        {
            _supplier = repository;
        }

        public async Task<bool> ValidateData(MasterSupplier supplier)
        {
            var x = await _supplier.FindByCondition(o => o.NamaSupplier.ToLower() == supplier.NamaSupplier.ToLower()).CountAsync();
            if (x > 0)
            {
                return true;
            }
            return false;
        }
    }
}