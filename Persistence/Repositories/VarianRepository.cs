﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class VarianRepository : IVarianRepository
    {
        private readonly IGenericRepository<MasterVarian> _masterVarianRepository;

        public VarianRepository(IGenericRepository<MasterVarian> repository)
        {
            _masterVarianRepository = repository;
        }

        public async Task<bool> ValidateData(MasterVarian masterVarian)
        {
            var x = await _masterVarianRepository.FindByCondition(o => o.NamaVarian.ToLower() == masterVarian.NamaVarian.ToLower()).CountAsync();
            if (x > 0)
            {
                return true;
            }
            return false;
        }
    }
}