﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class EkspedisiRepository : IEkspedisiRepository
    {
        private readonly IGenericRepository<MasterEkspedisi> _ekspedisiRepository;

        public EkspedisiRepository(IGenericRepository<MasterEkspedisi> repository)
        {
            _ekspedisiRepository = repository;
        }

        public async Task<bool> ValidateData(MasterEkspedisi ekspedisi)
        {
            var x = await _ekspedisiRepository.FindByCondition(o => o.NamaEkspedisi.ToLower() == ekspedisi.NamaEkspedisi.ToLower()).CountAsync();
            if (x > 0)
            {
                return true;
            }
            return false;
        }
    }
}