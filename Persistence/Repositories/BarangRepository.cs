﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class BarangRepository : IBarangRepository
    {
        private readonly IGenericRepository<MasterBarang> _repository;

        public BarangRepository(IGenericRepository<MasterBarang> repository)
        {
            _repository = repository;
        }

        public async Task<bool> ValidateData(MasterBarang masterBarang)
        {
            var x = _repository.FindByCondition(o => o.Sku.ToLower() == masterBarang.Sku.ToLower() && o.NamaBarang.ToLower() == masterBarang.NamaBarang.ToLower()).CountAsync();
            if (x == null)
            {
                return true;
            }
            return false;
        }
    }
}