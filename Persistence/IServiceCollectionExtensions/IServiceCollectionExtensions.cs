﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Application.Interfaces.Repositories.Configuration.Dapper;
using SkeletonApi.Persistence.Contexts;
using SkeletonApi.Persistence.Repositories;
using SkeletonApi.Persistence.Repositories.Dapper;

namespace SkeletonApi.Persistence.IServiceCollectionExtensions
{
    public static class IServiceCollectionExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext(configuration);
            services.AddRepositories();
        }

        public static void AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("sqlConnection");

            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseNpgsql(connectionString,
                   builder => builder.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
        }

        private static void AddRepositories(this IServiceCollection services)
        {
            services
                .AddTransient(typeof(IUnitOfWork), typeof(UnitOfWork))
                .AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>))
                .AddTransient(typeof(IGenRepository<>), typeof(GenRepository<>))
                .AddTransient(typeof(IDataRepository<>), typeof(DataRepository<>))
                .AddScoped<DapperUnitOfWorkContext>()
                .AddTransient<IRoleRepository, RoleRepository>()
                .AddTransient<IAuthenticationUserRepository, AuthenticationRepository>()
                .AddTransient<IAccountRepository, AccountRepository>()
                .AddScoped<IDiviceDateRepository, DeviceDataRepository>()
                .AddScoped<IBarangRepository, BarangRepository>()
                .AddScoped<IVarianRepository, VarianRepository>()
                .AddScoped<IRakRepository, RakRepository>()
                .AddScoped<IEkspedisiRepository, EkspedisiRepository>()
                .AddScoped<IMarketplaceRepository, MarketplaceRepository>()
                .AddScoped<ISupplierRepository, SupplierRepository>()
                .AddScoped<INotificationStokRepository, NotificationStokRepository>()
                .AddScoped<IJumlahStokRepository, JumlahStokRepository>()
                .AddScoped<IUpdateAndDeleteRepository, UpdateAndDeleteBarangMasukRepository>()
                .AddScoped<ILokasiRepository, LokasiRepository>()
                .AddScoped<IDashboardRepository, DashboardRepository>()
                .AddTransient<IUserRepository, UserRepository>();
        }
    }
}