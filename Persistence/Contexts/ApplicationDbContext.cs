﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Common.Interfaces;
using SkeletonApi.Domain.Entities;
using System.Data;

namespace SkeletonApi.Persistence.Contexts
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, string, IdentityUserClaim<string>,
    UserRole, IdentityUserLogin<string>,
    IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        private readonly IDomainEventDispatcher _dispatcher;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IDomainEventDispatcher dispatcher = null)
            : base(options)
        {
            _dispatcher = dispatcher;
        }

        public DbSet<Account> Accounts => Set<Account>();
        public DbSet<User> Users => Set<User>();
        public DbSet<Role> Roles => Set<Role>();
        public DbSet<UserRole> UserRoles => Set<UserRole>();
        public DbSet<Permission> Permissions => Set<Permission>();
        public DbSet<MasterBarang> MasterBarang => Set<MasterBarang>();
        public DbSet<MasterEkspedisi> MasterEkspedisi => Set<MasterEkspedisi>();
        public DbSet<MasterLokasi> MasterLokasi => Set<MasterLokasi>();
        public DbSet<MasterMarketplace> MasterMarketplace => Set<MasterMarketplace>();
        public DbSet<MasterRak> MasterRak => Set<MasterRak>();
        public DbSet<MasterSupplier> MasterSupplier => Set<MasterSupplier>();
        public DbSet<MasterVarian> MasterVarian => Set<MasterVarian>();
        public DbSet<BarangMasuk> BarangMasuk => Set<BarangMasuk>();
        public DbSet<BarangKeluar> BarangKeluar => Set<BarangKeluar>();
        public DbSet<BarangKembali> BarangKembali => Set<BarangKembali>();
        public DbSet<PeringatanStok> PeringatanStok => Set<PeringatanStok>();
        public DbSet<Pembelian> Pembelian => Set<Pembelian>();
        public DbSet<Komplain> Komplain => Set<Komplain>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId);

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId);
            });

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Permissions)
                .WithOne(e => e.Role)
                .HasForeignKey(e => e.RoleId)
                .IsRequired(false);

            modelBuilder.Entity<MasterVarian>()
                 .HasMany(e => e.Barang)
                 .WithOne(e => e.Varian)
                 .HasForeignKey(e => e.IdVarian)
                 .IsRequired(false);

            modelBuilder.Entity<MasterBarang>()
                   .HasMany(e => e.DaftarBarangMasuk)
                   .WithOne(e => e.MasterBarang)
                   .HasForeignKey(e => e.IdBarang)
                   .IsRequired(false);

            modelBuilder.Entity<MasterLokasi>()
                   .HasMany(e => e.DaftarBarangMasuk)
                   .WithOne(e => e.MasterLokasi)
                   .HasForeignKey(e => e.IdLokasi)
                   .IsRequired(false);

            modelBuilder.Entity<MasterRak>()
                  .HasMany(e => e.DaftarBarangMasuk)
                  .WithOne(e => e.MasterRak)
                  .HasForeignKey(e => e.IdRak)
                  .IsRequired(false);

            modelBuilder.Entity<MasterRak>()
                  .HasMany(e => e.DaftarBarangKeluar)
                  .WithOne(e => e.MasterRak)
                  .HasForeignKey(e => e.IdRak)
                  .IsRequired(false);

            modelBuilder.Entity<MasterBarang>()
                  .HasMany(e => e.DaftarBarangKeluar)
                  .WithOne(e => e.MasterBarang)
                  .HasForeignKey(e => e.IdBarang)
                  .IsRequired(false);

            modelBuilder.Entity<MasterBarang>()
                  .HasMany(e => e.DaftarPeringatanStok)
                  .WithOne(e => e.MasterBarang)
                  .HasForeignKey(e => e.IdBarang)
                  .IsRequired(false);

            modelBuilder.Entity<MasterLokasi>()
                   .HasMany(e => e.DaftarPeringatanStok)
                   .WithOne(e => e.MasterLokasi)
                   .HasForeignKey(e => e.IdLokasi)
                   .IsRequired(false);

            modelBuilder.Entity<MasterBarang>()
              .HasMany(e => e.BarangKembali)
              .WithOne(e => e.MasterBarang)
              .HasForeignKey(e => e.IdBarang)
              .IsRequired(false);

            modelBuilder.Entity<MasterLokasi>()
                   .HasMany(e => e.BarangKembali)
                   .WithOne(e => e.MasterLokasi)
                   .HasForeignKey(e => e.IdLokasi)
                   .IsRequired(false);

            modelBuilder.Entity<MasterLokasi>()
                  .HasMany(e => e.Pembelian)
                  .WithOne(e => e.MasterLokasi)
                  .HasForeignKey(e => e.IdLokasi)
                  .IsRequired(false);

            modelBuilder.Entity<MasterEkspedisi>()
                  .HasMany(e => e.Pembelian)
                  .WithOne(e => e.MasterEkspedisi)
                  .HasForeignKey(e => e.IdEkspedisi)
                  .IsRequired(false);

            modelBuilder.Entity<MasterSupplier>()
                  .HasMany(e => e.Pembelian)
                  .WithOne(e => e.MasterSupplier)
                  .HasForeignKey(e => e.IdSupplier)
                  .IsRequired(false);

            modelBuilder.Entity<MasterBarang>()
                  .HasMany(e => e.Pembelian)
                  .WithOne(e => e.MasterBarang)
                  .HasForeignKey(e => e.IdBarang)
                  .IsRequired(false);

            modelBuilder.Entity<MasterBarang>()
                  .HasMany(e => e.Komplain)
                  .WithOne(e => e.MasterBarang)
                  .HasForeignKey(e => e.IdBarang)
                  .IsRequired(false);

            modelBuilder.Entity<MasterMarketplace>()
                 .HasMany(e => e.Komplain)
                 .WithOne(e => e.MasterMarketplace)
                 .HasForeignKey(e => e.IdMarketplace)
                 .IsRequired(false);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            int result = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            // ignore events if no dispatcher provided
            if (_dispatcher == null) return result;

            // dispatch events only if save was successful
            var entitiesWithEvents = ChangeTracker.Entries<BaseEntity>()
                .Select(e => e.Entity)
                .Where(e => e.DomainEvents.Any())
                .ToArray();

            await _dispatcher.DispatchAndClearEvents(entitiesWithEvents);

            return result;
        }

        public override int SaveChanges()
        {
            return SaveChangesAsync().GetAwaiter().GetResult();
        }

        public IDbConnection Connection { get; }
    }
}