﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SkeletonApi.WebAPI.Migrations
{
    /// <inheritdoc />
    public partial class part4_ade : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "estimasi_tiba",
                table: "Pembelian",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Komplain",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    id_barang = table.Column<Guid>(type: "uuid", nullable: false),
                    id_marketplace = table.Column<Guid>(type: "uuid", nullable: false),
                    no_pesanan = table.Column<string>(type: "text", nullable: false),
                    username = table.Column<string>(type: "text", nullable: false),
                    harga = table.Column<double>(type: "double precision", nullable: false),
                    qty = table.Column<int>(type: "integer", nullable: false),
                    permasalahan = table.Column<string>(type: "text", nullable: false),
                    status = table.Column<string>(type: "text", nullable: false),
                    notifikasi = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    created_by = table.Column<Guid>(type: "uuid", nullable: true),
                    update_by = table.Column<Guid>(type: "uuid", nullable: true),
                    deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    update_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    deleted_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Komplain", x => x.id);
                    table.ForeignKey(
                        name: "FK_Komplain_MasterBarang_id_barang",
                        column: x => x.id_barang,
                        principalTable: "MasterBarang",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_Komplain_MasterMarketplace_id_marketplace",
                        column: x => x.id_marketplace,
                        principalTable: "MasterMarketplace",
                        principalColumn: "id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Komplain_id_barang",
                table: "Komplain",
                column: "id_barang");

            migrationBuilder.CreateIndex(
                name: "IX_Komplain_id_marketplace",
                table: "Komplain",
                column: "id_marketplace");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Komplain");

            migrationBuilder.AlterColumn<DateTime>(
                name: "estimasi_tiba",
                table: "Pembelian",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");
        }
    }
}
