﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SkeletonApi.WebAPI.Migrations
{
    /// <inheritdoc />
    public partial class part3_ade : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pembelian",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    id_supplier = table.Column<Guid>(type: "uuid", nullable: false),
                    id_lokasi = table.Column<Guid>(type: "uuid", nullable: false),
                    id_ekspedisi = table.Column<Guid>(type: "uuid", nullable: false),
                    id_barang = table.Column<Guid>(type: "uuid", nullable: false),
                    quantity = table.Column<int>(type: "integer", nullable: false),
                    harga_satuan_cny = table.Column<double>(type: "double precision", nullable: true),
                    konversi_cny = table.Column<double>(type: "double precision", nullable: true),
                    harga_satuan_idr = table.Column<double>(type: "double precision", nullable: true),
                    no_pesanan = table.Column<string>(type: "text", nullable: false),
                    estimasi_tiba = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    tgl_order = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    keterangan = table.Column<string>(type: "text", nullable: true),
                    kode_marking = table.Column<string>(type: "text", nullable: true),
                    cbm = table.Column<double>(type: "double precision", nullable: true),
                    harga_per_cbm = table.Column<double>(type: "double precision", nullable: true),
                    pic = table.Column<string>(type: "text", nullable: true),
                    status = table.Column<string>(type: "text", nullable: true),
                    created_by = table.Column<Guid>(type: "uuid", nullable: true),
                    update_by = table.Column<Guid>(type: "uuid", nullable: true),
                    deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    update_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    deleted_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pembelian", x => x.id);
                    table.ForeignKey(
                        name: "FK_Pembelian_MasterBarang_id_barang",
                        column: x => x.id_barang,
                        principalTable: "MasterBarang",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_Pembelian_MasterEkspedisi_id_ekspedisi",
                        column: x => x.id_ekspedisi,
                        principalTable: "MasterEkspedisi",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_Pembelian_MasterLokasi_id_lokasi",
                        column: x => x.id_lokasi,
                        principalTable: "MasterLokasi",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_Pembelian_MasterSupplier_id_supplier",
                        column: x => x.id_supplier,
                        principalTable: "MasterSupplier",
                        principalColumn: "id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pembelian_id_barang",
                table: "Pembelian",
                column: "id_barang");

            migrationBuilder.CreateIndex(
                name: "IX_Pembelian_id_ekspedisi",
                table: "Pembelian",
                column: "id_ekspedisi");

            migrationBuilder.CreateIndex(
                name: "IX_Pembelian_id_lokasi",
                table: "Pembelian",
                column: "id_lokasi");

            migrationBuilder.CreateIndex(
                name: "IX_Pembelian_id_supplier",
                table: "Pembelian",
                column: "id_supplier");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pembelian");
        }
    }
}
